<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {

    return [
        'text' => $faker->word,
        'type' => $faker->word,
        'media' => $faker->word,
        'Duration' => $faker->word,
        'online' => $faker->randomDigitNotNull,
        'request_monetize' => $faker->randomDigitNotNull,
        'is_monetize' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
