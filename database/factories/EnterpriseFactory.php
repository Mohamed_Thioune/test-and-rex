<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Enterprise;
use Faker\Generator as Faker;

$factory->define(Enterprise::class, function (Faker $faker) {

    return [
        'enterprise' => $faker->word,
        'activityDomain' => $faker->word,
        'targetArea' => $faker->word,
        'user_id' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
