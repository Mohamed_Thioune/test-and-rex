<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

use Illuminate\Support\Str;

use DB;

use Flash;

use App\Models\Enterprise;
use App\Models\Employee;
use App\User;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        Auth::logout();

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $data = $request->all();

/*      $this->guard()->login($user);
 */    
        if($request->type == "sample")
            if($request->profile == 1){
                $employee = array(
                    "lastName"  => $request->input('lastName'),
                    "firstName"  => $request->input('firstName'),
                    "enterprise"  => $request->input('enterprise'),
                    "expertiseDomain"  => $request->input('expertiseDomain'),
                    "function"  => $request->input('function'),
                    "user_id"  => $user->id
                );
                Employee::create($employee);
                Flash::success('Successfully registered as a user of a electrical network');
            }else if($request->profile == 2){
                $enterprise = array(
                    "enterprise"  => $request->input('enterprise'),
                    "expertiseDomain"  => $request->input('expertiseDomain'),
                    "model"  => $request->input('model'),
                    "user_id"  => $user->id
                );
                Enterprise::create($enterprise);
                Flash::success('Successfully registered as a enterprise of a electrical network');
            }

            if($request->type == "fabricant")
                if($request->profile == 1){
                    $enterprise = array(
                        "enterprise"  => $request->input('enterprise'),
                        "expertiseDomain"  => $request->input('expertiseDomain'),
                        "targetArea"  => $request->input('targetArea'),
                        "model"  => $request->input('model'),
                        "user_id"  => $user->id
                    );
                    Enterprise::create($enterprise);
                    Flash::success('Successfully registered as a enterprise of a electrical network');
                }else if($request->profile == 2){
                    $employee = array(
                        "lastName"  => $request->input('lastName'),
                        "firstName"  => $request->input('firstName'),
                        "enterprise"  => $request->input('enterprise'),
                        "expertiseDomain"  => $request->input('expertiseDomain'),
                        "function"  => $request->input('function'),
                        "user_id"  => $user->id
                    );

                    $enterprise = array(
                        "enterprise"  => $request->input('enterprise'),
                        "expertiseDomain"  => $request->input('expertiseDomain'),
                        "targetArea"  => $request->input('targetArea'),
                        "model"  => $request->input('model'),
                        "user_id"  => $user->id
                    );

                    Employee::create($employee);
                    Enterprise::create($enterprise);
                    Flash::success('Successfully registered as representative of a company under an electrical network activity');
                }
        
        #Insert request validation 
        $id = Str::random(45);
        DB::insert('insert into validations (id, user_id) values (?, ?)', [$id, $user->id]);
        
        #Mail validation
        \Mail::to($user['email'])->send(new \App\Mail\WelcomeEmail($data,$id));
   
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        //
    }
}

