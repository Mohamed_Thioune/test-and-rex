<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/auth', function () {
    return view('login');
})->name('auth');

Route::get('/signup', function () {
    return view('account');
})->name('signup');

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::view('/createAccount', 'createAccount');
Route::view('/qui-sommes-nous', 'qui-sommes-nous');
Route::view('/nos-solutions', 'nos-solutions');
Route::view('/actualite', 'actualite');
Route::view('/contact', 'contact')->name('contact-me');

Auth::routes();

Route::view('/login', 'login')->name('login');

Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');

Route::post('/home', 'HomeController@store_comment')->middleware('auth')->name('home.post');

Route::post('/home/reply', 'homecontroller@reply_comment')->middleware('auth')->name('home.reply');

Route::post('/home/reply', 'HomeController@reply_comment')->middleware('auth')->name('home.reply');

Route::get('/home/rex_communauty/{n}', 'HomeController@rex')->middleware('auth')->name('home.rex');

Route::post('/idea','UserController@idea')->name('idea');
Route::post('/learning','UserController@learning')->name('learning');
Route::post('/contact','UserController@contact')->name('contacter');

Route::get('/validation_verification_auth_by_email/{token}/confirmation=yes','UserController@validation')->name('validate');

Route::post('/file','PostController@file')->name('post.file');

Route::get('/formation', 'WelcomeController@formation')->name('welcome.formation');
Route::get('/support', 'WelcomeController@support')->name('welcome.support');
Route::get('/nousContacter', 'WelcomeController@contact')->name('welcome.contact');
Route::get('/notation', 'WelcomeController@notation')->name('welcome.notation');

Route::post('/contribution','WelcomeController@warning')->name('warning');

Route::get('/home/exception', 'HomeController@exception_large')->middleware('auth')->name('home_exception_large');

Route::get('/veille', 'HomeController@watch')->middleware('auth')->name('watch');

Route::get('/communication', function () {
    $events = DB::Table('events')
                ->select('events.*')
                ->where('events.day', '>=', now())
                ->orderBy('events.day')
                ->get();

    $product = DB::Table('products')
                ->select('products.*', 'enterprises.enterprise')
                ->join('enterprises', 'products.user_id', 'enterprises.user_id')
                ->first();

    $post_user_count = DB::Table('posts')
                    ->where('posts.user_id', Auth::id())
                    ->count();
    return view('plateforme.fabriquant.communication',compact($events,$product,$post_user_count));
});

Route::view('/communication', 'plateforme/fabriquant/communication');
Route::view('/publicationOffre', 'plateforme/fabriquant/publierUneOfrre');
Route::view('/webinar', 'plateforme/fabriquant/webinar');
Route::get('/notation', 'WelcomeController@note')->name('welcome.notation');
Route::view('/etudeDeMarche', 'plateforme/fabriquant/etudeDeMarche');


//Resources
Route::resource('comments', 'CommentController')->middleware('isAdmin');

Route::resource('responseComments', 'Response_commentsController')->middleware('isAdmin');

Route::resource('files', 'FileController')->middleware('isAdmin');

Route::resource('users', 'UserController');

Route::get('/users', 'UserController@index')->name('users.index')->middleware('isAdmin');
Route::get('/users/create', 'UserController@create')->name('users.create')->middleware('isAdmin');
Route::get('/users/{user}', 'UserController@show')->name('users.show')->middleware('isAdmin');
Route::get('/users/{user}/edit', 'UserController@edit')->name('users.edit')->middleware('isAdmin');
Route::delete('/users/{user}', 'UserController@destroy')->name('users.destroy')->middleware('isAdmin');


Route::resource('posts', 'PostController');

Route::get('/posts', 'PostController@index')->name('posts.index')->middleware('isAdmin');
Route::get('/posts/create', 'PostController@create')->name('posts.create')->middleware('isAdmin');
Route::get('/posts/{post}', 'PostController@show')->name('posts.show')->middleware('isAdmin');
Route::get('/posts/{post}/edit', 'PostController@edit')->name('posts.edit')->middleware('isAdmin');
Route::delete('/posts/{post}', 'PostController@destroy')->name('posts.destroy')->middleware('isAdmin');

Route::resource('reactions', 'ReactionController');  

Route::get('/reactions', 'ReactionController@index')->name('reactions.index')->middleware('isAdmin');
Route::get('/reactions/create', 'ReactionController@create')->name('reactions.create')->middleware('isAdmin');
Route::get('/reactions/{post}', 'ReactionController@show')->name('reactions.show')->middleware('isAdmin');
Route::get('/reactions/{reaction}/edit', 'ReactionController@edit')->name('reactions.edit')->middleware('isAdmin');
Route::delete('/reactions/{reaction}', 'ReactionController@destroy')->name('reactions.destroy')->middleware('isAdmin');

Route::resource('employees', 'EmployeeController')->middleware('isAdmin');

Route::resource('enterprises', 'EnterpriseController')->middleware('isAdmin');

Route::resource('events', 'EventController')->middleware('isAdmin');

Route::resource('products', 'ProductController')->middleware('isAdmin');


Route::resource('advertisements', 'advertisementController')->middleware('isAdmin');

Route::resource('announces', 'announceController')->middleware('isAdmin');