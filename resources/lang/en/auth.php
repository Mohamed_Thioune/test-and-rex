<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Your login information is incorrect',
    'activated' => 'You need to activate your account on your email box, check your spam just in case 😉',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
