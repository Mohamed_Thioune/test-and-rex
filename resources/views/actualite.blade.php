@extends('layouts.linkFront')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.4/simplePagination.css" integrity="sha512-emkhkASXU1wKqnSDVZiYpSKjYEPP8RRG2lgIxDFVI4f/twjijBnDItdaRh7j+VRKFs4YzrAcV17JeFqX+3NVig==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="{{asset('css/pagination.css')}}">

@endsection

@section('content-body-front')
<div class="contenQuiSommesNous">
<div class="blockNav">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
            <a class="navbar-brand navLogo" href="#">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife " href="/">Accueil</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife" href="/qui-sommes-nous">Qui sommes nous ?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/nos-solutions">Nos solutions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife"  href="/login">Rejoindre le Réseau</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/contact">Contacts</a>
                    </li>
                    <li class="nav-item itemConnecte">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" class="form-control" />
                            </div>
                            <button type="button" class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="blockImgHead">
        <img src="{{asset('img/solutionHead.png')}}" alt="">
        <h1 class="textHeadQsm">Actualités</h1>
    </div>
    <div class="content-Actuality">
        <div class="container">
            <div class="list-wrapper">
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-1.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-2.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-3.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-4.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
                <div class="list-item">
                    <div class="content">
                        <img class="imgContentCard" src="{{asset('img/Image-5.png')}}" alt="">
                        <div class="textActuality">
                            <a href="" class="linkElement">Esquire.com</a>
                            <h3>Sorry Vape Kings and Queens: The Surgeon General Just Recommended an Indoor Vape Ban</h3>
                            <p>You'll have to take those milky clouds outside.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="pagination-container"></div>
       </div>
    </div>
    <div class="blockConfiance">
        <div class="container">
            <h2 class="textConfiance">Ils nous ont fait confiance !</h2>
            <h3 class="bigText">Big brands, small bussiness, new startuo and inividuals</h3>
            <div class="imgPartener">
                <img src="{{asset('img/schneider-electric.png')}}" alt="">
                <img src="{{asset('img/nexans.png')}}" alt="">
                <img src="{{asset('img/abb.png')}}" alt="">
                <img src="{{asset('img/signify.png')}}" alt="">
                <img src="{{asset('img/schneider-electric.png')}}" alt="">
                <img src="{{asset('img/signify.png')}}" alt="">
            </div>
        </div>
    </div>
    <div class="blockRejoindre">
        <div class="container">
            <div class="elementContentRejoindre">
                <div class="block1">
                    <div class="dflex">
                        <p class="textFaire2">Nous Rejoindre</p>
                       <div class="position-relative">
                           <hr class="hrFaire2">
                       </div>
                    </div>
                    <h3 class="rejoindreText">Prêt.e à rejoindre votre <span>communauté </span>?</h3>
                    <h4 class="ayezText">Ayez les meilleures expérience d’échanges avec les professionnels de votre domaine.</h4>
                    <a href="/login" class="btn btnRejoindreWelcome">Nous Rejoindre</a>
                </div>
                <div class="block2">
                    <div class="contentImgPattern">
                        <img class="patternOrange" src="{{asset('img/patternOrange.png')}}" alt="">
                        <img class="patterHomme" src="{{asset('img/portraitHomme.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.min.js"></script>
<script>


    var items = $(".list-wrapper .list-item");
    var numItems = items.length;
    var perPage = 9;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
</script>
@endsection
@endsection