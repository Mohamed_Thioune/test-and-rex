<html>
<head>
    <meta content="TARIF" property="og:title">
    <meta content="TARIF" property="twitter:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="stylesheet" href="{{asset('bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('swiper/swiper-bundle.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/login.css')}}">
</head>
<body>
<div class="contentLogin">
    <div class="container">
        <div class="bulImg">
        </div>
        <img class="vector5" src="{{asset('img/Vector-5.png')}}" alt="">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-login">
            <a class="navbar-brand navLogo" href="#">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="#">Aide</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="#">Nous contacter</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link nav-linkModife dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Français
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Anglais</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="blockContentElement">
           <div class="blockOne">
               <p class="ravisText hide1 hide2">CREEZ VOTRE COMPTE</p>
               <p class="pasEncore hide1 hide2">Vous avez déja compte ? <a href="">Se connecter</a></p>
                   <div class="blockTabsAccount hide2 hide1">
                       <button class="btn btnUtilisateur activeBtn">Utilisateur d’Equipements</button>
                       <button class="btn btnFabricant">Fabricant</button>
                   </div>
                   <div class="blockContentUtilisateurEquipemennt">
                       <!--formulaire pour utilisateurs  -->
                       <form class="formAccount formAccountOne formUser"  action="">
                           <div class="form-group hide1 colM">
                               <label class="labelAccount" for="entreprise1">Entreprise</label>
                               <input type="text" class="form-control inputUtilisateur inputAccount" id="entreprise1" required>
                           </div>
                           <div class="form-group hide1 colM">
                               <label class="labelAccount"  for="activite1">Domaine d’activité</label>
                               <select class="form-control inputAccount inputUtilisateur selectModife" id="activite1" required>
                                   <option></option>
                                   <option>1</option>
                                   <option>2</option>
                               </select>
                               <i class="fa fa-chevron-down"></i>
                           </div>
                           <div class="form-group hide1 colL">
                               <label class="labelAccount"  for="pays1">Pays</label>
                               <select class="form-control inputAccount inputUtilisateur selectModife1" id="pays1" required>
                                   <option></option>
                                   <option>1</option>
                                   <option>2</option>
                               </select>
                           </div>
                           <div class="form-group hide1 colM">
                               <label class="labelAccount"  for="email1">Email</label>
                               <input type="email" class="form-control inputUtilisateur inputAccount" id="email1" required>
                           </div>
                           <div class="form-group hide1 colM">
                               <label class="labelAccount"  for="telephone1">Téléphone</label>
                               <input type="number" class="form-control inputUtilisateur inputAccount" id="telephone1" required>
                           </div>
                           <div class="form-group hide1 colL">
                               <label class="labelAccount"  for="password1">Mot de passe</label>
                               <input type="password" class="form-control inputUtilisateur inputAccount" id="password1" required>
                           </div>
                           <div class="elementCont hide1">
                               <div class="groupContinuer">
                                    <button type="submit" class="btn btn-connecter" id="continuerEntreprise">Continuer<img class="flecheBtn" src="{{asset('img/flecheBnt.png')}}" alt=""></button>
                               </div>
                               <p class="ouContinuer"><span>ou continuer avec</span></p>
                               <div class="btnSociauxblock">
                                   <button class="btn btnsociaux">
                                       <img src="{{asset('img/google.png')}}" alt="">
                                   </button>
                                   <button class="btn btnsociaux">
                                       <img src="{{asset('img/facebook.png')}}" alt="">
                                   </button>
                                   <button class="btn btnsociaux">
                                       <img src="{{asset('img/linkedin.png')}}" alt="">
                                   </button>
                               </div>
                           </div>

                           <!-- suite formulaire pour utilisateurs d equipement -->
                           <div class="blokOneBis">
                               <div class="formEquipement3">
                                   <div class="blockElementequipement">
                                       <button id="btnRetour1" class="btn btnRetour"> <img src="{{asset('img/retourImg.png')}}" alt=""></button>
                                       <p class="utilisateurEquipement">UTILISATEURS D’EQUIPEMENTS</p>
                                   </div>
                                   <p class="chooseProfil">Choisir votre profil : </p>
                                   <div class="form-group coll blockEmployeEntreprise">
                                       <label class="radioModife">Employé
                                           <input type="radio" id="employeRadio" checked="checked" name="radio" required>
                                           <span class="checkmark"></span>
                                       </label>
                                       <label class="radioModife">Entreprise
                                           <input type="radio" id="entrepriseRadio" name="radio" required>
                                           <span class="checkmark"></span>
                                       </label>
                                   </div>
                                   <div class="form-group colM">
                                       <label class="labelAccount"  for="Prenom2">Prénom</label>
                                       <input type="text" class="form-control inputAccount" id="Prenom2" required>
                                   </div>
                                   <div class="form-group colM">
                                       <label class="labelAccount"  for="Nom2">Nom</label>
                                       <input type="text" class="form-control inputAccount" id="Nom2" required>
                                   </div>
                                   <div class="form-group colL">
                                       <label class="labelAccount"  for="pays1">Fonction</label>
                                       <select class="form-control inputAccount selectModife1" id="pays1">
                                           <option></option>
                                           <option>1</option>
                                           <option>2</option>
                                       </select>
                                   </div>
                                   <div class="elementCont">
                                       <div class="groupContinuer">
                                           <button type="submit" class="btn btn-connecter">Terminer</button>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </form>

                       <!--formulaire pour utilisateurs fabriquant -->
                       <form action="" class="formAccount formEquipementOne">
                           <div class="form-group hide2 colM">
                               <label class="labelAccount" for="entreprise2">Entreprise</label>
                               <input type="text" class="form-control inputUtilisateur2 inputAccount" id="entreprise2" required>
                           </div>
                           <div class="form-group hide2 colM">
                               <label class="labelAccount"  for="activite2">Domaine d’activité</label>
                               <select class="form-control inputUtilisateur2 inputAccount selectModife" id="activite1" required>
                                   <option></option>
                                   <option>1</option>
                                   <option>2</option>
                               </select>
                               <i class="fa fa-chevron-down"></i>
                           </div>
                           <div class="form-group hide2 colL">
                               <label class="labelAccount"  for="zone">Zone Cible</label>
                               <select class="form-control inputUtilisateur2 inputAccount selectModife1" id="zone" required>
                                   <option></option>
                                   <option>1</option>
                                   <option>2</option>
                               </select>
                           </div>
                           <div class="form-group hide2 colM">
                               <label class="labelAccount"  for="email2">Email</label>
                               <input type="email" class="form-control inputUtilisateur2 inputAccount" id="email1" required>
                           </div>
                           <div class="form-group hide2 colM">
                               <label class="labelAccount"  for="telephone2">Téléphone</label>
                               <input type="number" class="form-control inputUtilisateur2 inputAccount" id="telephone2" required>
                           </div>
                           <div class="form-group hide2 colL">
                               <label class="labelAccount"  for="password2">Mot de passe</label>
                               <input type="password" class="form-control inputAccount" id="password2" required>
                           </div>
                           <div class="elementCont hide2">
                               <div class="groupContinuer">
                                   <button type="submit" id="continuerFabriquant" class="btn btn-connecter">Continuer <img class="flecheBtn" src="{{asset('img/flecheBnt.png')}}" alt=""></button>
                               </div>
                               <p class="ouContinuer"><span>ou continuer avec</span></p>
                               <div class="btnSociauxblock">
                                   <button class="btn btnsociaux">
                                       <img src="{{asset('img/google.png')}}" alt="">
                                   </button>
                                   <button class="btn btnsociaux">
                                       <img src="{{asset('img/facebook.png')}}" alt="">
                                   </button>
                                   <button class="btn btnsociaux">
                                       <img src="{{asset('img/linkedin.png')}}" alt="">
                                   </button>
                               </div>
                           </div>

                           <!-- suite formulaire pour utilisateurs FABRICANT -->
                           <div class="blokOneFabriquant">
                               <div class="blockElementequipement">
                                   <button id="btnRetour2" class="btn btnRetour"> <img src="{{asset('img/retourImg.png')}}" alt=""></button>
                                   <p class="utilisateurEquipement">FABRICANT</p>
                               </div>
                               <p class="chooseProfil">Choisir votre profil : </p>
                               <div class="form-group coll blockEmployeEntreprise">
                                   <label class="radioModife">Service Commercial
                                       <input type="radio" id="ServiceCommercial" checked="checked" name="radio" required>
                                       <span class="checkmark"></span>
                                   </label>
                                   <label class="radioModife">Service Communication
                                       <input type="radio" id="ServiceCommunication" name="radio" required>
                                       <span class="checkmark"></span>
                                   </label>
                               </div>
                               <div class="form-group colM">
                                   <label class="labelAccount"  for="Prenom2">Prénom</label>
                                   <input type="text" class="form-control inputAccount" id="Prenom2" required>
                               </div>
                               <div class="form-group colM">
                                   <label class="labelAccount"  for="Nom2">Nom</label>
                                   <input type="text" class="form-control inputAccount" id="Nom2" required>
                               </div>
                               <div class="form-group colL">
                                   <label class="labelAccount"  for="pays1">Fonction</label>
                                   <select class="form-control inputAccount selectModife1" id="pays1">
                                       <option></option>
                                       <option>1</option>
                                       <option>2</option>
                                   </select>
                               </div>
                               <div class="elementCont">
                                   <div class="groupContinuer">
                                       <button type="submit" class="btn btn-connecter">Terminer</button>
                                   </div>
                               </div>
                           </div>
                       </form>
                   </div>
           </div>


            <div class="secondblock">
                <div class="imgManuski">
                    <img src="{{asset('img/MaskIllust.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('js/style.js')}}"></script>
</body>
