<div class="table-responsive">
    <table class="table" id="reactions-table">
        <thead>
            <tr>
                <th>Value</th>
        <th>Type</th>
        <th>User Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($reactions as $reaction)
            <tr>
                <td>{{ $reaction->value }}</td>
            <td>{{ $reaction->type }}</td>
            <td>{{ $reaction->user_id }}</td>
                <td>
                    {!! Form::open(['route' => ['reactions.destroy', $reaction->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('reactions.show', [$reaction->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('reactions.edit', [$reaction->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
