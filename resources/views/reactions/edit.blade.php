@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Reaction
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($reaction, ['route' => ['reactions.update', $reaction->id], 'method' => 'patch']) !!}

                        @include('reactions.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection