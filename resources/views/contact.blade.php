@extends('layouts.linkFront')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.4/simplePagination.css" integrity="sha512-emkhkASXU1wKqnSDVZiYpSKjYEPP8RRG2lgIxDFVI4f/twjijBnDItdaRh7j+VRKFs4YzrAcV17JeFqX+3NVig==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="{{asset('css/pagination.css')}}">

@endsection

@section('content-body-front')
<div class="contenQuiSommesNous">
    <div class="blockNav">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
            <a class="navbar-brand navLogo" href="/">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife " href="/">Accueil</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife" href="/qui-sommes-nous">Qui sommes nous ?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/nos-solutions">Nos solutions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife"  href="/login">Rejoindre le Réseau</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/contact">Contacts</a>
                    </li>
                    <li class="nav-item itemConnecte">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" class="form-control" />
                            </div>
                            <button type="button" class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="blockImgHead">
        <img src="{{asset('img/solutionHead.png')}}" alt="">
        <h1 class="textHeadQsm">Contact</h1>
    </div>
    <div class="container">
        <div class="blockContact">
            <img class="contactIconImg" src="{{asset('img/contact_icon.png')}}" alt="">
            <p class="contactSupportText">Contact & Support</p>
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="cardAdressContact">
                        <p class="title">Adresse</p>
                        <hr>
                        <p class="description">13, Rue de la Remise aux Faisans
                            94600, CHOISY-LE-ROI
                            FRANCE</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="cardAdressContact">
                        <p class="title">Contacts</p>
                        <hr>
                        <p class="descriptionContact"><b>Fix</b> : +33 (0) 9 87 30 64 98</p>
                        <p class="descriptionContact"><b>Tel</b> : +33 (0) 6 68 61 79 22</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="cardAdressContact">
                        <p class="title">Mail</p>
                        <hr>
                        <p class="description">contact@testandrex.com</p>
                        <p class="description">support-technique@testandrex.com</p>
                    </div>
                </div>
            </div>
            <div class="blockCardMap">
                <img src="{{asset('img/map.png')}}" alt="">
            </div>
            <p class="contactSupportText">Formulaire</p>
            <form action="{{route('warning')}}" method="POST" class="formContact">
                @csrf
                <div class="form-group colM">
                    <label class="labelAccount" for="prenomContact">Prénom *</label>
                    <input type="text" class="form-control inputAccount" id="prenomContact" name="firstName" required>
                </div>
                <div class="form-group colM">
                    <label class="labelAccount" for="NomContact">Nom *</label>
                    <input type="text" class="form-control inputAccount" id="NomContact" name="lastName" required>
                </div>
                <div class="form-group colM">
                    <label class="labelAccount" for="emailContact">Votre e-mail *</label>
                    <input type="email" class="form-control inputAccount" id="emailContact" name="email" required>
                </div>
                <div class="form-group colM">
                    <label class="labelAccount" for="objetMessageContact">Objet du message *</label>
                    <select id="objetMessageContact" class="form-control inputAccount inputUtilisateur selectModife" name="object" required>
                        <option value="0"></option>
                        <option value="1">Infos formation</option>
                        <option value="2">réclamation</option>
                    </select>
                </div>
                <div class="form-group colM">
                    <label class="labelAccount" for="telephoneContat">Telephone</label>
                    <input type="number" class="form-control inputAccount" id="telephoneContat" name="phone" required>
                </div>
                <div class="form-group colM">
                    <label class="labelAccount" for="paysContact">Pays</label>
                    <select id="paysContact" class="form-control inputAccount inputUtilisateur selectModife" name="country" required>
                        <option value="Algérie">Algérie</option>                                            
                        <option value="Bénin">Bénin</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Cap-Vert">Cap-Vert</option>
                        <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                        <option value="Gambie">Gambie</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Guinée">Guinée</option>
                        <option value="Guinée Bissau">Guinée Bissau</option>
                        <option value="Libéria">Libéria</option>
                        <option value="Mali">Mali</option>
                        <option value="Maroc">Maroc</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigéria">Nigéria</option>
                        <option value="Sénégal">Sénégal</option>
                        <option value="Sierra Léone">Sierra Léone</option>
                        <option value="Togo">Togo</option>
                        <option value="Tunisie">Tunisie</option>
                        <option value="UAE">UAE</option>
                        <option value="USA">USA</option>
                    </select>
                </div>
                <div class="form-group colL">
                    <label class="labelAccount" for="messageContact">Votre message</label>
                    <textarea name="content" id="messageContact" rows="7" placeholder="Ecrire ici votre message ..."></textarea>
                </div>
                <button class="btn btnEnvoyer">Envoyer</button>
            </form>
        </div>
        <div class="blockConfiance">
            <div class="container">
                <h2 class="textConfiance">Ils nous font confiance</h2>
                <h3 class="bigText">Grandes et petites entreprises, nouvelles Start-up et individuels font partis de nos clients</h3>
                <div class="imgPartener">
                    <img src="{{asset('img/schneider-electric.png')}}" alt="">
                    <img src="{{asset('img/nexans.png')}}" alt="">
                    <img src="{{asset('img/abb.png')}}" alt="">
                    <img src="{{asset('img/signify.png')}}" alt="">
                    <img src="{{asset('img/schneider-electric.png')}}" alt="">
                    <img src="{{asset('img/signify.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="blockRejoindre">
        <div class="container">
            <div class="elementContentRejoindre">
                <div class="block1">
                    <div class="dflex">
                        <p class="textFaire2">Nous Rejoindre</p>
                        <div class="position-relative">
                            <hr class="hrFaire2">
                        </div>
                    </div>
                    <h3 class="rejoindreText">Prêt à rejoindre <span>les experts</span>?</h3>
                    <h4 class="ayezText">Ayez les meilleures expériences d’échange avec des experts électriciens.</h4>
                    <a href="/login" class="btn btnRejoindreWelcome">Nous Rejoindre</a>
                </div>
                <div class="block2">
                    <div class="contentImgPattern">
                        <img class="patternOrange" src="{{asset('img/patternOrange.png')}}" alt="">
                        <img class="patterHomme" src="{{asset('img/portraitHomme.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('layouts.footerLayaout')
    </div>
</div>

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.min.js"></script>

@endsection
@endsection