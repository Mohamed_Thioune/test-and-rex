<div class="blockFooter">
    <div class="container">
        <img class="logoFooter" src="{{asset('img/logo.png')}}" alt="">
        <div class="navFooter">
            <a class="nav-temFooter" href="/">Accueil</a>
            <a class="nav-temFooter" href="/qui-sommes-nous">Qui sommes nous ?</a>
            <a class="nav-temFooter" href="/nos-solutions">Nos solutions</a>
            <a class="nav-temFooter" href="/login">Rejoignez-la communaté</a>
            <a class="nav-temFooter" href="">Contacts</a>
        </div>
        <p class="textCopyright"><a href="">copyright (©)</a> 2021 TEST&REX.</p>
    </div>
</div>