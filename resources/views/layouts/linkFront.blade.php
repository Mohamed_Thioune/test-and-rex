<html>
<head>
    <title>TEST&REX</title>
    <meta content="TEST&REX, les experts électriciens vous accompagnent" property="og:title">
    <meta content="TEST&REX, les experts électriciens vous accompagnent" property="twitter:title">
    <meta name="description" content="À travers nos services, faites connaître vos solutions, partagez vos retours d'expérience, faites monter en compétence vos équipes grâce à un réseau d'experts électriciens confirmés.">

    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="stylesheet" href="{{asset('bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('owl-carousel/owl.theme.default.css')}}">
    <link rel="stylesheet" href="{{asset('owl-carousel/owl.theme.green.css')}}">
    @yield('css')
</head>
<body>
@yield('content-body-front')


<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/popper.min.js')}}"></script>
<script src="{{asset('bootstrap/bootstrap.min.js')}}"></script>
<script src="{{asset('owl-carousel/owl.carousel.js')}}"></script>
<script src="{{asset('owl-carousel/owl.navigation.js')}}"></script>
<script src="{{asset('js/style.js')}}"></script>
<script src="{{asset('js/platforme.js')}}"></script>
@yield('scripts')
</body>
