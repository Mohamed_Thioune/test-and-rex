<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('employees*') ? 'active' : '' }}">
    <a href="{{ route('employees.index') }}"><i class="fa fa-edit"></i><span>Employees</span></a>
</li>

<li class="{{ Request::is('enterprises*') ? 'active' : '' }}">
    <a href="{{ route('enterprises.index') }}"><i class="fa fa-edit"></i><span>Enterprises</span></a>
</li>

<li class="{{ Request::is('events*') ? 'active' : '' }}">
    <a href="{{ route('events.index') }}"><i class="fa fa-edit"></i><span>Events</span></a>
</li>

<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{{ route('products.index') }}"><i class="fa fa-edit"></i><span>Products</span></a>
</li>

<li class="{{ Request::is('posts*') ? 'active' : '' }}">
    <a href="{{ route('posts.index') }}"><i class="fa fa-edit"></i><span>Posts</span></a>
</li>

<li class="{{ Request::is('comments*') ? 'active' : '' }}">
    <a href="{{ route('comments.index') }}"><i class="fa fa-edit"></i><span>Comments</span></a>
</li>

<li class="{{ Request::is('reactions*') ? 'active' : '' }}">
    <a href="{{ route('reactions.index') }}"><i class="fa fa-edit"></i><span>Reactions</span></a>
</li>

<li class="{{ Request::is('responseComments*') ? 'active' : '' }}">
    <a href="{{ route('responseComments.index') }}"><i class="fa fa-edit"></i><span>Response Comments</span></a>
</li>
<li class="{{ Request::is('files*') ? 'active' : '' }}">
    <a href="{{ route('files.index') }}"><i class="fa fa-edit"></i><span>Files</span></a>
</li>

<li class="{{ Request::is('advertisements*') ? 'active' : '' }}">
    <a href="{{ route('advertisements.index') }}"><i class="fa fa-edit"></i><span>Advertisements</span></a>
</li>

<li class="{{ Request::is('announces*') ? 'active' : '' }}">
    <a href="{{ route('announces.index') }}"><i class="fa fa-edit"></i><span>Announces</span></a>
</li>

