@extends('layouts.linkFront')
<!--@section('css')
@endsection-->


@section('content-body-front')
<div class="contenQuiSommesNous">

<div class="blockNav">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
            <a class="navbar-brand navLogo" href="/">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife " href="/">Accueil</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife" href="/qui-sommes-nous">Qui sommes nous ?</a>
                    </li>
                    <li class="nav-item activeNav">
                        <a class="nav-link nav-linkModife" href="/nos-solutions">Nos solutions</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife"  href="/login">Rejoindre le Réseau</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/contact">Contact</a>
                    </li>
                    <li class="nav-item itemConnecte">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" class="form-control" />
                            </div>
                            <button type="button" class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="blockImgHead">
        <img src="{{asset('img/solutionHead.png')}}" alt="">
        <h1 class="textHeadQsm">Nos Solutions</h1>
    </div>
    <div class="container">
        <div class="blockAdapte">
           <div class="textBlockHead">
               <h2 class="solutionsAdapte">Des solutions adaptées
                   à vos besoins</h2>
           </div>
           <div class="row">
               <div class="col-lg-6 col-md-12">
                   <div class="blockIntroService">
                       <div class="blockHr">
                           <hr>
                           <hr>
                       </div>
                       <p>Vous êtes <span>UTILISATEUR,</span> client final de fabricant d'infrastructures des réseaux électriques ou d'instruments de test et mesures électriques, TEST&REX vous propose:</p>
                   </div>
                   <div class="contentCardSolutions">
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Formation test et mesures électriques </p>
                               <p class="textcontentCardServie">A travers son réseau d'experts techniques, TEST&REX vous offre des formations " sur mesure " ...</p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Veille technologique et Support Technique Général  </p>
                               <p class="textcontentCardServie">Les nouveautés sur le marché ...</p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Expertise-Accompagnement pour montée en compétence technique</p>
                               <p class="textcontentCardServie">Un plan d'accompagnement qui favorise la capitalisation des savoirs ...</p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Assistance Projet d'acquisition </p>
                               <p class="textcontentCardServie">Dans le cadre de vos projets d'acquisition d'infrastructures des réseaux électriques ... </p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-lg-6 col-md-12">
                   <div class="blockIntroService">
                       <div class="blockHr">
                           <hr>
                           <hr>
                       </div>
                       <p>Vous êtes <span>FABRICANT</span>  d'infrastuctures des réseaux électriques ou d'instruments de test et mesures électriques, TEST&REX vous propose:</p>
                   </div>
                   <div class="contentCardSolutions">
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Un réseau professionnel pour votre communication média</p>
                               <p class="textcontentCardServie">A travers la plateforme "TEST&REX-electric", communiquez ...</p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Prescription de Marque et Promotion de portefeuille </p>
                               <p class="textcontentCardServie">Profitez pleinement de la plateforme " TEST&REXelectric", un espace marketing ...</p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Étude de marché</p>
                               <p class="textcontentCardServie">Vous souhaitez être leader dans votre secteur d'activité ...</p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                       <div class="cardServiceHome">
                           <img class="imgCard" src="{{asset('img/imgSlide2.png')}}" alt="">
                           <div class="colorElement">
                               <div class="maskGroupImg">
                                   <img  src="{{asset('img/mask.png')}}" alt="">
                               </div>
                               <p class="contentCardServie">Business Development Strategy Consulting </p>
                               <p class="textcontentCardServie">Grâce à l'appui de ses experts ...</p>
                               <a href="/nos-solutions" class="btnSavoirPLus"><span>Lire la suite</span><img src="{{asset('img/chevrondown.svg')}}" alt=""></a>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    </div>

    <div class="blockConfiance">
        <div class="container">
            <h2 class="textConfiance">Ils nous font confiance</h2>
            <h3 class="bigText">Grandes et petites entreprises, nouvelles Start-up et individuels font partis de nos clients</h3>
            <div class="imgPartener">
                <img src="{{asset('img/schneider-electric.png')}}" alt="">
                <img src="{{asset('img/nexans.png')}}" alt="">
                <img src="{{asset('img/abb.png')}}" alt="">
                <img src="{{asset('img/signify.png')}}" alt="">
                <img src="{{asset('img/schneider-electric.png')}}" alt="">
                <img src="{{asset('img/signify.png')}}" alt="">
            </div>
        </div>
    </div>
    <div class="blockRejoindre">
        <div class="container">
            <div class="elementContentRejoindre">
                <div class="block1">
                    <div class="dflex">
                        <p class="textFaire2">Nous Rejoindre</p>
                        <div class="position-relative">
                            <hr class="hrFaire2">
                        </div>
                    </div>
                    <h3 class="rejoindreText">Prêt à rejoindre <span>les experts</span>?</h3>
                    <h4 class="ayezText">Ayez les meilleures expériences d'échange avec des experts électriciens.</h4>
                    <a href="/login" class="btn btnRejoindreWelcome">Nous Rejoindre</a>
                </div>
                <div class="block2">
                    <div class="contentImgPattern">
                        <img class="patternOrange" src="{{asset('img/patternOrange.png')}}" alt="">
                        <img class="patterHomme" src="{{asset('img/portraitHomme.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('layouts.footerLayaout')
    </div>
</div>

<!--@section('scripts')
@endsection-->
@endsection