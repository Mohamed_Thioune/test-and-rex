<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $employee->id }}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstName', 'Firstname:') !!}
    <p>{{ $employee->firstName }}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastName', 'Lastname:') !!}
    <p>{{ $employee->lastName }}</p>
</div>

<!-- Enterprise Field -->
<div class="form-group">
    {!! Form::label('enterprise', 'Enterprise:') !!}
    <p>{{ $employee->enterprise }}</p>
</div>

<!-- Expertisedomain Field -->
<div class="form-group">
    {!! Form::label('expertiseDomain', 'Expertisedomain:') !!}
    <p>{{ $employee->expertiseDomain }}</p>
</div>

<!-- Function Field -->
<div class="form-group">
    {!! Form::label('function', 'Function:') !!}
    <p>{{ $employee->function }}</p>
</div>

<!-- Username Field -->
<div class="form-group">
    {!! Form::label('username', 'Username:') !!}
    <p>{{ $employee->username }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $employee->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $employee->updated_at }}</p>
</div>

