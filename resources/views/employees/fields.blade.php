<!-- Firstname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('firstName', 'Firstname:') !!}
    {!! Form::text('firstName', null, ['class' => 'form-control']) !!}
</div>

<!-- Lastname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('lastName', 'Lastname:') !!}
    {!! Form::text('lastName', null, ['class' => 'form-control']) !!}
</div>

<!-- Enterprise Field -->
<div class="form-group col-sm-6">
    {!! Form::label('enterprise', 'Enterprise:') !!}
    {!! Form::text('enterprise', null, ['class' => 'form-control']) !!}
</div>

<!-- Expertisedomain Field -->
<div class="form-group col-sm-6">
    {!! Form::label('expertiseDomain', 'Expertisedomain:') !!}
    {!! Form::text('expertiseDomain', null, ['class' => 'form-control']) !!}
</div>

<!-- Function Field -->
<div class="form-group col-sm-6">
    {!! Form::label('function', 'Function:') !!}
    {!! Form::text('function', null, ['class' => 'form-control']) !!}
</div>

<!-- Username Field -->
<div class="form-group col-sm-6">
    {!! Form::label('username', 'Username:') !!}
    {!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('employees.index') }}" class="btn btn-default">Cancel</a>
</div>
