<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $enterprise->id }}</p>
</div>

<!-- Enterprise Field -->
<div class="form-group">
    {!! Form::label('enterprise', 'Enterprise:') !!}
    <p>{{ $enterprise->enterprise }}</p>
</div>

<!-- Activitydomain Field -->
<div class="form-group">
    {!! Form::label('activityDomain', 'Activitydomain:') !!}
    <p>{{ $enterprise->activityDomain }}</p>
</div>

<!-- Targetarea Field -->
<div class="form-group">
    {!! Form::label('targetArea', 'Targetarea:') !!}
    <p>{{ $enterprise->targetArea }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $enterprise->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $enterprise->updated_at }}</p>
</div>

