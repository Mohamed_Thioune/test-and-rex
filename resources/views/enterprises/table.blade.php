<div class="table-responsive">
    <table class="table" id="enterprises-table">
        <thead>
            <tr>
                <th>Enterprise</th>
                <th>Targetarea</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($enterprises as $enterprise)
            <tr>
                @php
                    if($enterprise->user_id ==  21)
                        continue;
                @endphp
                <td>{{ $enterprise->enterprise }}</td>
                <td>{{ $enterprise->targetArea }}</td>
                <td>
                    {!! Form::open(['route' => ['enterprises.destroy', $enterprise->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('enterprises.show', [$enterprise->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('enterprises.edit', [$enterprise->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
