<!-- Enterprise Field -->
<div class="form-group col-sm-6">
    {!! Form::label('enterprise', 'Enterprise:') !!}
    {!! Form::text('enterprise', null, ['class' => 'form-control']) !!}
</div>

<!-- Activitydomain Field -->
<div class="form-group col-sm-6">
    {!! Form::label('activityDomain', 'Activitydomain:') !!}
    {!! Form::text('activityDomain', null, ['class' => 'form-control']) !!}
</div>

<!-- Targetarea Field -->
<div class="form-group col-sm-6">
    {!! Form::label('targetArea', 'Targetarea:') !!}
    {!! Form::text('targetArea', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('enterprises.index') }}" class="btn btn-default">Cancel</a>
</div>
