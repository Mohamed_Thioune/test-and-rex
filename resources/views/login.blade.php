@php
Auth::logout();
@endphp

@extends('layouts.linkFront')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.4/simplePagination.css" integrity="sha512-emkhkASXU1wKqnSDVZiYpSKjYEPP8RRG2lgIxDFVI4f/twjijBnDItdaRh7j+VRKFs4YzrAcV17JeFqX+3NVig==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="{{asset('css/login.css')}}">

@endsection

@section('content-body-front')
<div class="contentAccount back1">
    <div class="blockNav">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
            <a class="navbar-brand navLogo" href="/">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife" href="{{ route('welcome') }}">Accueil</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife" href="/qui-sommes-nous">Qui sommes nous ?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/nos-solutions">Nos solutions</a>
                    </li>
                    <li class="nav-item activeNav">
                        <a class="nav-link nav-linkModife"  href="/account">Rejoindre le Réseau</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/contact">Contacts</a>
                    </li>
                    <li class="nav-item itemConnecte">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" class="form-control" />
                            </div>
                            <button type="button" class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="headTabAccount">
            <button id="btnTabsConnexion" class="btn activeBtn">Connexion</button>
            <button id="btnTabsInscription" class="btn">Inscription</button>
        </div>
        <div class="contentTabAccount">
            <div class="contentLogin">
               
                <div class="formblock">
                    @include('flash::message')
                    @include('adminlte-templates::common.errors')
                    <form class="formAccount formUser" method="POST" action="{{ url('/login') }}">
                        @csrf
                       <div class="form-group coll has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                           <label class="labelAccount" for="emailLogin">E-mail</label>
                           <input type="email" class="form-control inputAccount" id="emailLogin" name="email" required>
                           @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                       </div>
                       <div class="form-group coll has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                           <label class="labelAccount"  for="passwoordLogin">Mot de passe</label>
                           <input type="password" class="form-control inputAccount" id="passwoordLogin" name="password" required>
                           @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                           @endif
                       </div>
                       <div class="elementCheck">
                           <div>
                               <label class="radioModife">Se souvenir de moi
                                   <input type="checkbox" id="Sesouvenirdemoi" name="remember">
                                   <span class="checkmark"></span>
                               </label>
                           </div>
                           <a href="{{ url('/password/reset') }}" class="motdepassOublie">Mot de passe oublié ?</a>
                       </div>
                        <div class="elementCont">
                            <div class="groupContinuer">
                                <button type="submit" class="btn btn-connecter" id="Seconnecter">Se connecter</button>
                            </div>
                            {{-- <p class="ouContinuer"><span>ou continuer avec</span></p>
                            <div class="btnSociauxblock">
                                <button class="btn btnsociaux">
                                    <img src="{{asset('img/google.png')}}" alt="">
                                </button>
                                <button class="btn btnsociaux">
                                    <img src="{{asset('img/facebook.png')}}" alt="">
                                </button>
                                <button class="btn btnsociaux">
                                    <img src="{{asset('img/linkedin.png')}}" alt="">
                                </button>
                            </div> --}}
                        </div>
                    </form>
                </div>
                <div class="blockImg">
                    <img src="{{asset('img/ce2.png') }}" alt="">
                </div>
            </div>
            <div class="contentInscription">
                <div class="blockChooseProfil">
                    <p class="votreProfil">Votre profil :</p>
                    <div class="contentChooseProfil">
                        <div class="cardEquipement" id="sample">
                            <div class="userequipementImg">
                                <img src="{{asset('img/user8.png') }}" alt="">
                            </div>
                            <p class="clientText"> Vous êtes client final de fabricant, UTILISATEUR d'infrastructures des réseaux électriques ou d'instruments de test et mesures électriques</p>
                            <p class="utilisateurEquipementText">UTILISATEUR D'EQUIPEMENTS</p>
                            <button id="contunierEquipement" class="btn btnContinuerIci">Continuer ici
                                <img src=" {{asset('img/chevronPlus.png') }}" alt="">
                            </button>
                        </div>
                        <div class="cardEquipement" id="fabricant">
                            <div class="userequipementImg">
                                <img src="{{asset('img/fabriquant8.png') }}"  alt="">
                            </div>
                            <p class="clientText">Vous êtes FABRICANT d'infrastructures des réseaux électriques ou d'équipement de test et mesure électriques</p>
                            <p class="utilisateurEquipementText">FABRICANT</p>
                            <button id="btnContinuerFabriquant" class="btn btnContinuerIci">Continuer ici
                                <img src=" {{asset('img/chevronPlus.png') }}" alt="">
                            </button>
                        </div>
                    </div>
<!--                    block des formulaires-->
                    <form class="formeAccount" method="POST" action="{{ url('/register') }}">
                        @csrf
                        <input type="hidden" id="type" name="type" value="">
                        <div class="blockUtilisationEquipement">
                            <div class="blockElementequipement">
                                <button id="btnRetour1" class="btn btnRetour"> <img src="{{asset('img/retourImg.png')}}" alt=""></button>
                                <p class="utilisateurEquipement">UTILISATEURS D'EQUIPEMENTS</p>
                            </div>
                            <div class="cardEquipement2">
                                <div class="blockInput">
                                    <input type="hidden" name="type" value="sample">
                                    <div class="form-group hide1 colL has-feedback {{ $errors->has('profile') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="profil1">Quel est votre profile ? <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <select id="select1" class="form-control inputAccount inputUtilisateur selectModife" name="profile" required>
                                            <option value="0"></option>
                                            <option value="1">Electricien, employé d'une entreprise</option>
                                            <option value="2">Entreprise</option>
                                        </select>
                                        @if ($errors->has('profile'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profile') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                    <div class="form-group hide1 colM inputSelectHide has-feedback {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="PrenomEmploye">Prénom <span style="color:#e74c3c">&nbsp;(Obligatoire)</span> </label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="PrenomEmploye" name="firstName" value=" ">
                                        @if ($errors->has('firstName'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('firstName') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                    <div class="form-group hide1 colM inputSelectHide has-feedback {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="nomEmploye">Nom <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="nomEmploye" name="lastName" value=" ">
                                            @if ($errors->has('lastName'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('lastName') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group hide1 colM has-feedback {{ $errors->has('enterprise') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="Entreprise1">Entreprise <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="Entreprise1" name="enterprise" required>
                                        @if ($errors->has('enterprise'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('enterprise') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colM has-feedback {{ $errors->has('expertiseDomain') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="activite1">Domaine d'activité</label>
                                        <select class="form-control inputAccount inputUtilisateur selectModife1" id="activite1" name="expertiseDomain" required>
                                            <option value="Electricité et réseaux électriques">Electricité et réseaux électriques</option>
                                            <option value="Sécurité des installations électriques">Sécurité des installations électriques</option>
                                            <option value="Industries (mines, pétroles, sucrerie, etc...)">Industries (mines, pétroles, sucrerie, etc...)</option>
                                            <option value="Prestation de services">Prestation de services</option>
                                            <option value="Maintenance des infrastructures électriques">Maintenance des infrastructures électriques</option>
                                            <option value="Installation/construction d'infrastructures électriques">Installation/construction d'infrastructures électriques</option>
                                        </select>
                                        @if ($errors->has('expertiseDomain'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('expertiseDomain') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colL has-feedback {{ $errors->has('country') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="Pays1">Pays</label>
                                        <select id="" class="form-control inputAccount inputUtilisateur selectModife" name="country" id="Pays1" required>
                                            <option value="Algérie">Algérie</option>                                            
                                            <option value="Bénin">Bénin</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Cap-Vert">Cap-Vert</option>
                                            <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                                            <option value="Gambie">Gambie</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Guinée">Guinée</option>
                                            <option value="Guinée Bissau">Guinée Bissau</option>
                                            <option value="Libéria">Libéria</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Maroc">Maroc</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigéria">Nigéria</option>
                                            <option value="Sénégal">Sénégal</option>
                                            <option value="Sierra Léone">Sierra Léone</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tunisie">Tunisie</option>
                                            <option value="UAE">UAE</option>
                                            <option value="USA">USA</option>
                                        </select>                                       
                                         @if ($errors->has('country'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colM has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="email1">Email <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="email" class="form-control inputUtilisateur inputAccount" id="email1" name="email" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colM {{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="telephone1">Téléphone <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="number" class="for has-feedback{{ $errors->has('phone') ? ' has-error' : '' }} m-control inputUtilisateur inputAccount" id="telephone1" name="phone" required>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group hide1 colM has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="password1">Mot de passe <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="password1" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group hide1 colM has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="password1">Confirmation ...<span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="password1" name="password_confirmation" required>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                    <div class="elementCont hide1 has-feedback">
                                        <div class="groupContinuer">
                                            <button type="submit" class="btn btn-connecter">S'inscrire
                                                <img class="flecheBtn" src="{{asset('img/flecheBnt.png')}}" alt="">
                                            </button>
                                        </div>
                                        {{-- <p class="ouContinuer"><span>ou continuer avec</span></p>
                                        <div class="btnSociauxblock">
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/google.png')}}" alt="">
                                            </button>
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/facebook.png')}}" alt="">
                                            </button>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="blockImg2">
                                    <img src="{{asset('img/ce3.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </form>

                    <form class="formeAccount" method="POST" action="{{ url('/register') }}">
                        <div class="blockfabriquants">
                            <div class="blockElementequipement">
                                <button id="btnRetour2" class="btn btnRetour"> <img src="{{asset('img/retourImg.png')}}" alt=""></button>
                                <p class="utilisateurEquipement">FABRICANT</p>
                            </div>
                            <div class="cardEquipement2">
                                <div class="blockInput">
                                    @csrf
                                    <input type="hidden" name="type" value="fabricant">
                                    <div class="form-group hide1 colL has-feedback {{ $errors->has('profile') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="select2">Quel est votre profile ? <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <select id="select2" class="form-control inputAccount inputUtilisateur selectModife" name="profile" required>
                                            <option value="0"></option>
                                            <option value="1">Entreprise</option>
                                            <option value="2">Representant d'une entreprise</option>
                                        </select>
                                        @if ($errors->has('profile'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profile') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colM inputSelectHide2 has-feedback {{ $errors->has('firstName') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="PrenomEmploye">Prénom <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="PrenomEmploye" name="firstName" value=" ">
                                        @if ($errors->has('firstName'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('firstName') }}</strong>
                                        </span>
                                    @endif
                                    </div>
                                    <div class="form-group hide1 colM inputSelectHide2 has-feedback {{ $errors->has('lastName') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="nomEmploye">Nom <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="nomEmploye" name="lastName" value=" ">
                                            @if ($errors->has('lastName'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('lastName') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                    <div class="form-group hide1 colM has-feedback {{ $errors->has('enterprise') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="Entreprise1">Entreprise <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="Entreprise1" name="enterprise" required>
                                        @if ($errors->has('enterprise'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('enterprise') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colM has-feedback {{ $errors->has('expertiseDomain') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="activite1">Domaine d'activité</label>
                                        <select class="form-control inputAccount inputUtilisateur selectModife1" id="activite1" name="expertiseDomain" required>
                                            <option value="Electricité et réseaux électriques">Electricité et réseaux électriques</option>
                                            <option value="Sécurité des installations électriques">Sécurité des installations électriques</option>
                                            <option value="Industries (mines, pétroles, sucrerie, etc...)">Industries (mines, pétroles, sucrerie, etc...)</option>
                                            <option value="Prestation de services">Prestation de services</option>
                                            <option value="Maintenance des infrastructures électriques">Maintenance des infrastructures électriques</option>
                                            <option value="Installation/construction d'infrastructures électriques">Installation/construction d'infrastructures électriques</option>
                                        </select>

                                        @if ($errors->has('expertiseDomain'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('expertiseDomain') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colL has-feedback {{ $errors->has('country') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="Pays1">Pays</label>
                                        <select id="" class="form-control inputAccount inputUtilisateur selectModife" name="country" id="Pays1" required>
                                            <option value="Algérie">Algérie</option>                                            
                                            <option value="Bénin">Bénin</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Cap-Vert">Cap-Vert</option>
                                            <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                                            <option value="Gambie">Gambie</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Guinée">Guinée</option>
                                            <option value="Guinée Bissau">Guinée Bissau</option>
                                            <option value="Libéria">Libéria</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Maroc">Maroc</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigéria">Nigéria</option>
                                            <option value="Sénégal">Sénégal</option>
                                            <option value="Sierra Léone">Sierra Léone</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tunisie">Tunisie</option>
                                            <option value="UAE">UAE</option>
                                            <option value="USA">USA</option>
                                        </select>                                       
                                         @if ($errors->has('country'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('country') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colM has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="email1">Email <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="email" class="form-control inputUtilisateur inputAccount" id="email1" name="email" required>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group hide1 colM {{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="telephone1">Téléphone <span style="color:#e74c3c">&nbsp;(Obligatoire)</span> </label>
                                        <input type="number" class="for has-feedback{{ $errors->has('phone') ? ' has-error' : '' }} m-control inputUtilisateur inputAccount" id="telephone1" name="phone" required>
                                        @if ($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group hide1 colM has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label class="labelAccount"  for="password1">Mot de passe <span style="color:#e74c3c">&nbsp;(Obligatoire)</span></label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="password1" name="password" required>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-group hide1 colM has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="password1">Confirmation ... <span style="color:#e74c3c">&nbsp;(Obligatoire)</span> </label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="password1" name="password_confirmation" required>
                                        @if ($errors->has('password_confirmation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </span>
                                        @endif
                                    </div>


                                    <div class="form-group hide1 colM has-feedback{{ $errors->has('model') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="model">Model d'entreprise</label>
                                        <select id="" class="form-control inputAccount inputUtilisateur selectModife" name="model" id="model" required>
                                            <option value="PME">PME</option>
                                            <option value="ETI">ETI</option>
                                            <option value="GE">GE</option>
                                        </select>                                         
                                        @if ($errors->has('model'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('model') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    
                                    <div class="form-group hide1 colM has-feedback{{ $errors->has('targetArea') ? ' has-error' : '' }}">
                                        <label class="labelAccount" for="target">Zone cible </label>
                                        <select id="" class="form-control inputAccount inputUtilisateur selectModife" name="targetArea" id="target" required>
                                            <option value="Afrique Ouest">Afrique Ouest</option>
                                            <option value="Afrique Est">Afrique Est</option>
                                            <option value="Afrique Central">Afrique Central</option>
                                            <option value="Europe">Europe</option>
                                            <option value="Asie du sud">Asie du sud</option>
                                            <option value="Amerique latine">Amerique latine</option>
                                        </select>                                             
                                        @if ($errors->has('targetArea'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('targetArea') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="elementCont hide1">
                                        <div class="groupContinuer">
                                            <button type="submit" class="btn btn-connecter">S'inscrire<img class="flecheBtn" src="{{asset('img/flecheBnt.png')}}" alt=""></button>
                                        </div>
                                        {{-- <p class="ouContinuer"><span>ou continuer avec</span></p>
                                        <div class="btnSociauxblock">
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/google.png')}}" alt="">
                                            </button>
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/facebook.png')}}" alt="">
                                            </button>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="blockImg2">
                                    <img src="{{asset('img/ce3.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.min.js"></script>
<script>


    var items = $(".list-wrapper .list-item");
    var numItems = items.length;
    var perPage = 9;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
</script>
@endsection
@endsection
