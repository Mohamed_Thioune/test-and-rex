<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $post->id }}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{{ $post->text }}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{{ $post->type }}</p>
</div>

<!-- Media Field -->
<div class="form-group">
    {!! Form::label('media', 'Media:') !!}
    <p>{{ $post->media }}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('Duration', 'Duration:') !!}
    <p>{{ $post->Duration }}</p>
</div>

<!-- Online Field -->
<div class="form-group">
    {!! Form::label('online', 'Online:') !!}
    <p>{{ $post->online }}</p>
</div>

<!-- Request Monetize Field -->
<div class="form-group">
    {!! Form::label('request_monetize', 'Request Monetize:') !!}
    <p>{{ $post->request_monetize }}</p>
</div>

<!-- Is Monetize Field -->
<div class="form-group">
    {!! Form::label('is_monetize', 'Is Monetize:') !!}
    <p>{{ $post->is_monetize }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $post->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $post->updated_at }}</p>
</div>

