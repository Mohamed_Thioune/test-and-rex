<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::textarea('type', null, ['class' => 'form-control']) !!}
</div>

<!-- Media Field -->
<div class="form-group col-sm-6">
    {!! Form::label('media', 'Media:') !!}
    {!! Form::text('media', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('posts.index') }}" class="btn btn-default">Cancel</a>
</div>
