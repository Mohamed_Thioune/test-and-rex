@extends('layouts.linkFront')
<!--@section('css')
@endsection-->

@section('content-body-front')
<div class="contenQuiSommesNous">
<div class="blockNav">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
            <a class="navbar-brand navLogo" href="/">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife " href="/">Accueil</a>
                    </li>
                    <li class="nav-item activeNav">
                        <a class="nav-link nav-linkModife" href="/qui-sommes-nous">Qui sommes nous ?</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife" href="/nos-solutions">Nos solutions</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife"  href="/login">Rejoindre le Réseau</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/contact">Contact</a>
                    </li>
                    <li class="nav-item itemConnecte">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" class="form-control" />
                            </div>
                            <button type="button" class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="blockImgHead">
        <img src="{{asset('img/qsm.png')}}" alt="">
        <h1 class="textHeadQsm">Qui sommes-nous ?</h1>
    </div>
    <div class="qsmContentElement">
        <div class="container">
            <div class="dflex">
                <p class="textFaire">Expertise</p>
               <div class="position-relative">
                   <hr class="hrFaire">
               </div>
            </div>
            <h2 class="nos-dommaines">Présentation</h2>
            <div class="blockSLiderTextqsm">
                <div class="blockSlider">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item carouselItemModife active">
                                <img class="d-block w-100" src="{{asset('img/imgSlide2.png')}}" alt="First slide">
                            </div>
                            <div class="carousel-item carouselItemModife">
                                <img class="d-block w-100" src="{{asset('img/imgSlide2.png')}}" alt="Second slide">
                            </div>
                            <div class="carousel-item carouselItemModife">
                                <img class="d-block w-100" src="{{asset('img/imgSlide2.png')}}" alt="Third slide">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blockText">
                    <h2 class="testText"><span>TEST&REX</span>, c'est:</h2>
                    <p class="textDescriptionQsm">TEST&REX est une entreprise indépendante qui, à travers ses solutions, met en relation des acteurs du domaine des réseaux électriques pour une prise en compte effective des besoins de chacun d'eux.  </p>
                    <p class="textDescriptionQsm">A travers sa plateforme " TEST&REX-electric ", des experts du domaine partagent leurs connaissances et leurs expériences sur les différentes pratiques sur site, communiquent  sur les avantages des dernières innovations technologiques et mutualisent l'expertise technique. </p>
                    <p class="textDescriptionQsm">Le but est d'une part, promouvoir l'innovation technologique, mettre en avant les marques des fabricants afin d'assurer une meilleure visibilité et une meilleure connaissance des spécifications techniques de leurs produits auprès des utilisateurs finaux et des décideurs.
                        <br> D'autre part, assurer une meilleure rentabilité des investissements en matériels électriques et en instruments de test et mesures électriques afin de garantir une gestion efficiente de la durée de vie des infrastructures dans les postes électriques.</p>
                </div>
            </div>
        </div>
    </div>
   <div class="container">
   <div class="blockPOurVous">
        <img class="g1Orange" src="{{asset('img/g1.png')}}" alt="">
        <p class="textProfessionnels">Vous êtes professionnels du secteur de l'électricité, cette plateforme est pour vous ! <img class="g2Noir" src="{{asset('img/g2.png')}}" alt=""></p>
    </div>
   </div>
    <div class="blockNosMissions">
        <div class="container">
                    <h2 class="nos-dommaines"> Notre Mission</h2>
                    <div class=listeNosMissions>
                        <p class="notreOBjectif">Notre objectif est d'accompagner de manière indépendante les acteurs du marché des infrastructures
                            de réseaux électriques et des instruments de test et mesures électriques
                            dans l'amélioration de l'expérience client, du parcours client et dans la promotion de l'innovation technologique. </p>
                        <p class="notreOBjectif elementTextHide">Notre démarche par la mise en relation des fabricants et des utilisateurs finaux,
                            permet de pouvoir interagir ensemble directement dans les orientations stratégiques liées à l'innovation
                            avec une meilleure prise en compte des besoins réels sur le terrain à travers les retours d'expérience.</p>
                        <button id="btnDown1" class="btn btnDown">
                            <img src="{{asset('img/dowIcon.png')}}" alt="">
                        </button>
                    </div>
                    <div class="blockCardMissions">
                    <div class="row">
                <div id="carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <div class="d-none d-lg-block">
                        <div class="slide-box">
                        <div class="cardslide">
                            <img src="{{asset('img/slidecard.png')}}" alt="">
                            <p>Prescription de marque et Promotion de portefeuille</p>
                        </div>
                            <div class="cardslide">
                                <img src="{{asset('img/slidecard.png')}}" alt="">
                                <p>Formation & Accompagnement montée en compétence technique</p>
                            </div>
                        <div class="cardslide">
                            <img src="{{asset('img/slidecard.png')}}" alt="">
                            <p>Spécification & Fourniture du matériel adéquat</p>
                        </div>
                        </div>
                    </div>

                    </div>
                    <div class="carousel-item">
                    <div class="d-none d-lg-block">
                        <div class="slide-box">
                        <div class="cardslide">
                            <img src="{{asset('img/slidecard.png')}}" alt="">
                            <p>Étude de marché & Développement de Business</p>
                        </div>
                        <div class="cardslide">
                            <img src="{{asset('img/slidecard.png')}}" alt="">
                            <p>Veille technique et technologique</p>
                        </div>
                        <div class="cardslide">
                            <img src="{{asset('img/slidecard.png')}}" alt="">
                            <p>Communication entre experts</p>
                        </div>


                        </div>
                    </div>

                    </div>
                </div>
                    <div class="flecheCarousel">
                    <a class="carousel-control-prev prevModife" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true">
                        <img src="{{asset('img/prev.png')}}" alt="">
                    </span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next nextModife" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true">
                    <img src="{{asset('img/next.png')}}" alt="">
                    </span>
                    <span class="sr-only">Next</span>
                </a>
                    </div>
                </div>

            </div>

                    </div>

        </div>
    </div>
    <!-- fin block missions -->

    <div class="bloxkNosValeurs">
        <div class="container">
             <h2 class="nos-dommaines">Nos Valeurs</h2>
             <div class=listeNosMissions>
                <ul>
                    <li>La place  de  nos clients  est  au  centre de notre démarche  soutenue  par  une politique  axée  sur  des  valeurs  profondes que sont :</li>
                </ul>
            </div>
            <div class="blockCardValeurs">
                <div class="row">
                    <div class="col-md-3">
                        <div class="cardNosValeurs">
                            <div class="headCardValeurs">
                                <div class="circleOrange"></div>
                                <p>CONFIANCE</p>
                            </div>
                            <div class="imgCardValeurElement">
                                <img src="{{asset('img/ImgValeur.png')}}" alt="">
                            </div>
                            <div class="contentText">
                                <p>Nous entretenons une relation personnalisée avec tous les acteurs du secteur de l'électricité qui nous sollicitent.
                                    <br>L'expérience client est la base de notre démarche d'amélioration continue pour répondre aux besoins de nos clients.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="cardNosValeurs">
                            <div class="headCardValeurs">
                                <div class="circleOrange"></div>
                                <p>EXPERTISE PARTAGÉE</p>
                            </div>
                            <div class="imgCardValeurElement">
                                <img src="{{asset('img/ImgValeur.png')}}" alt="">
                            </div>
                            <div class="contentText">
                                <p>Tous les membres du réseau " TEST&REX-electric ", fabricants comme utilisateurs, bénéficient d'un partage de connaissance et d'expertise
                                    grâce à la mise en relation basée essentiellement sur la confiance. <br>
                                    Nous garantissons un accompagnement permanent et un suivi continu à l'ensemble de nos clients.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="cardNosValeurs">
                            <div class="headCardValeurs">
                                <div class="circleOrange"></div>
                                <p>EXPÉRIENCE PARTAGÉE</p>
                            </div>
                            <div class="imgCardValeurElement">
                                <img src="{{asset('img/ImgValeur.png')}}" alt="">
                            </div>
                            <div class="contentText">
                                <p>Tout acteur du secteur de l'électricité membre de la plateforme " TEST&REX-electric " profite des retours d'expérience terrain des autres professionnels du même secteur.
                                    <br>
                                    L'entraide sans engagement permet  à chacun  de bénéficier d'aptitudes  et de savoir-faire multiples.</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="cardNosValeurs">
                            <div class="headCardValeurs">
                                <div class="circleOrange"></div>
                                <p>SATISFACTION</p>
                            </div>
                            <div class="imgCardValeurElement">
                                <img src="{{asset('img/ImgValeur.png')}}" alt="">
                            </div>
                            <div class="contentText">
                                <p>Notre politique et notre démarche visent à donner une entière satisfaction à toutes les parties
                                    prenantes grâce à la réactivité et à l'efficacité de nos équipes et des membres du réseau, garantissant ainsi l'atteinte de résultats.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blockRejoindre">
        <div class="container">
            <div class="elementContentRejoindre">
                <div class="block1">
                    <div class="dflex">
                        <p class="textFaire2">Nous Rejoindre</p>
                        <div class="position-relative">
                            <hr class="hrFaire2">
                        </div>
                    </div>
                    <h3 class="rejoindreText">Prêt à rejoindre <span>les experts</span>?</h3>
                    <h4 class="ayezText">Ayez les meilleures expériences d'échange avec des experts électriciens.</h4>
                    <a href="/login" class="btn btnRejoindreWelcome">Nous Rejoindre</a>
                </div>
                <div class="block2">
                    <div class="contentImgPattern">
                        <img class="patternOrange" src="{{asset('img/patternOrange.png')}}" alt="">
                        <img class="patterHomme" src="{{asset('img/portraitHomme.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        @include('layouts.footerLayaout')
    </div>
</div>

<!--@section('scripts')
@endsection-->
@endsection