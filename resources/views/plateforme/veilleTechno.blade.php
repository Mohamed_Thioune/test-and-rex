@extends('plateforme.sample.template')
@php
    $fabricants =  DB::Table('users')->select('enterprises.id', 'enterprises.enterprise')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.type', "fabricant")
                ->get();  
                
    if($user->photo != "Mu-bull-gris.png") 
        $url0 = Storage::disk('s3')->temporaryUrl('users/'.$user->photo, now()->addMinutes(100));
    else 
        $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png'; 
@endphp
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
@endsection
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
            <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife " href="{{ route('home') }}">Accueil</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/veille">Veille technologique</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/support">Support tech.</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife" href="/formation">Formations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/notation">Noter un Fabricant</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/nousContacter">Nous contacter</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                <img src="{{$url0}}"  alt="">
                            </button>
                            <div class="modalModife">
                                <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <ul class="navbar-nav d-block">
                                                    <li class="nav-item">
                                                        <a href="{{ url('/logout') }}" class="nav-link"
                                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        <div class="blockVeilleCard">
            @include('flash::message')
            <div class="btnGroupVeille">
                <button id="btnComparatifsVeille" class="btn btnVeille active">
                    <img class="iconeVeilleBnt" src="{{asset('img/vT1.png')}}" alt="">
                    Comparatif de Produits
                </button>
                <button id="btnNouvautes" class="btn btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/vT2.png')}}" alt="">
                    Nouveautés
                </button>
                <button id="btnIdee" class="btn btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/vT3.png')}}" alt="">
                    Je donne mes idées
                </button>
            </div>
           <div class="comparatifsProduitsBlock">
            <form action="" class="filtreVeille">
                    <p class="filtreText">Filtre :</p>
                    <div class="inputFiltre">
                        <select id="selectTech1" class="form-control selectModifeVeille">
                            <option value="0"></option>
                            <option value="entreprise">Sénégal</option>
                            <option value="electricienEntreprise">Canada</option>
                            <option value="independantElectricient">Afrique du sud</option>
                        </select>
                        <select id="selectTech2" class="form-control selectModifeVeille" placeholder="Fabricant">
                            <option value="0"></option>
                            <option value="entreprise">Utilisateur</option>
                        </select>
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form2" class="form-control" />
                            </div>
                            <button type="button" placeholder="Mots-clés ..." class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </div>
                </form>
                <div class="blockProduitsVeille">
                    {{-- <div class="oneProductsVeille">
                        <div class="imgElementVeille">
                            <img src="{{asset('img/ImgProduit.png') }}" alt="">
                            <div class="abosluteBackground"></div>
                            <p class="recommandeB">RECOMMANDE</p>
                            <div class="vueCommentaireProduits">
                                <div class="vue">
                                    <img src="{{asset('img/Eye.png') }}" alt="">
                                    <p>2459</p>
                                </div>
                                <div class="commentaire">
                                    <img src="{{asset('img/Message.png') }}" alt="">
                                    <p>47</p>
                                </div>
                            </div>
                        </div>
                        <div class="fabriquantProduits">
                            <p class="titleFab">Adiscipae Electris</p>
                            <ul class=desFab>
                                <li>Nemo enim ipsam voluptatem quia voluptas verbatim</li>
                            </ul>
                            <div class="fabImgBlock">
                                <img class="vectorF" src="{{asset('img/Vector-f.png') }}" alt="">
                                <p>Fabricant :</p>
                                <img class="logoF" src="{{asset('img/megger.png') }}" alt="">
                            </div>
                        </div>
                        <div class="priceBlock">
                            <p class="active">12.700 <span>€</span></p>
                            <a href="" class="decouvrirBtn">Découvrir <img class="nextDecouvrir" src="{{asset('img/next.png') }}" alt=""></a>
                        </div>
                    </div>
                    <div class="oneProductsVeille">
                        <div class="imgElementVeille">
                            <img src="{{asset('img/ImgProduit2.png') }}" alt="">
                            <div class="abosluteBackground"></div>
                            <p class="recommandeB">RECOMMANDE</p>
                            <div class="vueCommentaireProduits">
                                <div class="vue">
                                    <img src="{{asset('img/Eye.png') }}" alt="">
                                    <p>2459</p>
                                </div>
                                <div class="commentaire">
                                    <img src="{{asset('img/Message.png') }}" alt="">
                                    <p>47</p>
                                </div>
                            </div>
                        </div>
                        <div class="fabriquantProduits">
                            <p class="titleFab">Sed ergum volte</p>
                            <ul class=desFab>
                                <li>Nemo enim ipsam voluptatem quia voluptas aut fugit,</li>
                            </ul>
                            <div class="fabImgBlock">
                                <img class="vectorF" src="{{asset('img/Vector-f.png') }}" alt="">
                                <p>Fabricant :</p>
                                <img class="logoF" src="{{asset('img/megger.png') }}" alt="">
                            </div>
                        </div>
                        <div class="priceBlock">
                            <p>15.200<span>€</span></p>
                            <a href="" class="decouvrirBtn">Découvrir <img class="nextDecouvrir" src="{{asset('img/next.png') }}" alt=""></a>
                        </div>
                    </div>
                    <div class="oneProductsVeille">
                        <div class="imgElementVeille">
                            <img src="{{asset('img/ImgProduit3.png') }}" alt="">
                            <div class="abosluteBackground"></div>
                            <p class="recommandeB">RECOMMANDE</p>
                            <div class="vueCommentaireProduits">
                                <div class="vue">
                                    <img src="{{asset('img/Eye.png') }}" alt="">
                                    <p>2459</p>
                                </div>
                                <div class="commentaire">
                                    <img src="{{asset('img/Message.png') }}" alt="">
                                    <p>47</p>
                                </div>
                            </div>
                        </div>
                        <div class="fabriquantProduits">
                            <p class="titleFab">Lorem ipsum dolore sit ametis</p>
                            <ul class=desFab>
                                <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,</li>
                            </ul>
                            <div class="fabImgBlock">
                                <img class="vectorF" src="{{asset('img/Vector-f.png') }}" alt="">
                                <p>Fabricant :</p>
                                <img class="logoF" src="{{asset('img/megger.png') }}" alt="">
                            </div>
                        </div>
                        <div class="priceBlock">
                            <p>35.860 <span>€</span></p>
                            <a href="" class="decouvrirBtn">Découvrir <img class="nextDecouvrir" src="{{asset('img/next.png') }}" alt=""></a>
                        </div>
                    </div> --}}
                </div>
           </div>
           <div class="nauvautesBlock">
                <div class="headRectangle">
                    <div class="fiabliteserviceBlock">
                        <div class="elementIcone">
                            <img src="{{asset('img/ic1.png')}}" alt="">
                        </div>
                        <div>
                            <p class="fiabliteText">Fiabilité de service</p>
                            <p class="sedText">Sed ut perspiciatis unde omnis iste natus.</p>
                        </div>
                    </div>
                    <div class="fiabliteserviceBlock">
                        <div class="elementIcone el2">
                            <img src="{{asset('img/ic2.png')}}" alt="">
                        </div>
                        <div>
                            <p class="fiabliteText">Meilleur support</p>
                            <p class="sedText">Nemo enim ipsam volup quia voluptas sit aspernatur</p>
                        </div>
                    </div>
                    <div class="fiabliteserviceBlock">
                        <div class="elementIcone ">
                            <img src="{{asset('img/ic3.png')}}" alt="">
                        </div>
                        <div>
                            <p class="fiabliteText">Qualité assurée</p>
                            <p class="sedText">Adipisci velit, sed quia non numquam eius..</p>
                        </div>
                    </div>
                </div>
                <div class="groupCardNProduit">
                    @foreach ($products as $product)
                        <div class="cardNouvauteProduit">
                            <div class="blockImgNProduit">
                                <img src="{{ $product->photo ? asset('images/uploads'). "/".$product->photo : asset('img/img1.png') }}" alt="">
                                <div class="blockOrangeHover">
                                    <div class="likeIconeText">
                                        <p>130</p>
                                        <img src="{{asset('img/heartLike.png')}}" alt="">
                                    </div>
                                    <div class="blockOeicart">
                                    <a href=""><img src="{{asset('img/eyeLike.png')}}" alt=""></a>
                                    <a href=""><img src="{{asset('img/shopping-cartLike.png')}}" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <p class="titleCardNProduit">{{ $product->name }}</p>
                            <p class="automatisationText">{{ $product->type }}</p>
                            @if($product->price) 
                            <p class="automatisationText2">F{{ $product->price }}</p>
                            @endif
                        </div>
                    @endforeach
                </div>
           </div>
           <div class="ideeBlock">

           <form class="formeAccount" action="{{route('idea')}}" method="POST">
                @csrf
                <div class="blockInput2">
                    {{--  <div class="form-group  colM inputSelectHide">
                        <label class="labelAccount"  for="prenomIdee">Prénom</label>
                        <input type="password" class="form-control" id="prenomIde" required>
                    </div>
                    <div class="form-group  colM inputSelectHide">
                        <label class="labelAccount"  for="nomidee">Nom</label>
                        <input type="email" class="form-control" id="nomidee" required>
                    </div>
                    <div class="form-group  colM">
                        <label class="labelAccount"  for="Entrepriseidee">Entreprise</label>
                        <input type="text" class="form-control" id="Entrepriseidee" required>
                    </div> --}}
                    <div class="form-group  colM">
                        <label class="labelAccount" for="fabriquantIdee">Type d'application</label>
                        <select id="select1" name="application" class="form-control selectModife" id="fabriquantIdee" required>
                            <option value="Transformateur">Transformateur</option>
                            <option value="Diagnostic câbles souterrains">Diagnostic câbles souterrains</option>
                            <option value="Protection & Automatisme">Protection & Automatisme</option>
                            <option value="Moteur & Alternateur">Moteur & Alternateur</option>
                            <option value="Disjoncteur">Disjoncteur</option>
                            <option value="Autres applications">Autres applications</option>
                        </select>
                    </div>

                    <div class="form-group  colM">
                        <label class="labelAccount" for="fabriquantIdee">Fabricant</label>
                        <select id="select1" name="fabricant" class="form-control selectModife" id="fabriquantIdee" required>
                        @foreach ($fabricants as $fabricant)
                            <option value="{{$fabricant->enterprise}}">{{$fabricant->enterprise}}</option>
                        @endforeach
                        </select>
                    </div>

                    <div class="form-group colL flexElement2">
                        <label class="labelAccount"  for="activite1">Idée d'amélioration</label>
                        <textarea name="content" id=""  rows="6"></textarea>
                    </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btnSoomettreIdee">Soumettre votre idée</button>
                    </div>
                </form>

            </div>

        </div>
    </div>
</div>
<p class="textCopyright1">© 2021 Test&Rex. Tous droits réservés.</p>


@section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/piexif.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/sortable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('js/fileInput/fileinput.min.js') }}"></script>
        <script src="{{ asset('js/fileInput/LANG.js') }}"></script>
        <script>
            const swiper = new Swiper(".mySwiper", {
                slidesPerView: 2.3,
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {

                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1.3,
                    spaceBetween: 10
                },
                980: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 2.3,
                    spaceBetween: 20
                },
                }

            });
        </script>
@endsection
@endsection