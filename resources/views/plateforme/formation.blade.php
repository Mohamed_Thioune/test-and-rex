@extends('plateforme.sample.template')
@php
    if($user->photo != "Mu-bull-gris.png") 
        $url0 = Storage::disk('s3')->temporaryUrl('users/'.$user->photo, now()->addMinutes(100));
    else 
        $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png';         
@endphp
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
@endsection
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
            <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife " href="{{ route('home') }}">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/veille">Veille technologique</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/support">Support tech.</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/formation">Formations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/notation">Noter un Fabricant</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/nousContacter">Nous contacter</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                <img src="{{$url0}}"  alt="">
                            </button>
                            <div class="modalModife">
                                <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <ul class="navbar-nav d-block">
                                                    <li class="nav-item">
                                                        <a href="{{ url('/logout') }}" class="nav-link"
                                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        <div class="blockFormation">
            <h1 class="titleFormation">Formations</h1>
            <div class="blockFormationCard">
                <div class="cardFormation btn" id="btnDemandeFormation">
                    <div class="imgCardFormation">
                        <img src="{{asset('img/03Formation.png')}}" alt="">
                    </div>
                    <p class="titleCardFormation">Demander une Formation</p>
                </div>
                <a href="" class="cardFormation">
                    <div class="imgCardFormation">
                        <img src="{{asset('img/04Formation.png')}}" alt="">
                    </div>
                    <p class="titleCardFormation">Participer à un Webinar</p>
                </a>
            </div>
        </div>
        <div class="blockNosFormation">
            <div class="d-flex align-items-center">
                <p class="nosFormationsTitle">Nos Formations</p>
                <hr class="hrModife2">
            </div>
            <div class="swiper-container mySwiper">
                <div class="swiper-wrapper">
                    <a href="" class="swiper-slide swiper-slide-Modife1">
                        <div class="cardNosFormations firstformation">
                            <div class="imgBlockNosFormations">
                                <img src="{{asset('img/formation1.png')}}" alt="">
                            </div>
                        </div>
                        <p class="titleNosFormations">GESTION DE PROJET</p>
                    </a>
                    <a href="" class="swiper-slide swiper-slide-Modife1">
                        <div class="cardNosFormations secondformation">
                            <div class="imgBlockNosFormations">
                                <img src="{{asset('img/formation2.png')}}" alt="">
                            </div>
                        </div>
                        <p class="titleNosFormations">PROGRAMMATION</p>
                    </a>
                    <a href="" class="swiper-slide swiper-slide-Modife1">
                        <div class="cardNosFormations treeformation">
                            <div class="imgBlockNosFormations">
                                <img src="{{asset('img/formation2.png')}}" alt="">
                            </div>
                        </div>
                        <p class="titleNosFormations">PROGRAMMATION</p>
                    </a>
                    <div class="swiper-slide swiper-slide-Modife1">Slide 4</div>
                </div>
            </div>
        </div>
    </div>
    <div class="blockDemander">
        <div class="cardDemanderUneFormation">
            <div class="d-flex">
                <button class="btn btnRetourDemande"><img src="{{asset('img/retourOrange.png')}}" alt=""></button>
                <p class="titleFormation">Demander une formation</p>
            </div>
            <form method="POST" action="{{route('learning')}}">
                @csrf
                <div class="formFormation">
                    <div class="form-group hide1 colM">
                        <label class="labelFormation" for="application">Type d'application</label>
                        <select name="application" class="form-control inputFormation inputUtilisateur selectModife" id="application" >
                            <option value="Transformateur">Transformateur</option>
                            <option value="Diagnostic câbles souterrains">Diagnostic câbles souterrains</option>
                            <option value="Protection & Automatisme">Protection & Automatisme</option>
                            <option value="Moteur & Alternateur">Moteur & Alternateur</option>
                            <option value="Disjoncteur">Disjoncteur</option>
                            <option value="Autres applications">Autres applications</option>
                        </select>
                    </div>
                    <div class="form-group hide1 colM">
                        <label class="labelFormation" for="typeProduit">Type de produit</label>
                        <select name="product" class="form-control inputFormation inputUtilisateur selectModife" id="typeProduit" required>
                            <option value="...">...</option>
                        </select>
                    </div>
                
                    <div class="form-group hide1 colM">
                        <label class="labelFormation"  for="activite2">Domaine d'activité</label>
                        <select name="activity" class="form-control inputFormation inputUtilisateur selectModife1" id="activite2" required>
                            <option value="Prestation de services">Prestation de services</option>
                            <option value="Electricité et réseaux électriques">Electricité et réseaux électriques</option>
                            <option value="Sécurité des installations électriques">Sécurité des installations électriques</option>
                            <option value="Industries (mines, pétroles, sucrerie, etc...)">Industries (mines, pétroles, sucrerie, etc...)</option>
                            <option value="Maintenance des infrastructures électriques">Maintenance des infrastructures électriques</option>
                            <option value="Installation/construction d’infrastructures électriques ">Installation/construction d’infrastructures électriques </option>
                        </select>
                    </div>

                    <div class="form-group hide1 colL">
                        <label class="labelFormation"  for="password1">Délai de votre demande</label>
                        <input name="deadline" type="date" class="form-control inputUtilisateur inputFormation" id="password1" required>
                    </div>

                    <div class="groupContinuer">
                        <button type="submit" class="btn btn-demande" id="continuerEntreprise">Envoyer la demande</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<p class="textCopyright1">© 2021d Test&Rex. Tous droits réservés.</p>


@section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/piexif.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/sortable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('js/fileInput/fileinput.min.js') }}"></script>
        <script src="{{ asset('js/fileInput/LANG.js') }}"></script>
        <script>
            const swiper = new Swiper(".mySwiper", {
                slidesPerView: 2.3,
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {

                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1.3,
                    spaceBetween: 10
                },
                980: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 2.3,
                    spaceBetween: 20
                },
                }

            });
        </script>
@endsection
@endsection