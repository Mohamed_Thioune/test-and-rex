{{-- formation --}}
@extends('plateforme.sample.template')
@php
    if($user->photo != "Mu-bull-gris.png") 
        $url0 = Storage::disk('s3')->temporaryUrl('users/'.$user->photo, now()->addMinutes(100));
    else 
        $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png';         
@endphp
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
<link rel="stylesheet" href="{{asset('css/style.css')}}">
@endsection
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
            <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife " href="{{ route('home') }}">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/veille">Veille technologique</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/support">Support tech.</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife" href="/formation">Formation</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/notation">Noter un Fabricant</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/nousContacter">Nous contacter</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                <img src="{{$url0}}"  alt="">
                            </button>
                            <div class="modalModife">
                                <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <ul class="navbar-nav d-block">
                                                    <li class="nav-item">
                                                        <a href="{{ url('/logout') }}" class="nav-link"
                                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>
<div class="contenQuiSommesNous">
    <div class="container">
        <div class="blockContact">
            <img class="contactIconImg" src="{{asset('img/contact_icon.png')}}" alt="">
            <p class="contactSupportText">Contact & Support</p>
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="cardAdressContact">
                        <p class="title">Adresse</p>
                        <hr>
                        <p class="description">13, Rue de la Remise aux Faisans
                            94600, CHOISY-LE-ROI
                            FRANCE</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="cardAdressContact">
                        <p class="title">Contacts</p>
                        <hr>
                        <p class="descriptionContact"><b>Fix</b> : +33 (0) 9 87 30 64 98</p>
                        <p class="descriptionContact"><b>Tel</b> : +33 (0) 6 68 61 79 22</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="cardAdressContact">
                        <p class="title">Mail</p>
                        <hr>
                        <p class="description">contact@testandrex.com</p>
                        <p class="description">support-technique@testandrex.com</p>
                    </div>
                </div>
            </div>
            <div class="blockCardMap">
                <img src="{{asset('img/map.png')}}" alt="">
            </div>
            <p class="contactSupportText">Formulaire</p>
            <form method="POST" action="{{route('contacter')}}" class="formContact">
                @csrf
                @if(Auth::guest())
                    <div class="form-group colM">
                        <label class="labelAccount" for="prenomContact">Prénom *</label>
                        <input type="text" name="firstName" class="form-control inputAccount" id="prenomContact" name="prenomContact" required>
                    </div>
                    <div class="form-group colM">
                        <label class="labelAccount" for="NomContact">Nom *</label>
                        <input type="text" name="lastName" class="form-control inputAccount" id="NomContact" name="NomContact" required>
                    </div>
                    <div class="form-group colM">
                        <label class="labelAccount" for="emailContact">Votre e-mail *</label>
                        <input type="email" name="email" class="form-control inputAccount" id="emailContact" name="emailContact" required>
                    </div>
                @endif
                <div class="form-group colM">
                    <label class="labelAccount" for="objetMessageContact">Objet du message *</label>
                    <select id="objetMessageContact" class="form-control inputAccount inputUtilisateur selectModife" name="objet" required>
                        <option value="Infos formation">Infos formation</option>
                        <option value="Réclamation">Réclamation</option>
                    </select>
                </div>
                @if(Auth::guest())
                    <div class="form-group colM">
                        <label class="labelAccount" for="telephoneContat">Telephone</label>
                        <input type="number" name="phone" class="form-control inputAccount" id="telephoneContat" name="telephoneContat" required>
                    </div>
                    <div class="form-group colM">
                        <label class="labelAccount" for="paysContact">Pays</label>
                        <select id="paysContact" name="country" class="form-control inputAccount inputUtilisateur selectModife" name="paysContact" required>
                            <option value="Bénin">Bénin</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Cap-Vert">Cap-Vert</option>
                            <option value="Côte d'Ivoire">Côte d'Ivoire</option>
                            <option value="Gambie">Gambie</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Guinée">Guinée</option>
                            <option value="Guinée Bissau">Guinée Bissau</option>
                            <option value="Libéria">Libéria</option>
                            <option value="Mali">Mali</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigéria">Nigéria</option>
                            <option value="Sénégal">Sénégal</option>
                            <option value="Sierra Léone">Sierra Léone</option>
                            <option value="Togo">Togo</option>
                            <option value="UAE">UAE</option>
                            <option value="USA">USA</option>
                        </select>
                    </div>
                @endif
                <div class="form-group colL">
                    <label class="labelAccount" for="messageContact">Votre message</label>
                    <textarea name="content" id="messageContact" rows="7" placeholder="Ecrire ici votre message ..."></textarea>
                </div>
                <button class="btn btnEnvoyer">Envoyer</button>
            </form>
        </div>
    </div>
</div>
