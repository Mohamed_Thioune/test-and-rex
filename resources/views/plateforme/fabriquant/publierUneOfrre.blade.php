@extends('plateforme.fabriquant.layouts.template')
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
@endsection
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife " href="/homeFabriquant">Accueil</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife" href="/communication">Communication</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/publicationOffre">Publier une offre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/etudeDeMarche">Etude de marché</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Mes notations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Contacts</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <a class="nav-link nav-toute" href="#">
                                <img src=" {{asset('img/touteIcone.png') }}" alt="">
                                Toutes
                            </a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="circleImgNav">
                                <img src="{{asset('images/uploads')}}/{{ Auth::user()->photo ? Auth::user()->photo : 'Mu-bull-gris.png'}}" alt="">
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="blockNoAbonne">

    <p class="titleNoAbonne">Publication d'offres</p>
   
    <div class="cardNoAbonne">
        <img class="ICO" src="{{asset('img/ICO.png')}}" alt="">
        <p class="pasAbonneText">Vous n'etes pas encore abonné</p>
        <p class="accessServiceText">Pour accéder à ce service vous devez vous abonner à l'une de nos formules.
        Veuillez contacter notre service marketing pour les modalités d'adhésion.</p>
        <a href="" class="btn btnVousAbonne">Vous abonner ici
            <img src="{{asset('img/next.png')}}" alt="">
         </a>
    </div>
</div>
<div class="contentPageInterneOne">
    <div class="blocksContentPublication">
        <div class="blockOnePublication blockC">
            <div class="blockFormation">
                <h1 class="titleFormation">Publication d'offres</h1>
                <div class="blockFormationCard">
                    <button class="cardFormation btn" id="btnDemandeFormation">
                        <div class="imgCardFormation">
                            <img src="{{asset('img/03Formation.png')}}" alt="">
                        </div>
                        <p class="titleCardFormation">Publier une offre</p>
                    </button>
                </div>
            </div>
            <div class="blockNosFormation">
                <div class="d-flex align-items-center">
                    <p class="nosFormationsTitle">Offres publiées</p>
                    <hr class="hrModife3">
                    <a href="" class="voirToutBnt2">Voir Tout
                        <img src="{{asset('img/next.png')}}" alt="">
                    </a>
                </div>
                <div class="swiper-container mySwiper">
                    <div class="swiper-wrapper">
                        <a href="" class="swiper-slide swiper-slide-Modife1">
                            <div class="cardNosFormations firstformation">
                                <div class="imgBlockNosFormations">
                                    <img src="{{asset('img/formation1.png')}}" alt="">
                                </div>
                            </div>
                            <div>
                            <p class="titleNosFormations2">Lorem ipsum verud</p>
                            <div class="blockPrix">
                                <div class="pricePromo">
                                    <p class="prixNormal">29.000 </p>
                                    <p class="fauxPrix"> 42.000   </p>
                                </div>
                                <p class="pourcentagePromotion">-23%</p>
                            </div>
                            </div>
                        </a>
                        <a href="" class="swiper-slide swiper-slide-Modife1">
                            <div class="cardNosFormations secondformation">
                                <div class="imgBlockNosFormations">
                                    <img src="{{asset('img/formation2.png')}}" alt="">
                                </div>
                            </div>
                            <div>
                            <p class="titleNosFormations2">Lorem ipsum verud</p>
                            <div class="blockPrix">
                                <div class="pricePromo">
                                    <p class="prixNormal">29.000 </p>
                                    <p class="fauxPrix"> 42.000   </p>
                                </div>
                                <p class="pourcentagePromotion">-23%</p>
                            </div>
                            </div>
                        </a>
                        <a href="" class="swiper-slide swiper-slide-Modife1">
                            <div class="cardNosFormations treeformation">
                                <div class="imgBlockNosFormations">
                                    <img src="{{asset('img/formation2.png')}}" alt="">
                                </div>
                            </div>
                            <div>
                            <p class="titleNosFormations2">Lorem ipsum verud</p>
                            <div class="blockPrix">
                                <div class="pricePromo">
                                    <p class="prixNormal">29.000 </p>
                                    <p class="fauxPrix"> 42.000   </p>
                                </div>
                                <p class="pourcentagePromotion">-23%</p>
                            </div>
                            </div>
                        </a>
                        <div class="swiper-slide swiper-slide-Modife1">Slide 4</div>
                    </div>
                </div>
                <div class="d-flex align-items-center">
                    <p class="nosFormationsTitle">Mes  Publicités</p>
                    <hr class="hrModife3">
                    <a href="" class="voirToutBnt2">Voir Tout
                        <img src="{{asset('img/next.png')}}" alt="">
                    </a>
                </div>
            </div>
        </div>
        <div class="blockDemander">
            <div class="cardDemanderUneFormation">
                <div class="d-flex">
                    <button class="btn btnRetourDemande"><img src="{{asset('img/retourOrange.png')}}" alt=""></button>
                    <p class="titleFormation">Demander une formation</p>
                </div>
                <form action="">
                    <div class="formFormation">
                        <div class="form-group hide1 colM">
                            <label class="labelFormation"  for="dm">N° DM</label>
                            <input type="text" class="form-control inputFormation" id="dm" required>
                        </div>
                        <div class="form-group hide1 colM">
                            <label class="labelFormation" for="application">Type d'application</label>
                            <select class="form-control inputFormation inputUtilisateur selectModife" id="application" required>
                                <option></option>
                                <option>Ingénieur de surface</option>
                                <option>Ingénieur de surface</option>
                            </select>
                        </div>
                        <div class="form-group hide1 colM">
                            <label class="labelFormation" for="typeProduit">Type de produit</label>
                            <select class="form-control inputFormation inputUtilisateur selectModife" id="typeProduit" required>
                                <option></option>
                                <option>Ingénieur de surface</option>
                                <option>Ingénieur de surface</option>
                            </select>
                        </div>
                        <div class="form-group hide1 colL">
                            <label class="labelFormation"  for="Entreprise2">Entreprise</label>
                            <input type="text" class="form-control inputFormation" id="Entreprise2" required>
                        </div>
                        <div class="form-group hide1 colM">
                            <label class="labelFormation"  for="activite2">Domaine d'activité</label>
                            <select class="form-control inputFormation inputUtilisateur selectModife1" id="activite2" required>
                                <option></option>
                                <option>Ingénieur de surface</option>
                                <option>Ingénieur de surface</option>
                            </select>
                        </div>
                        <div class="form-group hide1 colL">
                            <label class="labelFormation"  for="prenonDemande">Prénom</label>
                            <input type="text" class="form-control inputUtilisateur inputFormation" id="prenonDemande" required>
                        </div>
                        <div class="form-group hide1 colM">
                            <label class="labelFormation"  for="nonDemande">Nom</label>
                            <input type="text" class="form-control inputUtilisateur inputFormation" id="nonDemande" required>
                        </div>
                        <div class="form-group hide1 colM">
                            <label class="labelFormation"  for="email1">E-mail</label>
                            <input type="email" class="form-control inputUtilisateur inputFormation" id="email1" required>
                        </div>
                        <div class="form-group hide1 colM">
                            <label class="labelFormation"  for="telephone1">Téléphone</label>
                            <input type="number" class="form-control inputUtilisateur inputFormation" id="telephone1" required>
                        </div>
                        <div class="form-group hide1 colL">
                            <label class="labelFormation"  for="password1">Délai de votre demande</label>
                            <input type="date" class="form-control inputUtilisateur inputFormation" id="password1" required>
                        </div>
                        <div class="groupContinuer">
                            <button type="submit" class="btn btn-demande" id="continuerEntreprise">Envoyer la demande</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="blocksContentPublierOffre">
    <div class="">
            <div class="headOrganiserUnWebinar">
               <button class="btn btnRetourPublication">
                  <img class="prevWebinar" src="{{asset('img/prev.png') }}" alt="">
               </button>
                <img class="barRight" src="{{asset('img/bar_right.png') }}" alt="">
                <p class="titleFormation">Publier une offre</p>
            </div>
            <form action="">
                <div class="formFormation">
                    <div class="form-group  colM">
                        <span class="labelFormation d-block">Image de l'offre</label>
                        <div class="col-ting">
                            <div class="control-group file-upload" id="file-upload1">
                                <div class="image-box text-center">
                                    <p>Cliquer pour selectionner une image pour l'offre</p>
                                    <img src="" alt="">
                                </div>
                                <div class="controls" style="display: none;">
                                    <input type="file" name="contact_image_1"/>
                                </div>
                            </div>
                         </div>
                    </div>
                    <div class="colM">
                        <div class="form-group  ">
                            <label class="labelFormation"  for="titreOffre2">Titre de l'offre</label>
                            <input type="text" id="titreOffre2" class="form-control inputUtilisateur inputFormation" required>
                        </div>
                        <div class="form-group  ">
                            <label class="labelFormation"  for="ancienTarif">Ancien tarif</label>
                            <input type="text" id="ancienTarif" class="form-control inputUtilisateur inputFormation" required>
                        </div>
                        <div class="form-group ">
                            <label class="labelFormation"  for="newTarif">Nouveau tarif</label>
                            <input type="text" class="form-control inputUtilisateur inputFormation" id="newTarif" required>
                        </div>
                    </div>
                    <div class="form-group  colL">
                        <label class="labelFormation"  for="descriptionOffre2">Description de l'offre</label>
                        <textarea class="modifeTextArea2" name="" id="descriptionOffre2" rows="6"></textarea>
                    </div>

                    <div class="form-group  colL">
                        <label class="labelFormation"  for="dateOffre">Fin de l'offre</label>
                        <input type="date" class="form-control inputUtilisateur inputFormation" id="dateOffre"  required>
                    </div>
                    <div class="groupContinuer">
                        <button type="submit" class="btn btn-demande">Publier l'offre</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<p class="textCopyright1">© 2021d Test&Rex. Tous droits réservés.</p>


@section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script>
            const swiper = new Swiper(".mySwiper", {
                slidesPerView: 2.3,
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {

                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1.3,
                    spaceBetween: 10
                },
                980: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 2.3,
                    spaceBetween: 20
                },
                }

            });
        </script>
        <script>
            $(".image-box").click(function(event) {
            var previewImg = $(this).children("img");

            $(this)
                .siblings()
                .children("input")
                .trigger("click");

            $(this)
                .siblings()
                .children("input")
                .change(function() {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        var urll = e.target.result;
                        $(previewImg).attr("src", urll);
                        previewImg.parent().css("background", "transparent");
                        previewImg.show();
                        previewImg.siblings("p").hide();
                    };
                    reader.readAsDataURL(this.files[0]);
                });
              });

        </script>
@endsection
@endsection