@extends('plateforme.fabriquant.layouts.template')
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
@endsection

@php  
    if(Auth::user()->photo != "Mu-bull-gris.png") 
        $url0 = Storage::disk('s3')->temporaryUrl('users/'.Auth::user()->photo, now()->addMinutes(100));
    else 
        $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png';
@endphp
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife " href="/home">Accueil</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/communication">Communication</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/publicationOffre">Publier une offre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="etudeDeMarche">Etude de marché</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Mes notations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/nousContacter">Contacts</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <a class="nav-link nav-toute" href="#">
                                <img src=" {{asset('img/touteIcone.png') }}" alt="">
                                Toutes
                            </a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                <img src="{{$url0}}"  alt="">
                            </button>
                            <div class="modalModife">
                                <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <ul class="navbar-nav d-block">
                                                    <li class="nav-item">
                                                        <a href="{{ url('/logout') }}" class="nav-link" 
                                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        <div class="blockFormation">
            <h1 class="titleFormation">Espace communication</h1>
            <div class="blockFormationCard">
                <div class="cardFormation btn" id="btnDemandeFormation">
                    <div class="imgCardFormation">
                        <img src="{{asset('img/03Formation.png')}}" alt="">
                    </div>
                    <p class="titleCardFormation">Publier une publicité</p>
                </div>
                <a href="/webinar" class="cardFormation">
                    <div class="imgCardFormation">
                        <img src="{{asset('img/04Formation.png')}}" alt="">
                    </div>
                    <p class="titleCardFormation">Organiser un Webinar</p>
                </a>
            </div>
        </div>
        <div class="blockNosFormation">
            <div class="d-flex align-items-center">
                <p class="nosFormationsTitle">Mes  Webinars</p>
                <hr class="hrModife3">
                <a href="" class="voirToutBnt2">Voir Tout
                    <img src="{{asset('img/next.png')}}" alt="">
                 </a>
            </div>
            <div class="swiper-container mySwiper">
                <div class="swiper-wrapper">
                    <a href="" class="swiper-slide swiper-slide-Modife1">
                        <div class="cardNosFormations firstformation">
                            <div class="imgBlockNosFormations">
                                <img src="{{asset('img/formation1.png')}}" alt="">
                            </div>
                        </div>
                        <p class="titleNosFormations">GESTION DE PROJET</p>
                    </a>
                    <a href="" class="swiper-slide swiper-slide-Modife1">
                        <div class="cardNosFormations secondformation">
                            <div class="imgBlockNosFormations">
                                <img src="{{asset('img/formation2.png')}}" alt="">
                            </div>
                        </div>
                        <p class="titleNosFormations">PROGRAMMATION</p>
                    </a>
                    <a href="" class="swiper-slide swiper-slide-Modife1">
                        <div class="cardNosFormations treeformation">
                            <div class="imgBlockNosFormations">
                                <img src="{{asset('img/formation2.png')}}" alt="">
                            </div>
                        </div>
                        <p class="titleNosFormations">PROGRAMMATION</p>
                    </a>
                    <div class="swiper-slide swiper-slide-Modife1">Slide 4</div>
                </div>
            </div>
            <div class="d-flex align-items-center">
                <p class="nosFormationsTitle">Mes  Publicités</p>
                <hr class="hrModife3">
                <a href="" class="voirToutBnt2">Voir Tout
                    <img src="{{asset('img/next.png')}}" alt="">
                 </a>
            </div>
        </div>
    </div>
    <div class="blockDemander">
        <div class="cardDemanderUneFormation">
            <div class="d-flex">
                <button class="btn btnRetourDemande"><img src="{{asset('img/retourOrange.png')}}" alt=""></button>
                <p class="titleFormation">Demander une formation</p>
            </div>
            <form action="">
                <div class="formFormation">
                    <div class="form-group hide1 colM">
                        <label class="labelFormation"  for="dm">N° DM</label>
                        <input type="text" class="form-control inputFormation" id="dm" required>
                    </div>
                    <div class="form-group hide1 colM">
                        <label class="labelFormation" for="application">Type d’application</label>
                        <select class="form-control inputFormation inputUtilisateur selectModife" id="application" required>
                            <option></option>
                            <option>Ingénieur de surface</option>
                            <option>Ingénieur de surface</option>
                        </select>
                    </div>
                    <div class="form-group hide1 colM">
                        <label class="labelFormation" for="typeProduit">Type de produit</label>
                        <select class="form-control inputFormation inputUtilisateur selectModife" id="typeProduit" required>
                            <option></option>
                            <option>Ingénieur de surface</option>
                            <option>Ingénieur de surface</option>
                        </select>
                    </div>
                    <div class="form-group hide1 colL">
                        <label class="labelFormation"  for="Entreprise2">Entreprise</label>
                        <input type="text" class="form-control inputFormation" id="Entreprise2" required>
                    </div>
                    <div class="form-group hide1 colM">
                        <label class="labelFormation"  for="activite2">Domaine d’activité</label>
                        <select class="form-control inputFormation inputUtilisateur selectModife1" id="activite2" required>
                            <option></option>
                            <option>Ingénieur de surface</option>
                            <option>Ingénieur de surface</option>
                        </select>
                    </div>
                    <div class="form-group hide1 colL">
                        <label class="labelFormation"  for="prenonDemande">Prénom</label>
                        <input type="text" class="form-control inputUtilisateur inputFormation" id="prenonDemande" required>
                    </div>
                    <div class="form-group hide1 colM">
                        <label class="labelFormation"  for="nonDemande">Nom</label>
                        <input type="text" class="form-control inputUtilisateur inputFormation" id="nonDemande" required>
                    </div>
                    <div class="form-group hide1 colM">
                        <label class="labelFormation"  for="email1">E-mail</label>
                        <input type="email" class="form-control inputUtilisateur inputFormation" id="email1" required>
                    </div>
                    <div class="form-group hide1 colM">
                        <label class="labelFormation"  for="telephone1">Téléphone</label>
                        <input type="number" class="form-control inputUtilisateur inputFormation" id="telephone1" required>
                    </div>
                    <div class="form-group hide1 colL">
                        <label class="labelFormation"  for="password1">Délai de votre demande</label>
                        <input type="date" class="form-control inputUtilisateur inputFormation" id="password1" required>
                    </div>
                    <div class="groupContinuer">
                        <button type="submit" class="btn btn-demande" id="continuerEntreprise">Envoyer la demande</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<p class="textCopyright1">© 2021d Test&Rex. Tous droits réservés.</p>


@section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/piexif.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/sortable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('js/fileInput/fileinput.min.js') }}"></script>
        <script src="{{ asset('js/fileInput/LANG.js') }}"></script>
        <script>
            const swiper = new Swiper(".mySwiper", {
                slidesPerView: 2.3,
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {

                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1.3,
                    spaceBetween: 10
                },
                980: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 2.3,
                    spaceBetween: 20
                },
                }

            });
        </script>
@endsection
@endsection