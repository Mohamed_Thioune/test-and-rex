@extends('plateforme.fabriquant.layouts.template')
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
@endsection
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
            <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife " href="/homeFabriquant">Accueil</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife" href="/communication">Communication</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/publicationOffre">Publier une offre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/etudeDeMarche">Etude de marché</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Mes notations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Contacts</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <a class="nav-link nav-toute" href="#">
                                <img src=" {{asset('img/touteIcone.png') }}" alt="">
                                Toutes
                            </a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="circleImgNav">
                                <img src=" {{asset('img/img1Cercle.png') }}" alt="">
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        <div class="flexJustifyElement">
            <h1 class="textTitleOne">Découvrir ce qui se passe dans la communauté </h1>
        </div>
        <div class="swiper-container mySwipe">
            <div class="swiper-wrapper">
                <button  class="btn swiper-slide btnVeille active">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeToutes.png')}}" alt="">
                    Toutes
                </button>
                <button  class="btn  swiper-slide btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeTransformateur.png')}}" alt="">
                    Transformateur
                </button>
                <button  class="btn swiper-slide btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconedisjoncteur.png')}}" alt="">
                    Disjoncteur
                </button>

                <button  class="btn swiper-slide btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeprotecteur.png')}}" alt="">
                    Protection et Automatiqme
                </button>
                <button  class="btn swiper-slide btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconediagnostique.png')}}" alt="">
                    Diagnostic câbles souterrains
                </button>
                <button  class="btn swiper-slide btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeAutres.png')}}" alt="">
                    Autres applications
                </button>
            </div>
        </div>
    </div>
    <div class="publierRex">
        <div class="homeBlockOneCategory">
            <div class="head">
               <button id="btnRetourPub1" class="btn btnRetour">
                   <img src="{{asset('img/list_check.png')}}" alt="">
               </button>
                <p>Publier un REX pour la communauté</p>
            </div>
            <div class="rowCard">
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex">
                    <p class="titleCard">REX <br>Transformateur</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/01.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex">
                    <p class="titleCard">REX <br>Diagnostic cables souterrains</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/02.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex">
                    <p class="titleCard">REX <br>Protection & Automatisme</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/03.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex">
                    <p class="titleCard">REX <br>Moteur & Alternateur</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/05.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex">
                    <p class="titleCard">REX <br>Disjoncteur</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/06.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex">
                    <p class="titleCard">REX <br>Autres applications</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/07.png')}}" alt="">
                    </div>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="ModalPublication" tabindex="-1" role="dialog" aria-labelledby="ModalPublicationTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="elementImgText">
                                <img src="{{asset('img/retourOrange.png')}}" alt="">
                                <h5 class="modal-title" id="exampleModalLongTitle">Faire un REX Transformateur  </h5>
                            </div>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btnPublic dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Tout le monde
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item dropdown-itemModife" href="#">Dropdown link</a>
                                    <a class="dropdown-item dropdown-itemModife" href="#">Dropdown link</a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="photoAndInputPub">
                                <div class="profilPhoto">
                                    <img src=" {{asset('img/imgTwoCircle.png') }}" alt="">
                                </div>
                                <div>
                                    <p class="nameDePublication">Ibrahima Fall</p>
                                    <p class="localisationPub">Sénégal</p>
                                </div>
                            </div>
                            <form class="formPublication" action="">
                                <textarea class="modifeTextArea" placeholder="Faites un retour d’experience sur les transformateurs ..." name="" id="" rows="5"></textarea>
                                    <div class="blockFooterMap">
                                        <input id="image-attachment" name="imageattachment[]" type="file" multiple class="file-loading">
                                       <div class="btnGroup7">
                                           <button class="btn btnMap">
                                               <img src="{{asset('img/Icon-map.png') }}" alt="">
                                           </button>
                                           <button class="btn btnUser">
                                               <img src="{{asset('img/IconUser.png') }}" alt="">
                                           </button>
                                       </div>
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="blockDesPublication blockC">
    <div class="pubBlok">
        <div class="head2">
            <p class="textPublication">Publications</p>
            <img src="{{asset('img/Icon7.png')}}" alt="">
            <p class="touteslesPub">Toutes les publications</p>
        </div>
    </div>
    <div class="cardPublication">
        <div class="headElement">
            <div class="blockPhoto">
                <img src="{{asset('img/pierre.png')}}" alt="">
            </div>
            <div class="blockNameDate">
                <p class="nameProfilPub">Charles DIOP</p>
                <p class="heurePub">Hier à 08:44</p>
            </div>
            <div class="blockTroisPoint">
                <button class="btn bntTroisPoint">
                    <img src="{{asset('img/IconTrais.png')}}" alt="">
                </button>
            </div>
        </div>
        <div class="contentCard">
            <p class="textDescriptionPublication">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            <div class="elementPub">
                <video controls>
                    <source src="{{asset('videos/vPub1.mp4')}}" type="video/mp4;charset=UTF-8">
                    <source src="{{asset('videos/vPub1.webm')}}" type='video/webm; codecs="vp8, vorbis"' />
                    <source src="{{asset('videos/vPub1.ogg')}}" type='video/ogg; codecs="theora, vorbis"' />
                </video>
            </div>
            <div class="blockLike">
                <div class="elementBtn">
                    <button class="btn btnLike  btnPertinent">
                        <img src="{{asset('img/like1.png')}}" alt="">
                        Pertinent
                    </button>
                    <button class="btn btnLike  btnInteressant">
                        <img src="{{asset('img/interessant1.png')}}" alt="">
                        Interessant
                    </button>
                    <button class="btn btnLike  btnEnSavoirPlus">
                        <img src="{{asset('img/enSavoirPlus.png')}}" alt="">
                        En savoir +
                    </button>
                </div>
                <div class="partageSpace">
                    <img src="{{asset('img/share.png')}}" alt="">
                    <p class="partageText">Partager</p>
                </div>
            </div>
            <div class="blockCompterLike">
                <div class="elementCompterLike">
                    <img class="aimeIc" src="{{asset('img/aime.png')}}" alt="">
                    <p class="aimeText">34 J’aime</p>
                </div>
                <div class="elementCompterLike">
                    <img class="likeInteresseIc" src="{{asset('img/likeInteresse.png')}}" alt="">
                    <p class="aimeText">34 J’aime</p>
                </div>
                <div class="elementCompterLike">
                    <img class="likeInteresseIc" src="{{asset('img/commentaire.png')}}" alt="">
                    <p class="aimeText">34 J’aime</p>
                </div>
            </div>
            <form action="" class="">
                <div class="blockInputCommentaire">
                    <div class="photoCommentaire">
                        <img src="{{asset('img/imgTwoCircle.png')}}" alt="">
                    </div>
                    <div class="blockCommente">
                    <input type="text" placeholder="Commenter ...">
                    <button class="btn btnEmojie">
                        <img src="{{asset('img/Smile.png')}}" alt="">
                    </button>
                </div>
                <div class="photoInputFile">
                    <img src="{{asset('img/Icon-photo.png')}}" alt="">
                </div>
                <!--   <div class="photoInputFile">
                       <input type="file">
                   </div>-->
                <button class="btn btnEnvoyer">Envoyer</button>
                </div>

                <div class="commentaireBlock">
                    <div class="reponseCommentaireBlock">
                        <div class="headResponseCommenatire">
                            <div class="imgProfilResponse">
                                <img src="{{asset('img/Ellipse-3.png')}}" alt="">
                            </div>
                            <p class="nameResponse">Aida NGOM</p>
                            <p class="timeResponse">Il y’a 2 min</p>
                        </div>
                            <p class="responseCommentaireText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <div class="likeBlockResponse">
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/aime.png')}}" alt="">
                                    <p>1.2K J’aime</p>
                                </button>
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/share.png')}}" alt="">
                                    <p>Répondre</p>
                                </button>
                            </div>
                    </div>

                    <div class="sousReponseCommentaireBlock">
                        <div class="headResponseCommenatire">
                            <div class="imgProfilResponse">
                                <img src="{{asset('img/Ellipse-1.png')}}" alt="">
                            </div>
                            <p class="nameResponse">Salif DIALLO</p>
                            <p class="timeResponse">A l’instant</p>
                        </div>
                            <p class="sousResponseCommentaireText">Adipiscing elit, sed do eiusmod tempor incididunt </p>
                            <div class="likeBlockResponse">
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/aime.png')}}" alt="">
                                    <p>1.2K J’aime</p>
                                </button>
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/share.png')}}" alt="">
                                    <p>Répondre</p>
                                </button>
                            </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="cardPublication">
        <div class="headElement">
            <div class="blockPhoto">
                <img src="{{asset('img/pierre.png')}}" alt="">
            </div>
            <div class="blockNameDate">
                <p class="nameProfilPub">Charles DIOP</p>
                <p class="heurePub">Hier à 08:44</p>
            </div>
            <div class="blockTroisPoint">
                <button class="btn bntTroisPoint">
                    <img src="{{asset('img/IconTrais.png')}}" alt="">
                </button>
            </div>
        </div>
        <div class="contentCard">
            <p class="textDescriptionPublication">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
            <div class="elementPub">
                <a class="feed-shared-image" href="">
                    <img src="{{asset('img/imgPub.png')}}" alt="">
                </a>
            </div>
            <div class="blockLike">
                <div class="elementBtn">
                    <button class="btn btnLike  btnPertinent">
                        <img src="{{asset('img/like1.png')}}" alt="">
                        Pertinent
                    </button>
                    <button class="btn btnLike  btnInteressant">
                        <img src="{{asset('img/interessant1.png')}}" alt="">
                        Interessant
                    </button>
                    <button class="btn btnLike  btnEnSavoirPlus">
                        <img src="{{asset('img/enSavoirPlus.png')}}" alt="">
                        En savoir +
                    </button>
                </div>
                <div class="partageSpace">
                    <img src="{{asset('img/share.png')}}" alt="">
                    <p class="partageText">Partager</p>
                </div>
            </div>
            <div class="blockCompterLike">
                <div class="elementCompterLike">
                    <img class="aimeIc" src="{{asset('img/aime.png')}}" alt="">
                    <p class="aimeText">34 J’aime</p>
                </div>
                <div class="elementCompterLike">
                    <img class="likeInteresseIc" src="{{asset('img/likeInteresse.png')}}" alt="">
                    <p class="aimeText">34 J’aime</p>
                </div>
                <div class="elementCompterLike">
                    <img class="likeInteresseIc" src="{{asset('img/commentaire.png')}}" alt="">
                    <p class="aimeText">34 J’aime</p>
                </div>
            </div>
            <form action="" class="">
                <div class="blockInputCommentaire">
                    <div class="photoCommentaire">
                        <img src="{{asset('img/imgTwoCircle.png')}}" alt="">
                    </div>
                    <div class="blockCommente">
                    <input type="text" placeholder="Commenter ...">
                    <button class="btn btnEmojie">
                        <img src="{{asset('img/Smile.png')}}" alt="">
                    </button>
                </div>
                <div class="photoInputFile">
                    <img src="{{asset('img/Icon-photo.png')}}" alt="">
                </div>
                <!--   <div class="photoInputFile">
                       <input type="file">
                   </div>-->
                <button class="btn btnEnvoyer">Envoyer</button>
                </div>

                <div class="commentaireBlock">
                    <div class="reponseCommentaireBlock">
                        <div class="headResponseCommenatire">
                            <div class="imgProfilResponse">
                                <img src="{{asset('img/Ellipse-3.png')}}" alt="">
                            </div>
                            <p class="nameResponse">Aida NGOM</p>
                            <p class="timeResponse">Il y’a 2 min</p>
                        </div>
                            <p class="responseCommentaireText">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <div class="likeBlockResponse">
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/aime.png')}}" alt="">
                                    <p>1.2K J’aime</p>
                                </button>
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/share.png')}}" alt="">
                                    <p>Répondre</p>
                                </button>
                            </div>
                    </div>

                    <div class="sousReponseCommentaireBlock">
                        <div class="headResponseCommenatire">
                            <div class="imgProfilResponse">
                                <img src="{{asset('img/Ellipse-1.png')}}" alt="">
                            </div>
                            <p class="nameResponse">Salif DIALLO</p>
                            <p class="timeResponse">A l’instant</p>
                        </div>
                            <p class="sousResponseCommentaireText">Adipiscing elit, sed do eiusmod tempor incididunt </p>
                            <div class="likeBlockResponse">
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/aime.png')}}" alt="">
                                    <p>1.2K J’aime</p>
                                </button>
                                <button class="btn elementLikeResponse">
                                    <img src="{{asset('img/share.png')}}" alt="">
                                    <p>Répondre</p>
                                </button>
                            </div>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>

@section('scripts')
<script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/piexif.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/sortable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('js/fileInput/fileinput.min.js') }}"></script>
        <script src="{{ asset('js/fileInput/LANG.js') }}"></script>
        <script>
            const swiper = new Swiper(".mySwipe", {
                slidesPerView: 'auto',
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },

            });
        </script>


@endsection
@endsection