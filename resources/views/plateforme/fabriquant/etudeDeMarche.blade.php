@extends('plateforme.fabriquant.layouts.template')
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
@endsection
@php  
    if(Auth::user()->photo != "Mu-bull-gris.png") 
        $url0 = Storage::disk('s3')->temporaryUrl('users/'.Auth::user()->photo, now()->addMinutes(100));
    else 
        $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png';
@endphp
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife " href="/home">Accueil</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/communication">Communication</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/publicationOffre">Publier une offre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="etudeDeMarche">Etude de marché</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Mes notations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/nousContacter">Contacts</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <a class="nav-link nav-toute" href="#">
                                <img src=" {{asset('img/touteIcone.png') }}" alt="">
                                Toutes
                            </a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                <img src="{{$url0}}"  alt="">
                            </button>
                            <div class="modalModife">
                                <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <ul class="navbar-nav d-block">
                                                    <li class="nav-item">
                                                        <a href="{{ url('/logout') }}" class="nav-link" 
                                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div> . .
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="blockNoAbonne">
   
    <p class="titleNoAbonne">Etude de marché</p>
   
    <div class="cardNoAbonne">
        <div class="griseBlock"></div>
        <div class="blockFormationCard">
            <button class="cardFormation btn" id="btnDemandeFormation">
                <div class="imgCardFormation2">
                    <img src="{{asset('img/ic7.png')}}" alt="">
                </div>
                <p class="titleCardFormation">Accés aux idées d'amélioration des utilisateurs</p>
            </button>
            <button class="cardFormation btn" id="btnDemandeFormation">
                <div class="imgCardFormation2">
                    <img src="{{asset('img/ic8.png')}}" alt="">
                </div>
                <p class="titleCardFormation">Publier une enquete ou un sondage</p>
            </button>
        </div>
        <img class="ICO" src="{{asset('img/ICO.png')}}" alt="">
        <p class="pasAbonneText">Vous n'etes pas encore abonné</p>
        <p class="accessServiceText">Pour accéder à ces services vous devez vous abonner à l'une de nos formules.
         Veuillez contacter notre service marketing pour les modalités d'adhésion.</p>
        <button id="btnIdeeAmelioration" class="btn btnVousAbonne">Contacter le service marketing </button>
    </div>
</div>
<div class="blockideeAmelioration">
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        <div class="blockVeilleCard">
            <div class="btnGroupVeille">
                <button id="btnComparatifsVeille" class="btn btnVeille active">
                    <img class="iconeVeilleBnt" src="{{asset('img/vT3.png')}}" alt="">
                    Idées d'amélioration
                </button>
            </div>
           <div class="comparatifsProduitsBlock">
            <form action="" class="filtreVeille">
                    <p class="filtreText">Filtre :</p>
                    <div class="inputFiltre">
                        <select id="selectTech1" class="form-control selectModifeVeille">
                            <option value="0"></option>
                            <option value="entreprise">Sénégal</option>
                            <option value="electricienEntreprise">Canada</option>
                            <option value="independantElectricient">Afrique du sud</option>
                        </select>
                        <select id="selectTech2" class="form-control selectModifeVeille" placeholder="Fabricant">
                            <option value="0"></option>
                            <option value="entreprise">Utilisateur</option>
                        </select>
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form2" class="form-control" />
                            </div>
                            <button type="button" placeholder="Mots-clés ..." class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </div>
                </form>

           </div>

        </div>
        <div class="cardIdeeAmelioration" data-toggle="modal" data-target="#exampleModal">
            <p class="produitIdee">Disjoncteur</p>
            <div class="elementImgCard">
                <img src="{{asset('img/iconIdee.png')}}" alt="">
            </div>
            <hr class="hrModife4">
            <div class="elementsText">
                <p class="title">Lorem ipsum dolore sit ametis</p>
                <ul>
                    <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur   ipsam voluptatem quia voluptas sit ase...</li>
                </ul>
            </div>
        </div>

        <div class="cardIdeeAmelioration">
            <p class="produitIdee">Disjoncteur</p>
            <div class="elementImgCard">
                <img src="{{asset('img/iconIdee.png')}}" alt="">
            </div>
            <hr class="hrModife4">
            <div class="elementsText">
                <p class="title">Lorem ipsum dolore sit ametis</p>
                <ul>
                    <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur   ipsam voluptatem quia voluptas sit ase...</li>
                </ul>
            </div>
        </div>

        <div class="cardIdeeAmelioration">
            <p class="produitIdee">Disjoncteur</p>
            <div class="elementImgCard">
                <img src="{{asset('img/iconIdee.png')}}" alt="">
            </div>
            <hr class="hrModife4">
            <div class="elementsText">
                <p class="title">Lorem ipsum dolore sit ametis</p>
                <ul>
                    <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur   ipsam voluptatem quia voluptas sit ase...</li>
                </ul>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <p class="produitIdee">Disjoncteur</p>
            <div class="modal-header-modife">
                <img src="{{asset('img/iconIdee.png')}}" alt="">
                <p>Lorem ipsum dolore sit ametis</p>
            </div>
            <div class="modal-body modalEtudeMarche">
                <ul>
                    <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur   ipsam voluptatem quia voluptas sit ase. <br> Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo</li>
                    <li> Inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</li>
                </ul>
            </div>
            <div class="footerEtude">
                <p class="suggereText">Suggeré par </p>
                <div class="auteurSuggere">
                    <img src="{{asset('img/imgTwoCircle.png')}}" alt="">
                </div>
                <p class="auteur">Ibrahima FALL</p>
            </div>

            </div>
        </div>
        </div>
        <!-- fin Modal -->

    </div>
</div>
</div>

<p class="textCopyright1">© 2021d Test&Rex. Tous droits réservés.</p>


@section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script>
            const swiper = new Swiper(".mySwiper", {
                slidesPerView: 2.3,
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {

                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1.3,
                    spaceBetween: 10
                },
                980: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 2.3,
                    spaceBetween: 20
                },
                }

            });
        </script>
       

@endsection
@endsection