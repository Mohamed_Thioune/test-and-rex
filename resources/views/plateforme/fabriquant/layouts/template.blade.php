<html>
<head>
    <meta content="TARIF" property="og:title">
    <meta content="TARIF" property="twitter:title">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('swiper/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/platforme.css') }}">
    @yield('css')
</head>
<body>
    @php

    $events = DB::Table('events')
                ->select('events.*')
                ->where('events.day', '>=', now())
                ->orderBy('events.day')
                ->get();

    $product = DB::Table('products')
                ->select('products.*', 'enterprises.enterprise')
                ->join('enterprises', 'products.user_id', 'enterprises.user_id')
                ->first();

    $post_user_count = DB::Table('posts')
                    ->where('posts.user_id', Auth::id())
                    ->count();

        if(Auth::user()->type == "sample"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }else if(Auth::user()->type == "fabricant"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }

 
    @endphp
<div class="blockGeneral1">
    <div class="blockDrapreau">
        <div class="pays">
            <img src=" {{asset('img/drapeau/Andorra.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/United-Arab-Emirates.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Afghanistan.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/African-Union.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Barbuda.png')}}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Anguilla.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Albania.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Armenia.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Andorra.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Guinea.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Gambia.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Greenland.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Ghana.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/AMS.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Angola.png') }}" alt="">
        </div>

    </div>
    <div class="blockMap">
        <img src=" {{asset('img/Icon-map.png') }}" alt="">
        @if(isset($user))
            <span class="pays">&nbsp;{{$user->country ? $user->country : '🔎' }}</span>
        @endif    
    </div>
</div>
<div>
    @yield('content-nav')
</div>
<div class="genaralBlock">
    <div class="container">
        <div class="contentPageGeneral">
            <div class="blockleft">
                <div class="blockVideo">
                    <img src="{{asset('img/pubVideo.png')}}" alt="">
                    <div class="blockAbsolute">
                        <div class="elementPlay">
                            <img src="{{asset('img/Icon.png')}}" alt="">
                        </div>
                        <p class="textTestRex">Test&Rex</p>
                        <p class="textMonde">Le monde de l’électricité du futur !</p>
                    </div>
                </div>
                <div class="blockProfilLeft">
                    @if(isset($user))
                        <a href="{{route('users.edit', Auth::id())}}" class="btn btnTrais">
                            <img src="{{asset('img/IconTrais.png')}}" alt="">
                        </a>
                        <div class="photoBlockProfil">
                            <img src="{{asset('images/uploads')}}/{{ Auth::user()->photo ? Auth::user()->photo : 'Mu-bull-gris.png'}}" alt="">
                        </div>
                        
                            <p class="nameProfil">
                                {{ isset($user->firstName) ? $user->firstName. " " .$user->lastName : $user->enterprise }}
                            </p>
                        <p class="fonction">{{ isset($user->function) ? $user->function : $user->expertiseDomain }}</p>
                        <hr class="hrModife">
                        <div class="entrepriseBlock">
                            <img class="imgHome" src="{{asset('img/Union.png')}}" alt="">
                            <p class="entrepriseProfil">Entreprise : <span> {{ $user->enterprise }}</span></p>
                        </div>
                    @endif
                    <div class="pubSuivi">
                        <div class="element">
                            @if(!isset($post_user_count))
                                <p class="numberPub">0</p>
                            @else
                                <p class="numberPub">{{$post_user_count}}</p>
                            @endif 
                            
                            <p class="pubText">Publications</p>
                        </div>
                        <div class="element">
                            <p class="numberPub">0k</p>
                            <p class="pubText">Suivi par</p>
                        </div>
                        <div class="element">
                            <p class="numberPub">0</p>
                            <p class="pubText">Suivis</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentPage">
                @yield('content-body-plateforme')
            </div>
            <div class="blockRight ">
                <div class="annonnce">
                    <img src="{{asset('img/ImgRight2.png')}}" alt="">
                    <div class="blockAbsolute">
                        <p class="annonceText">Annonce</p>
                        <p class="particpe">Participez au IESE 2021</p>
                        <div class="rectangle7">
                            <img src="{{asset('img/Vector7.png')}}" alt="">
                            <p class="obenirText">OBTENIR VOTRE TICKET</p>
                        </div>
                    </div>
                </div>
                
                <div class="blockevenement">
                    <div class="flexElement">
                        <p class="evenementText">Evenements</p>
                        <a href="{{route('events.index')}}" class="voirToutBnt">Voir Tout
                            <img src="{{asset('img/IconVoirTout.png')}}" alt="">
                        </a>
                    </div>
                        @for($i=0; $i<2; $i++)
                            @if(isset($events[$i]))
                                @php
                                    $split = explode(" ", $events[$i]->day);
                                    $date = explode("-", $split[0]);
                                    $time = explode(":",$split[1]);
                                    $months = array('01'=>'JAN', '02'=>'FEB', '03'=>'MAR', '04'=>'AVR', '05'=>'MAY', '06'=>'JUN', '07'=>'JUL', '08'=>'AUG', '09'=>'SEP', '10'=>'OCT', '11'=>'NOV', '12'=>'DEC');
                                @endphp
                                <a href="{{$events[$i]->link}}" target="_blank"  style="{{$i == 0 ? 'font-weight:bold;color:#FF871C':'font-weight:bold; color:#42AB44'}}" class="blockDateEvenement {{$i == 0 ? '': 'vertBlock'}}">
                                    <div class="dateBlock">
                                        <p class="dayCalendar">{{ $date[2] }}</p>
                                        <p class="moisCalendar">{{ $months[$date[1]] }}</p>
                                    </div>
                                    <div class="lorenDate">
                                        <p class="textDescription">{{$events[$i]->title}}</p>
                                        <p class="dataTime">{{ $date[2] ." ". $months[$date[1]] ." ". $date[0] ." à ". $time[0] .":". $time[1] }}</p>
                                    </div>
                                </a>
                            @endif
                        @endfor
                </div>

                <div class="blockPubMarketPlace">
                    @if(isset($user))
                        <p class="evenementText"><a style="color:black" href="{{route('products.index')}}">Marketplace</a>&nbsp;@if($user->type == "fabricant" && $user->state == 2) <a href="{{route('products.create')}}" style="color:black"> <i class="fas fa-cart-plus"></i> </a> @endif</p>
                    @endif
                    @if($product)
                    <div class="marketplace">
                        <img src="{{ $product->photo ? asset('images/uploads').'/'.$product->photo : asset('img/ProjectImg.png')}}" alt="">
                        <div class="blockAbsolute">
                            <div class="blockProduit">
                                <p><b>{{$product->name}} </b> <span>- {{$product->enterprise}}</span></p>
                            </div>
                            <div class="blockTilteProduit">
                                <p>{{$product->type}}}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                    <br>
                </div>
            </div>

        </div>


        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/popper.js') }}"></script>
        <script src="{{ asset('bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/platforme.js') }}"></script>
        @yield('scripts')
</div>

</body>