@extends('plateforme.fabriquant.layouts.template')
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
@endsection
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife " href="/homeFabriquant">Accueil</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/communication">Communication</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/publicationOffre">Publier une offre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/etudeDeMarche">Etude de marché</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Mes notations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Contacts</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <a class="nav-link nav-toute" href="#">
                                <img src=" {{asset('img/touteIcone.png') }}" alt="">
                                Toutes
                            </a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <div class="circleImgNav">
                                <img src="{{asset('images/uploads')}}/{{ Auth::user()->photo ? Auth::user()->photo : 'Mu-bull-gris.png'}}" alt="">
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        <div class="organiserUnWebinar">
            <div class="headOrganiserUnWebinar">
               <a href="/communication">
                  <img class="prevWebinar" src="{{asset('img/prev.png') }}" alt="">
               </a>
                <img class="barRight" src="{{asset('img/bar_right.png') }}" alt="">
                <p class="titleFormation">Organiser un webinar</p>
            </div>
            <form action="">
                <div class="formFormation">
                    <div class="form-group  colL">
                        <label class="labelFormation"  for="dm">Titre du webinar</label>
                        <input type="text" class="form-control inputFormation"  required>
                    </div>
                    <div class="form-group  colL">
                        <span class="labelFormation d-block">Banniere du webinar</label>
                        <div class="image-input">
                            <input type="file" accept="image/*" id="imageInput">
                            <label for="imageInput" class="image-button "> <img class="imageBannier" src="{{asset('img/bannier1.png') }}" alt=""></label>
                            <img src="" class="image-preview">
                            <span class="change-image">Choisissez une autre image</span>
                        </div>
                    </div>
                    <div class="form-group  colL">
                        <label class="labelFormation"  for="Entreprise2">Description du webinar</label>
                        <textarea class="modifeTextArea2" name="" id="" rows="6"></textarea>
                    </div>
                    <div class="form-group  colM">
                        <label class="labelFormation"  for="prenonDemande">Info 1</label>
                        <input type="text" class="form-control inputUtilisateur inputFormation" required>
                    </div>
                    <div class="form-group  colM">
                        <label class="labelFormation"  for="nonDemande">Info 2</label>
                        <input type="text" class="form-control inputUtilisateur inputFormation"  required>
                    </div>
                    <div class="form-group  colL">
                        <label class="labelFormation"  for="email1">Date de début</label>
                        <input type="email" class="form-control inputUtilisateur inputFormation"  required>
                    </div>
                    <div class="groupContinuer">
                        <button type="submit" class="btn btn-demande">Creer le webinar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<p class="textCopyright1">© 2021d Test&Rex. Tous droits réservés.</p>


@section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/piexif.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/sortable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('js/fileInput/fileinput.min.js') }}"></script>
        <script src="{{ asset('js/fileInput/LANG.js') }}"></script>
        <script>
            const swiper = new Swiper(".mySwiper", {
                slidesPerView: 2.3,
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {

                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1.3,
                    spaceBetween: 10
                },
                980: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 2.3,
                    spaceBetween: 20
                },
                }

            });
        </script>
        <script>
            $('#imageInput').on('change', function() {
            $input = $(this);
            if($input.val().length > 0) {
                fileReader = new FileReader();
                fileReader.onload = function (data) {
                $('.image-preview').attr('src', data.target.result);
                }
                fileReader.readAsDataURL($input.prop('files')[0]);
                $('.image-button').css('display', 'none');
                $('.image-preview').css('display', 'block');
                $('.change-image').css('display', 'block');
            }
        });

        $('.change-image').on('click', function() {
            $control = $(this);
            $('#imageInput').val('');
            $preview = $('.image-preview');
            $preview.attr('src', '');
            $preview.css('display', 'none');
            $control.css('display', 'none');
            $('.image-button').css('display', 'block');
        });
        </script>
@endsection
@endsection