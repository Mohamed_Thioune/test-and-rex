@extends('plateforme.sample.template')
@php
    if($user->photo != "Mu-bull-gris.png") 
        $url0 = Storage::disk('s3')->temporaryUrl('users/'.$user->photo, now()->addMinutes(100));
    else 
        $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png';         
@endphp
@section('css')
<link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
<link rel="stylesheet" href="{{ asset('css/swiper-bundle.min.css') }}">
@endsection
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
            <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife " href="{{ route('home') }}">Accueil</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife" href="/veille">Veille technologique</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/support">Support tech.</a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife" href="/formation">Formations</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/notation">Noter un Fabricant</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/nousContacter">Nous contacter</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                <img src="{{$url0}}"  alt="">
                            </button>
                            <div class="modalModife">
                                <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <ul class="navbar-nav d-block">
                                                    <li class="nav-item">
                                                        <a href="{{ url('/logout') }}" class="nav-link"
                                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        <div class="blockVeilleCard">
            <div class="btnGroupVeille">
                    <button id="btnComparatifsMarques" class="btn btnVeille active">
                    <img class="iconeVeilleBnt" src="{{asset('img/vT1.png')}}" alt="">
                        Comparatif de marques
                </button>
                <button id="btnNouvautesFabriquant" class="btn btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/vT2.png')}}" alt="">
                    Classement Fabricants
                </button>
                <button id="btnIdeeProduits" class="btn btnVeille">
                    <img class="iconeVeilleBnt" src="{{asset('img/vT3.png')}}" alt="">
                    Classement Produits
                </button>
            </div>
            <div class="elementComparatifDeMarque">
                <div class="blockAvisPese">
                    <img src="{{asset('img/ICO-7.png')}}" alt="">
                    <p class="avis-text">Notre équipe est entrain de travailler avec ardeur sur cette fonctionnalité, 
                        elle sera disponible bientôt.<br>
                        Restez connecté !</p>
                </div>
                <form action="">
                    <div class="form-group inputNotation">
                        <label class="labelAccount labelDomaine"  >Choisir un domaine :</label>
                        <select  class="form-control selectModifeVeille">
                            <option value="0">Parcours client</option>
                            <option value="1"></option>
                        </select>
                    </div>
                    <div class="form-group mb-4">
                        <label class="labelAccount labelDomaine mb-2"  >Choisir un domaine :</label>
                        <div class="inputNotation2">
                            <select  class="form-control selectModifeVeille">
                                <option value="0">Megger</option>
                                <option value="1"></option>
                            </select>
                            <p class="vsText">vs</p>
                            <select  class="form-control selectModifeVeille">
                                <option value="0">Schneider</option>
                                <option value="1"></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="labelAccount labelDomaine mb-2"  >Choisir les produits :</label>
                        <div class="inputNotation2">
                            <div class="selectPA">
                                <select  class="form-control selectModifeVeille">
                                    <option value="0">Produit A</option>
                                    <option value="1"></option>
                                </select>
                                <div class="rate">
                                    <input class="inputModifeElement1" type="radio" id="star5" name="rate" value="5" />
                                    <label for="star5" title="text">5 stars</label>
                                    <input class="inputModifeElement1" type="radio" id="star4" name="rate" value="4" />
                                    <label for="star4" title="text">4 stars</label>
                                    <input class="inputModifeElement1" type="radio" id="star3" name="rate" value="3" />
                                    <label for="star3" title="text">3 stars</label>
                                    <input class="inputModifeElement1" type="radio" id="star2" name="rate" value="2" />
                                    <label for="star2" title="text">2 stars</label>
                                    <input class="inputModifeElement1" type="radio" id="star1" name="rate" value="1" />
                                    <label for="star1" title="text">1 star</label>
                                </div>
                            </div>
                            <p class="vsText2">vs</p>
                            <div class="selectPA2">
                                <select  class="form-control selectModifeVeille2">
                                    <option value="0">Produit Y</option>
                                    <option value="1"></option>
                                </select>
                                <div class="rating">
                                    <input class="inputModifeElement" type="radio" id="star1-1" name="rating" value="1-1" /><label for="star1-1"></label>
                                    <input class="inputModifeElement" type="radio" id="star2-2" name="rating" value="2-2" /><label for="star2-2"></label>
                                    <input class="inputModifeElement" type="radio" id="star3-3" name="rating" value="3-3" /><label for="star3-3"></label>
                                    <input class="inputModifeElement" type="radio" id="star4-4" name="rating" value="4-4" /><label for="star4-4"></label>
                                    <input class="inputModifeElement" type="radio" id="star5-5" name="rating" value="5-5" /><label for="star5-5"></label>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn btnSoomettreIdee">Envoyer la note</button>
                    </div>
                </form>
            </div>
            <div class="elementClassementFabriquant">
                <form action="" class="filtreVeille">
                    <p class="filtreText">Filtre :</p>
                    <div class="inputFiltre">
                        <select id="selectTech1" class="form-control selectModifeVeille">
                            <option value="0"></option>
                            <option value="entreprise">Sénégal</option>
                            <option value="electricienEntreprise">Canada</option>
                            <option value="independantElectricient">Afrique du sud</option>
                        </select>
                        <select id="selectTech2" class="form-control selectModifeVeille" placeholder="Fabricant">
                            <option value="0"></option>
                            <option value="entreprise">Utilisateur</option>
                        </select>
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form2" class="form-control" />
                            </div>
                            <button type="button" placeholder="Mots-clés ..." class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </div>
                </form>
                <div class="">
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille2">
                            <img src="{{asset('img/scVERT.png') }}" alt="">
                            <div class="abosluteBackground2"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">Schneider</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                           <p class="numberEtoile">1</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille2">
                            <img src="{{asset('img/scABC.png') }}" alt="">
                            <div class="abosluteBackground2"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">ABB</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                           <p class="numberEtoile">2</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile "></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille2">
                            <img src="{{asset('img/scNexans.png') }}" alt="">
                            <div class="abosluteBackground2"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">Nexans</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                           <p class="numberEtoile">3</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille2">
                            <img src="{{asset('img/scMegger.png') }}" alt="">
                            <div class="abosluteBackground2"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">Megger</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                           <p class="numberEtoile">1</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <div class="elementClassementProduit">
                <form action="" class="filtreVeille">
                    <p class="filtreText pl-2">Filtre :</p>
                    <div class="inputFiltre">
                        <select id="selectTech1" class="form-control selectModifeVeille">
                            <option value="0"></option>
                            <option value="entreprise">Sénégal</option>
                            <option value="electricienEntreprise">Canada</option>
                            <option value="independantElectricient">Afrique du sud</option>
                        </select>
                        <select id="selectTech2" class="form-control selectModifeVeille" placeholder="Fabricant">
                            <option value="0"></option>
                            <option value="entreprise">Utilisateur</option>
                        </select>
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form2" class="form-control" />
                            </div>
                            <button type="button" placeholder="Mots-clés ..." class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </div>
                </form>
                <div class="">
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille3">
                            <img src="{{asset('img/ImgProduit.png') }}" alt="">
                            <div class="abosluteBackground"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">Schneider</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                            <p class="numberEtoile">1</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille3">
                            <img src="{{asset('img/ImgProduit.png') }}" alt="">
                            <div class="abosluteBackground"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">Produit Alpha</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                            <p class="numberEtoile">1</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille3">
                            <img src="{{asset('img/ImgProduit.png') }}" alt="">
                            <div class="abosluteBackground"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">Produit Alpha</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                            <p class="numberEtoile">1</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                    <div class="oneProductsVeille2">
                        <div class="imgElementVeille3">
                            <img src="{{asset('img/ImgProduit.png') }}" alt="">
                            <div class="abosluteBackground"></div>
                        </div>
                        <div class="fabriquantProduits2">
                            <p class="titleFab">Produit Alpha</p>
                            <div class="fabImgBlock mb-2">
                                <img class="logoF1" src="{{asset('img/VectorMessage.png') }}" alt="">
                                <p class="numberProduit">17</p>
                            </div>
                            <div class="fabImgBlock">
                                <img class="logoF1" src="{{asset('img/para1.png') }}" alt="">
                                <p>Secteur : Outils de mesures</p>
                            </div>
                        </div>
                        <div class="BlockNumberEtoile">
                            <p class="numberEtoile">1</p>
                            <div class="etoileBlock">
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile spanEtoileExistant"></span>
                                <span class="spanEtoile"></span>
                            </div>
                            <p class="numberNoEtoile">4</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<p class="textCopyright1">© 2021 Test&Rex. Tous droits réservés.</p>


@section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/piexif.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/sortable.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>
        <script src="{{ asset('js/fileInput/fileinput.min.js') }}"></script>
        <script src="{{ asset('js/fileInput/LANG.js') }}"></script>
        <script>
            const swiper = new Swiper(".mySwiper", {
                slidesPerView: 2.3,
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                // Responsive breakpoints
                breakpoints: {

                // when window width is <= 320px
                320: {
                    slidesPerView: 1,
                    spaceBetween: 10
                },
                // when window width is <= 480px
                480: {
                    slidesPerView: 1.3,
                    spaceBetween: 10
                },
                980: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },
                1200: {
                    slidesPerView: 2.3,
                    spaceBetween: 20
                },
                }

            });
        </script>
@endsection
@endsection