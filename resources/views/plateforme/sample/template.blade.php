<html>
<head>
    <title>TEST&REX</title>
    <meta content="TEST&REX, les experts électriciens vous accompagnent" property="og:title">
    <meta content="TEST&REX, les experts électriciens vous accompagnent" property="twitter:title">
    <meta name="description" content="À travers nos services, faites connaître vos solutions, partagez vos retours d'expérience, faites monter en compétence vos équipes grâce à un réseau d'experts électriciens confirmés.">
    
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link rel="stylesheet" href="{{ asset('bootstrap/bootstrap.min.css') }}">
       <!-- Filepond stylesheet -->
    <link href="https://unpkg.com/filepond/dist/filepond.css" rel="stylesheet">
    <link href="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.css" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('swiper/swiper-bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/platforme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fileinput.css') }}">
    <script src="https://kit.fontawesome.com/2def424b14.js" crossorigin="anonymous"></script>
    @yield('css')
</head>
<body>
    @php
    $advertisement =  DB::Table('advertisements')
            ->orderBy('advertisements.created_at', 'desc')
            ->first();
                
    $announce =  DB::Table('announces')
                ->orderBy('announces.created_at', 'desc')
                ->first();

    $events = DB::Table('events')
                ->select('events.*')
                ->where('events.day', '>=', now())
                ->orderBy('events.day')
                ->get();

    $product = DB::Table('products')
                ->select('products.*', 'enterprises.enterprise')
                ->join('enterprises', 'products.user_id', 'enterprises.user_id')
                ->orderBy('products.created_at', 'desc')
                ->first();


    if($user->photo != "Mu-bull-gris.png") 
        $url = Storage::disk('s3')->temporaryUrl('users/'.$user->photo, now()->addMinutes(100));
    else 
        $url = asset('images/uploads') . '/' . 'Mu-bull-gris.png';

    if($announce->media) 
        $urlA = Storage::disk('s3')->temporaryUrl('advertisements/'.$announce->media, now()->addMinutes(100));
    else 
        $urlA = asset('images/uploads') . '/' . 'Mu-bull-gris.png';
    
    if($advertisement->media) 
        $urlV = Storage::disk('s3')->temporaryUrl('advertisements/'.$advertisement->media, now()->addMinutes(100));
    else 
        $urlV = asset('images/uploads') . '/' . 'Mu-bull-gris.png';

    if($product->photo != "Mu-bull-gris.png") 
        $urlPr = Storage::disk('s3')->temporaryUrl('product/'.$product->photo, now()->addMinutes(100));
    else 
        $urlPr = asset('images/uploads') . '/' . 'Mu-bull-gris.png';

    
    $post_user_count = DB::Table('posts')
                    ->where('posts.user_id', Auth::id())
                    ->count();
 
    @endphp
<div class="blockGeneral1">
    <div class="blockDrapreau">
        <div class="pays">
            <img src=" {{asset('img/drapeau/Andorra.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/United-Arab-Emirates.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Afghanistan.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/African-Union.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Barbuda.png')}}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Anguilla.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Albania.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Armenia.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Andorra.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Guinea.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Gambia.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Greenland.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Ghana.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/AMS.png') }}" alt="">
        </div>
        <div class="pays">
            <img src=" {{asset('img/drapeau/Angola.png') }}" alt="">
        </div>

    </div>
    <div class="blockMap">
        <img src=" {{asset('img/Icon-map.png') }}" alt="">
        @if(isset($user))
            <span class="pays">&nbsp;{{$user->country ? $user->country : '🔎' }}</span>
        @endif
    </div>
</div>
<div>
    @yield('content-nav')
</div>
<div class="genaralBlock">
    <div class="container">
        <div class="contentPageGeneral">
            <div class="blockleft">
                <div class="blockVideo">
                    <img src="{{$urlV}}" alt="">
                    <div class="blockAbsolute">
                        @if($advertisement->type == 'video')
                            <video controls autoplay>
                                <source src="{{$urlV}}" type="video/mp4;charset=UTF-8">
                                <source src="{{$urlV}}" type='video/webm; codecs="vp8, vorbis"' />
                                <source src="{{$urlV}}" type='video/ogg; codecs="theora, vorbis"' />
                            </video>
                        @else
                            <div target="_blank" class="elementPlay">
                                <a href="{{$advertisement->link}}" target="_blank" >
                                    <img src="{{asset('img/Icon.png')}}" alt="">
                                </a>
                            </div>
                            <p class="textTestRex">{{$advertisement->title}}</p>
                            <p class="textMonde">{{$advertisement->textual}}</p>
                        @endif
                    </div>
                </div>
                <div class="blockProfilLeft">
                    @if(isset($user))
                        <a href="#" class="btn btnTrais">
                            <img src="{{asset('img/IconTrais.png')}}" alt="">
                        </a>
                        <div class="photoBlockProfil">
                            <img src="{{$url}}" alt="">
                        </div>
                        
                            <p class="nameProfil">
                                {{ isset($user->firstName) ? $user->firstName. " " .$user->lastName : $user->enterprise }}
                            </p>
                        <p class="fonction">{{ isset($user->function) ? $user->function : $user->expertiseDomain }}</p>
                        <hr class="hrModife">
                        <div class="entrepriseBlock">
                            <img class="imgHome" src="{{asset('img/Union.png')}}" alt="">
                            <p class="entrepriseProfil">Entreprise : <span> {{ $user->enterprise }}</span></p>
                        </div>
                    @endif
                    <div class="pubSuivi">
                        <div class="element" style="margin: 0 auto">
                            @if(!isset($post_user_count))
                                <p class="numberPub">0</p>
                            @else
                                <p class="numberPub">{{$post_user_count}}</p>
                            @endif 
                            
                            <p class="pubText">Publications</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentPage">
                @yield('content-body-plateforme')
            </div>
            <div class="blockRight ">
                <div class="annonnce">
                    <img src="{{$urlA}}" alt="">
                    <div class="blockAbsolute">
                        <p class="annonceText">ANNONCE</p>
                        <p class="particpe">{{$announce->title}}</p>
                        <div class="rectangle7">
                            <img src="{{asset('img/Vector7.png')}}" alt="">
                            <a href="{{$announce->link}}" target="_blank" class="obenirText">OBTENIR VOTRE TICKET</a>
                        </div>
                    </div>
                </div>
           
                <div class="blockevenement">
                    <div class="flexElement">
                        <p class="evenementText">ÉVÉNEMENT</p>
                       {{--  <a href="{{route('events.index')}}" class="voirToutBnt">Voir Tout
                            <img src="{{asset('img/IconVoirTout.png')}}" alt="">
                        </a> --}}
                    </div>
                        @for($i=0; $i<2; $i++)
                            @if(isset($events[$i]))
                                @php
                                    $split = explode(" ", $events[$i]->day);
                                    $date = explode("-", $split[0]);
                                    $time = explode(":",$split[1]);
                                    $months = array('01'=>'JAN', '02'=>'FEB', '03'=>'MAR', '04'=>'AVR', '05'=>'MAY', '06'=>'JUN', '07'=>'JUL', '08'=>'AUG', '09'=>'SEP', '10'=>'OCT', '11'=>'NOV', '12'=>'DEC');
                                @endphp
                                <a href="{{$events[$i]->link}}" target="_blank"  style="{{$i == 0 ? 'font-weight:bold;color:#FF871C':'font-weight:bold; color:#42AB44'}}" class="blockDateEvenement {{$i == 0 ? '': 'vertBlock'}}">
                                    <div class="dateBlock">
                                        <p class="dayCalendar">{{ $date[2] }}</p>
                                        <p class="moisCalendar">{{ $months[$date[1]] }}</p>
                                    </div>
                                    <div class="lorenDate">
                                        <p class="textDescription">{{$events[$i]->title}}</p>
                                        <p class="dataTime">{{ $date[2] ." ". $months[$date[1]] ." ". $date[0] ." à ". $time[0] .":". $time[1] }}</p>
                                    </div>
                                </a>
                            @endif
                        @endfor
                </div>
                
                <div class="blockPubMarketPlace">
                    @if(isset($user))
                        <p class="evenementText">MARKETPLACE</p>
                    @endif
                    @if($product)
                    <div class="marketplace">
                        <img src="{{$urlPr}}" alt="">
                        <div class="blockAbsolute">
                            <div class="blockProduit">
                                <p><b>{{$product->name}} </b> <span>- {{$product->enterprise}}</span></p>
                            </div>
                            <div class="blockTilteProduit">
                                <p>{{$product->type}}}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                    <br>
                </div>
            </div>
        </div>

        <!-- début modal Modif profil-->
        <div class="modalModifeProfil">
            <div class="modal-dialog modal-dialog-centered modal-dialogModife" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Mettre à jour votre profil</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modalBbody">
                        {!! Form::model(Auth::user(), ['route' => ['users.update', Auth::id()], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}                            
        
                            <div class="photoProfilContent">
                                <div class="image-input">
                                    <input type="file" name="photo" accept="image/*" id="imageInput">
                                    <label for="imageInput" class="image-button">
                                        @if(isset($user))
                                            <img src="{{$url}}" alt="">
                                        @endif
                                    </label>
                                    <div class="photoBlockProfil photoBlockProfilInput">
                                        <img src="" class="image-preview">
                                    </div>
                                    <span class="change-image">Choissez une autre photo de profil </span>
                                </div>
                            </div>
                            <div class="contentInput">
                                @if(($user->type == 'sample' && $user->profile == 1) || ($user->type == 'fabricant' && $user->profile == 2))
                                    <div class="form-group hide1 colM">
                                        <label class="labelFormation" for="prenomProfil">Prénom</label>
                                        <input type="text" name="firstName" class="form-control inputFormation" id="prenomProfil" value="{{$user->firstName}}">
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelFormation"  for="nomProfil">Nom</label>
                                        <input type="text" name="lastName"  class="form-control inputFormation" id="nomProfil"  value="{{$user->lastName}}">
                                    </div>
                                @elseif(($user->type == 'sample' && $user->profile == 2) || ($user->type == 'fabricant' && $user->profile == 1))
                                    <div class="form-group hide1 colM">
                                        <label class="labelFormation" for="prenomProfil">Entreprise</label>
                                        <input type="text" name="enterprise" class="form-control inputFormation" id="prenomProfil" value="{{$user->enterprise}}">
                                    </div>
                                @endif
                                <div class="form-group hide1 colM">
                                    <label class="labelFormation" for="paysModife">Pays</label>
                                    <select name="country" class="form-control inputFormation inputUtilisateur selectModife" id="paysModife" required>
                                        <option value="{{$user->country}}">{{$user->country}}</option>
                                        <option value="Sénégal">Sénégal</option>
                                        <option value="Angola">Angola</option>
                                        <option value="France">France</option>
                                        <option value="Russie">Russie</option>
                                        <option value="Nigeria">Nigeria</option>
                                        <option value="Cote d'ivoire">Cote d'ivoire</option>
                                    </select>
                                </div>
                                <div class="form-group hide1 colM">
                                    <label class="labelFormation"  for="adress">Adress</label>
                                    <input type="text" name="address" class="form-control inputFormation" id="adress" value="{{$user->address}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Enregistrer" class="btn btnEnregistrer">
                                <button type="button" class="btn btn-dark BtnAnnuler" data-dismiss="modal">Annuler</button>
                            </div>
                        {!! Form::close() !!}
                        </div>

                </div>
            </div>
        </div>
        <!-- début modal Modif profil-->

        <script src="{{ asset('js/jquery.min.js') }}"></script>

        <script src="{{ asset('bootstrap/popper.min.js') }}"></script>
        <script src="{{ asset('bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/platforme.js') }}"></script>

        @yield('scripts')

</div>

</body>
