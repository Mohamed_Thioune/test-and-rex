<div class="table-responsive">
    <table class="table" id="responseComments-table">
        <thead>
            <tr>
                <th>Value</th>
        <th>Fullname</th>
        <th>User Id</th>
        <th>Comment Id</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($responseComments as $responseComments)
            <tr>
                <td>{{ $responseComments->value }}</td>
            <td>{{ $responseComments->fullname }}</td>
            <td>{{ $responseComments->user_id }}</td>
            <td>{{ $responseComments->comment_id }}</td>
                <td>
                    {!! Form::open(['route' => ['responseComments.destroy', $responseComments->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('responseComments.show', [$responseComments->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('responseComments.edit', [$responseComments->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
