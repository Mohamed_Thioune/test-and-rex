<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registration Page</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/AdminLTE.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/css/skins/_all-skins.min.css">

    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/square/_all.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition register-page">
<div class="register-box" >
    <div class="register-logo">
        <a href="{{ url('/home') }}"><b>TEST</b> & REX</a>
    </div>

    <div class="register-box-body" id="starting">

        <p class="login-box-msg">USER | Template</p>

        <form method="post" action="{{ url('/register') }}">
            @csrf
            @include('adminlte-templates::common.errors')

            <div class="form-group has-feedback">
                <label style="color:#2389C5">Vous êtes : </label><br> 
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="type" value="sample" id="type" checked>
                    <label class="form-check-label" for="flexCheckDefault">
                        Utilisateur
                    </label>
                  </div>
                  <div class="form-check ">
                    <input class="form-check-input" type="radio" name="type" value="fabricant" id="type">
                    <label class="form-check-label" for="flexCheckDefault" >
                        Fabricant
                  </div>
            </div>

            <div class="form-group has-feedback{{ $errors->has('phone') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="Phone" id="field1" required>
                <span class="glyphicon glyphicon-phone form-control-feedback"></span>

                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('country') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="country" value="{{ old('country') }}" placeholder="Country" id="field2" required>
                <span class="glyphicon glyphicon-flag form-control-feedback"></span>

                @if ($errors->has('country'))
                    <span class="help-block">
                        <strong>{{ $errors->first('country') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('address') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="address" value="{{ old('address') }}" placeholder="Address" id="field3">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>

                @if ($errors->has('address'))
                    <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" id="field4" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Password" id="field5" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input type="password" name="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}" placeholder="Confirm password" id="field6" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                @if ($errors->has('password_confirmation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>

            <button type="button" id="next" class="btn btn-danger btn-block btn-flat"> Next</button>

    </div>
    <div class="register-box-body" id="profile">
        <p class="login-box-msg">Choose your profil </p>
        <div class="form-check form-check-inline" id="profil_employee">
            <input class="form-check-input" type="radio" name="profile" value="1" id="profil">
            <label class="form-check-label" for="flexCheckDefault">
                Employee
            </label>
          </div>
          <div class="form-check form-check-inline ">
            <input class="form-check-input" type="radio" name="profile" value="2" id="profil" checked>
            <label class="form-check-label" for="flexCheckDefault">
                Enterprise
          </div>
          <div class="form-check form-check-inline " id="profil_mutant">
            <input class="form-check-input" type="radio" name="profile" value="3" id="profil" >
            <label class="form-check-label" for="flexCheckDefault">
                Service marketing
          </div>
          <span id="apply" class="btn btn-danger btn-block btn-flat"> Apply</span>
    </div>
    <div class="register-box-body" id="employee">
        <p class="login-box-msg">SAMPLE - "Employee"</p>

            <div class="form-group has-feedback{{ $errors->has('lastName') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" placeholder="Last Name">
                <span class="glyphicon glyphicon-align-justify form-control-feedback"></span>

                @if ($errors->has('lastName'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lastName') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('firstName') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" placeholder="First Name">
                <span class="glyphicon glyphicon-align-justify form-control-feedback"></span>

                @if ($errors->has('firstName'))
                    <span class="help-block">
                        <strong>{{ $errors->first('firstName') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('enterprise_em') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="enterprise_em" value="{{ old('enterprise_em') }}" placeholder="Name of your enterprise_em">
                <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>

                @if ($errors->has('enterprise_em'))
                    <span class="help-block">
                        <strong>{{ $errors->first('enterprise_em') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('expertiseDomain_em') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="expertiseDomain_em" value="{{ old('expertiseDomain_em') }}" placeholder="Domain of expertise">
                <span class="glyphicon glyphicon-wrench form-control-feedback"></span>

                @if ($errors->has('expertiseDomain_em'))
                    <span class="help-block">
                        <strong>{{ $errors->first('expertiseDomain_em') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('function') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="function" value="{{ old('function') }}" placeholder="Your function ">
                <span class="glyphicon glyphicon-globe form-control-feedback"></span>

                @if ($errors->has('function'))
                    <span class="help-block">
                        <strong>{{ $errors->first('function') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row" id="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>

    </div>
    <div class="register-box-body" id="enterprise">
        <p class="login-box-msg">Service commercial - "Enterprise"</p>

            <div class="form-group has-feedback{{ $errors->has('entreprise_en') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="entreprise_en" value="{{ old('entreprise_en') }}" placeholder="Name of your entreprise">
                <span class="glyphicon glyphicon-align-justify form-control-feedback"></span>

                @if ($errors->has('entreprise_en'))
                    <span class="help-block">
                        <strong>{{ $errors->first('entreprise_en') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('expertiseDomain_en') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="expertiseDomain_en" value="{{ old('expertiseDomain_en') }}" placeholder="Domain of expertise">
                <span class="glyphicon glyphicon-align-justify form-control-feedback"></span>

                @if ($errors->has('expertiseDomain_en'))
                    <span class="help-block">
                        <strong>{{ $errors->first('expertiseDomain_en') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('targetArea') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="targetArea" value="{{ old('targetArea') }}" placeholder="Area Target">
                <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>

                @if ($errors->has('targetArea'))
                    <span class="help-block">
                        <strong>{{ $errors->first('targetArea') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('model') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="model" value="{{ old('model') }}" placeholder="Model Enterprise , Ex : PME.. ">
                <span class="glyphicon glyphicon-wrench form-control-feedback"></span>

                @if ($errors->has('model'))
                    <span class="help-block">
                        <strong>{{ $errors->first('model') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row" id="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>

        </form>

    </div>
    <div class="register-box-body" id="enterprise-employee">
        <p class="login-box-msg">SERVICE MARKETING - "ENT | EMP"</p>
        <form method="post" action="{{ url('/register') }}">
            @csrf
            <input type="hidden" name="type" value="" id="type_field">
            <input type="hidden" name="profile" value="" id="profile_field">
            <input type="hidden" name="phone" value="" id="phone">
            <input type="hidden" name="country" value="" id="country">
            <input type="hidden" name="address" value="" id="address">
            <input type="hidden" name="email" value="" id="email">
            <input type="hidden" name="password" value="" id="password">
            <input type="hidden" name="password_confirmation" value="" id="password_confirmation">

            <div class="form-group has-feedback{{ $errors->has('lastName') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="lastName" value="{{ old('lastName') }}" placeholder="Last Name">
                <span class="glyphicon glyphicon-align-justify form-control-feedback"></span>

                @if ($errors->has('lastName'))
                    <span class="help-block">
                        <strong>{{ $errors->first('lastName') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('firstName') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="firstName" value="{{ old('firstName') }}" placeholder="First Name">
                <span class="glyphicon glyphicon-align-justify form-control-feedback"></span>

                @if ($errors->has('firstName'))
                    <span class="help-block">
                        <strong>{{ $errors->first('firstName') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('enterprise') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="enterprise" value="{{ old('enterprise') }}" placeholder="Name of your enterprise">
                <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>

                @if ($errors->has('enterprise'))
                    <span class="help-block">
                        <strong>{{ $errors->first('enterprise') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('expertiseDomain') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="expertiseDomain" value="{{ old('expertiseDomain') }}" placeholder="Domain of expertise">
                <span class="glyphicon glyphicon-wrench form-control-feedback"></span>

                @if ($errors->has('expertiseDomain'))
                    <span class="help-block">
                        <strong>{{ $errors->first('expertiseDomain') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('function') ? ' has-error' : '' }}">
                <input type="text" class="form-control" name="function" value="{{ old('function') }}" placeholder="Your function ">
                <span class="glyphicon glyphicon-globe form-control-feedback"></span>

                @if ($errors->has('function'))
                    <span class="help-block">
                        <strong>{{ $errors->first('function') }}</strong>
                    </span>
                @endif
            </div>
            

            <div class="form-group has-feedback{{ $errors->has('targetArea') ? ' has-error' : '' }}" id="targetArea">
                <input type="text" class="form-control" name="targetArea" placeholder="Area target">
                <span class="glyphicon glyphicon-screenshot form-control-feedback"></span>

                @if ($errors->has('targetArea'))
                    <span class="help-block">
                        <strong>{{ $errors->first('targetArea') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group has-feedback{{ $errors->has('model') ? ' has-error' : '' }}">
                <input type="text" name="model" class="form-control" placeholder="Choose your model">
                <span class="glyphicon glyphicon-info-sign form-control-feedback"></span>

                @if ($errors->has('model'))
                    <span class="help-block">
                        <strong>{{ $errors->first('model') }}</strong>
                    </span>
                @endif
            </div>

            <div class="row" id="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> I agree to the <a href="#">terms</a>
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
            </div>

        </form>

        <a href="{{ url('/login') }}" class="text-center">I already have a membership</a>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- AdminLTE App -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.3/js/adminlte.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>

<script>
  /*   $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    }); */
    var starting = document.getElementById('starting')
    var profile = document.getElementById('profile');
    var profil_employee = document.getElementById('profil_employee');
    var profil_mutant = document.getElementById('profil_mutant');
    var employee = document.getElementById('employee');
    var row = document.getElementById('row');
    var enterprise = document.getElementById('enterprise');
    var next = document.getElementById('next');
    var apply = document.getElementById('apply');
    var type_area = document.getElementById('targetArea');
    var entreprise_employee = document.getElementById('enterprise-employee');

    // Fields user form service marketing
    var type_field = document.getElementById('type_field');
    var profile_field = document.getElementById('profile_field');
    var phone = document.getElementById('phone');
    var country = document.getElementById('country');
    var address = document.getElementById('address');
    var email = document.getElementById('email');
    var password = document.getElementById('password');
    var password_confirmation = document.getElementById('password_confirmation');
   

    /* Hide all other blocks before the selection of the user */
    row.style.display = "none";
    profile.style.display = "none";
    employee.style.display = "none";
    enterprise.style.display = "none";
    entreprise_employee.style.display = "none";

    next.addEventListener("click", function(e) {
        if(document.querySelector('#type:checked') !== null)
            profile.style.display = "block";
            starting.style.display = "none";
            if(document.querySelector('#type:checked').value === "sample"){
                profil_mutant.style.display = "none";
                type_area.style.display = "none";
            }else
                profil_employee.style.display = "none";

            apply.addEventListener("click", function(e) {
                if(document.querySelector('#profil:checked') !== null){
                    profile.style.display = "none";
                    row.style.display = "block";
                    alert("We got you : " + document.querySelector('#profil:checked').value);
                    if(document.querySelector('#profil:checked').value == 1 ){
                        employee.style.display = "block";
                    }else if(document.querySelector('#profil:checked').value == 2 ){
                        enterprise.style.display = "block";
                    }else if(document.querySelector('#profil:checked').value == 3 ){
                        /* Fill the user form service marketing with user values */
                        type_field.value = document.querySelector('#type:checked').value;
                        profile_field.value = document.querySelector('#profil:checked').value;
                        phone.value = document.getElementById('field1').value;
                        country.value = document.getElementById('field2').value;
                        address.value = document.getElementById('field3').value;
                        email.value = document.getElementById('field4').value;
                        password.value = document.getElementById('field5').value;
                        password_confirmation.value = document.getElementById('field6').value;

                        entreprise_employee.style.display = "block";
                    }
                }
            });

     });
     
</script>
</body>
</html>
