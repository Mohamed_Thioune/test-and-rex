@php 

Auth::logout();

$request->session()->invalidate();

$request->session()->regenerateToken();

return redirect('/');

@endphp
@extends('layouts.linkFront')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.4/simplePagination.css" integrity="sha512-emkhkASXU1wKqnSDVZiYpSKjYEPP8RRG2lgIxDFVI4f/twjijBnDItdaRh7j+VRKFs4YzrAcV17JeFqX+3NVig==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="{{asset('css/login.css')}}">

@endsection

@section('content-body-front')
<div class="contentAccount back1">
    <div class="blockNav">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ">
            <a class="navbar-brand navLogo" href="#">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife " href="{{ route('welcome') }}">Accueil</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link nav-linkModife" href="/qui-sommes-nous">Qui sommes nous ?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/nos-solutions">Nos solutions</a>
                    </li>
                    <li class="nav-item activeNav">
                        <a class="nav-link nav-linkModife"  href="/account">Connexion-Inscription</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/actualite">Actualités</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="#">Contacts</a>
                    </li>
                    <li class="nav-item itemConnecte">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" class="form-control" />
                            </div>
                            <button type="button" class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <div class="headTabAccount">
            <button id="btnTabsConnexion" class="btn activeBtn">Connexion</button>
            <button id="btnTabsInscription" class="btn">Inscription</button>
        </div>
        <div class="contentTabAccount">
            <div class="contentLogin">
                <div class="formblock">
                    <form class="formAccount formUser" method="POST" action="{{ url('/login') }}">
                        @csrf

                       <div class="form-group coll has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                           <label class="labelAccount" for="emailLogin">E-mail</label>
                           <input type="email" class="form-control inputAccount" id="emailLogin" name="email" required>
                           @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                       </div>
                       <div class="form-group coll has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                           <label class="labelAccount"  for="passwoordLogin">Mot de passe</label>
                           <input type="password" class="form-control inputAccount" id="passwoordLogin" name="password" required>
                           @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                           @endif
                       </div>
                       <div class="elementCheck">
                           <div>
                               <label class="radioModife">Se souvenir de moi
                                   <input type="checkbox" id="Sesouvenirdemoi" name="remember">
                                   <span class="checkmark"></span>
                               </label>
                           </div>
                           <a href="{{ url('/password/reset') }}" class="motdepassOublie">Mot de passe oublié ?</a>
                       </div>
                        <div class="elementCont">
                            <div class="groupContinuer">
                                <button type="submit" class="btn btn-connecter" id="Seconnecter">Se connecter</button>
                            </div>
                            <p class="ouContinuer"><span>ou continuer avec</span></p>
                            <div class="btnSociauxblock">
                                <button class="btn btnsociaux">
                                    <img src="{{asset('img/google.png')}}" alt="">
                                </button>
                                <button class="btn btnsociaux">
                                    <img src="{{asset('img/facebook.png')}}" alt="">
                                </button>
                                <button class="btn btnsociaux">
                                    <img src="{{asset('img/linkedin.png')}}" alt="">
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="blockImg">
                    <img src="{{asset('img/ce2.png') }}" alt="">
                </div>
            </div>
            <div class="contentInscription">
                <div class="blockChooseProfil">
                    <p class="votreProfil">Votre profil :</p>
                    <div class="contentChooseProfil">
                        <div class="cardEquipement">
                            <div class="userequipementImg">
                                <img src="{{asset('img/user8.png') }}" alt="">
                            </div>
                            <p class="clientText"> Vous êtes client final de fabricant, UTILISATEUR d’infrastructures des réseaux électriques ou d’instruments de test et mesures électriques</p>
                            <p class="utilisateurEquipementText">UTILISATEUR D’EQUIPEMENTS</p>
                            <button id="contunierEquipement" class="btn btnContinuerIci">Continuer ici
                                <img src=" {{asset('img/chevronPlus.png') }}" alt="">
                            </button>
                        </div>
                        <div class="cardEquipement">
                            <div class="userequipementImg">
                                <img src="{{asset('img/fabriquant8.png') }}" alt="">
                            </div>
                            <p class="clientText">Vous êtes FABRICANT d’infrastructures des réseaux électriques ou d’équipement de test et mesure électriques</p>
                            <p class="utilisateurEquipementText">FABRICANT</p>
                            <button id="btnContinuerFabriquant" class="btn btnContinuerIci">Continuer ici
                                <img src=" {{asset('img/chevronPlus.png') }}" alt="">
                            </button>
                        </div>
                    </div>
<!--                    block des formulaires-->
                    <form class="formeAccount" action="" method="POST">
                        <div class="blockUtilisationEquipement">
                            <div class="blockElementequipement">
                                <button id="btnRetour1" class="btn btnRetour"> <img src="{{asset('img/retourImg.png')}}" alt=""></button>
                                <p class="utilisateurEquipement">UTILISATEURS D’EQUIPEMENTS</p>
                            </div>
                            <div class="cardEquipement2">
                                <div class="blockInput">
                                    <div class="form-group hide1 colL">
                                        <label class="labelAccount" for="profil1">Quel est votre profile ?</label>
                                        <select class="form-control inputAccount inputUtilisateur selectModife" id="profil1" required>
                                            <option></option>
                                            <option>Ingénieur de surface</option>
                                            <option>Ingénieur de surface</option>
                                        </select>
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelAccount"  for="Entreprise1">Entreprise</label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="Entreprise1" required>
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelAccount"  for="activite1">Domaine d’activité</label>
                                        <select class="form-control inputAccount inputUtilisateur selectModife1" id="activite1" required>
                                            <option></option>
                                            <option>Ingénieur de surface</option>
                                            <option>Ingénieur de surface</option>
                                        </select>
                                    </div>
                                    <div class="form-group hide1 colL">
                                        <label class="labelAccount"  for="Pays1">Pays</label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="Pays1" required>
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelAccount"  for="email1">Email</label>
                                        <input type="email" class="form-control inputUtilisateur inputAccount" id="email1" required>
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelAccount"  for="telephone1">Téléphone</label>
                                        <input type="number" class="form-control inputUtilisateur inputAccount" id="telephone1" required>
                                    </div>
                                    <div class="form-group hide1 colL">
                                        <label class="labelAccount"  for="password1">Mot de passe</label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="password1" required>
                                    </div>
                                    <div class="elementCont hide1">
                                        <div class="groupContinuer">
                                            <button type="submit" class="btn btn-connecter" id="continuerEntreprise">S’inscrire<img class="flecheBtn" src="{{asset('img/flecheBnt.png')}}" alt=""></button>
                                        </div>
                                        <p class="ouContinuer"><span>ou continuer avec</span></p>
                                        <div class="btnSociauxblock">
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/google.png')}}" alt="">
                                            </button>
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/facebook.png')}}" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="blockImg2">
                                    <img src="{{asset('img/ce3.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="blockfabriquants">
                            <div class="blockElementequipement">
                                <button id="btnRetour2" class="btn btnRetour"> <img src="{{asset('img/retourImg.png')}}" alt=""></button>
                                <p class="utilisateurEquipement">FABRICANT</p>
                            </div>
                            <div class="cardEquipement2">
                                <div class="blockInput">
                                    <div class="form-group hide1 colL">
                                        <label class="labelAccount" for="profil1">Quel est votre profile ?</label>
                                        <select class="form-control inputAccount inputUtilisateur selectModife" id="profil1" required>
                                            <option></option>
                                            <option>Ingénieur de surface</option>
                                            <option>Ingénieur de surface</option>
                                        </select>
                                    </div>
                                    <div class="form-group hide1 colM">
                                         <label class="labelAccount"  for="Entreprise1">Entreprise</label>
                                        <input type="text" class="form-control inputUtilisateur inputAccount" id="Entreprise1" required>
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelAccount"  for="activite1">Domaine d’activité</label>
                                        <select class="form-control inputAccount inputUtilisateur selectModife1" id="activite1" required>
                                            <option></option>
                                            <option>Ingénieur de surface</option>
                                            <option>Ingénieur de surface</option>
                                        </select>
                                    </div>
                                    <div class="form-group hide1 colL">
                                        <label class="labelAccount"  for="Pays1">Pays</label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="Pays1" required>
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelAccount"  for="email1">Email</label>
                                        <input type="email" class="form-control inputUtilisateur inputAccount" id="email1" required>
                                    </div>
                                    <div class="form-group hide1 colM">
                                        <label class="labelAccount"  for="telephone1">Téléphone</label>
                                        <input type="number" class="form-control inputUtilisateur inputAccount" id="telephone1" required>
                                    </div>
                                    <div class="form-group hide1 colL">
                                        <label class="labelAccount"  for="password1">Mot de passe</label>
                                        <input type="password" class="form-control inputUtilisateur inputAccount" id="password1" required>
                                    </div>
                                    <div class="elementCont hide1">
                                        <div class="groupContinuer">
                                            <button type="submit" class="btn btn-connecter" id="continuerEntreprise">S’inscrire<img class="flecheBtn" src="{{asset('img/flecheBnt.png')}}" alt=""></button>
                                        </div>
                                        <p class="ouContinuer"><span>ou continuer avec</span></p>
                                        <div class="btnSociauxblock">
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/google.png')}}" alt="">
                                            </button>
                                            <button class="btn btnsociaux">
                                                <img src="{{asset('img/facebook.png')}}" alt="">
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="blockImg2">
                                    <img src="{{asset('img/ce3.png') }}" alt="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/simplePagination.js/1.6/jquery.simplePagination.min.js"></script>
<script>


    var items = $(".list-wrapper .list-item");
    var numItems = items.length;
    var perPage = 9;

    items.slice(perPage).hide();

    $('#pagination-container').pagination({
        items: numItems,
        itemsOnPage: perPage,
        prevText: "&laquo;",
        nextText: "&raquo;",
        onPageClick: function (pageNumber) {
            var showFrom = perPage * (pageNumber - 1);
            var showTo = showFrom + perPage;
            items.hide().slice(showFrom, showTo).show();
        }
    });
</script>
@endsection
@endsection