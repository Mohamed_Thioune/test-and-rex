<div class="table-responsive">
    <table class="table" id="advertisements-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Textual</th>
                <th>Link</th>
                <th>Media</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($advertisements as $advertisement)
            <tr>
            <td>{{ $advertisement->title }}</td>
            <td>{{ $advertisement->textual }}</td>
            <td>{{ $advertisement->link }}</td>
            <td>{{ $advertisement->media }}</td>
                <td>
                    {!! Form::open(['route' => ['advertisements.destroy', $advertisement->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('advertisements.show', [$advertisement->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('advertisements.edit', [$advertisement->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
