<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $advertisement->id }}</p>
</div>


<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $advertisement->title }}</p>
</div>

<!-- Textual Field -->
<div class="form-group">
    {!! Form::label('textual', 'Textual:') !!}
    <p>{{ $advertisement->textual }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{{ $advertisement->link }}</p>
</div>

<!-- Media Field -->
<div class="form-group">
    {!! Form::label('media', 'Media:') !!}
    <p>{{ $advertisement->media }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $advertisement->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $advertisement->updated_at }}</p>
</div>

