@extends('layouts.linkFront')
<!--@section('css')
@endsection-->

@section('content-body-front')
<div class="contentWelcome">
    <div class="blockNav">
        <nav class="navbar navbar-expand-lg navbar-light bg-light navbar-login">
            <a class="navbar-brand navLogo" href="/">
                <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item activeNav">
                        <a class="nav-link nav-linkModife " href="/">Accueil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/qui-sommes-nous">Qui sommes nous ?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/nos-solutions">Nos solutions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link nav-linkModife" href="/login"> Rejoindre le Réseau </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/contact">Contact</a>
                        </li>
                    <li class="nav-item itemConnecte">
                        <div class="input-group">
                            <div class="form-outline">
                                <input type="search" id="form1" class="form-control" />
                            </div>
                            <button type="button" class="btn btnModifeSearch">
                                <img src=" {{asset('img/searchIcone.png') }}" alt="">
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    
        <div class="blockSlider">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item itemModife active">
                    <img class="d-block w-100" src="{{asset('img/slide1.png')}}" alt="First slide">
                    <h1 class="textPletforme1">Un réseau professionnel d'experts électriciens. </h1>
                    <img src="{{asset('img/attacheImg.png')}}" class="attacheImg" alt="">
                    <h2 class="textPletforme">Mise en relation de professionnels du secteur de l'électricité.</h2>
                </div>
                <div class="carousel-item itemModife">
                <img class="d-block w-100" src="{{asset('img/slide1.png')}}" alt="First slide">
                    <h1 class="textPletforme1">Interface pour la promotion de l'innovation technologique, l'expérience client et la promotion de marque.</h1>
                    <img src="{{asset('img/attacheImg.png')}}" class="attacheImg" alt="">
                    <h2 class="textPletforme">Partage d'expérience et d'expertise dans le test et les mesures électriques. </h2>
                </div>
                <div class="carousel-item itemModife">
                <img class="d-block w-100" src="{{asset('img/slide1.png')}}" alt="First slide">
                    <h1 class="textPletforme1">Un espace de communication entre experts utilisateurs finaux et experts fabricants. </h1>
                    <img src="{{asset('img/attacheImg.png')}}" class="attacheImg" alt="">
                    <h2 class="textPletforme">Spécification technique, Prescription de marque & Support technique.</h2>
                </div>
            </div>
        </div>
    </div>

    <div class=blockAProposAcceuil>
        <div class="container">
            <div class="twoBlockPropos">
                <div class="mieuxNousConnaitre">
                    <p class="title">A Propos</p>
                    <div class="d-flex align-items-center">
                        <p class="sousTitle">Mieux nous connaître</p>
                        <div class="hrGris">
                            <hr>
                            <hr>
                        </div>
                    </div>
                    <p class="textApropsAcceuil">TEST&REX est une entreprise indépendante qui, à travers ses solutions,
                        met en relation des acteurs du domaine des réseaux électriques
                        pour une prise en compte effective des besoins de chacun d'eux.  <a href="/qui-sommes-nous" class="btn btnSuitePropos" >Lire la suite</a></p>
                </div>
                <div class="mieuxNousConnaitre bleu">
                    <p class="title">TEST&REX-electric </p>
                    <div class="d-flex align-items-center">
                        <p class="sousTitle">Un réseau d'experts électriciens pour :</p>
                        <div class="hrGris">
                            <hr>
                            <hr>
                        </div>
                    </div>
                    <ul>
                        <li class="textApropsAcceuil">Un partage d'expérience sur site, entre experts électriciens</li>
                        <li class="textApropsAcceuil">Un partage de connaissance en électricité et en test et mesures électriques</li>
                        <li class="textApropsAcceuil">Faire connaître votre marque et vos solutions</li>
                        <li class="textApropsAcceuil">Faire monter en compétences, vos électriciens.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="blockFaire">
        <div class="container">
            <div class="groupFaire">
                <h2 class="nos-dommaines">Nos services</h2>
            </div>
            <div class="servicesBlock">
                <div class="blockUtilisateurs">
                    <div class="firstBlockService">
                        <p class="textfirstBlockService">Vous êtes <span>UTILISATEUR,</span> client final de fabricant d'infrastructures des réseaux électriques ou d'instruments de test et mesures électriques</p>
                    </div>
                    <div class="rightTopBlockService cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <P class="textcardfirstBlockService">Formation test et mesures électriques </P>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                    <div class="lefTopBlockService cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <p class="textcardfirstBlockService">Veille technologique et Support Technique Général</p>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                    <div class="rightBottomBlockService cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <p class="textcardfirstBlockService">Expertise-Accompagnement pour montée en compétence technique</p>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                    <div class="lefBottomBlockService cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <p class="textcardfirstBlockService">Assistance Projet d'acquisition de matériel électriques.</p>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                </div>
                <hr class="hrServices">
                <div class="blockFabriquants">
                    <div class="firstBlockService">
                        <p class="textfirstBlockService">Vous êtes <span>FABRICANT</span> d'infrastructures des réseaux électriques ou d'instruments de test et mesures électriques.</p>
                    </div>
                    <div class="rightTopBlockService2 cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <P class="textcardfirstBlockService">Un réseau professionnel pour votre communication média
                        </P>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                    <div class="lefTopBlockService cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <p class="textcardfirstBlockService">Prescription de Marque et
                            Promotion de portefeuille</p>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                    <div class="rightBottomBlockService2 cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <p class="textcardfirstBlockService">Business Development
                            Strategy Consulting
                        </p>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                    <div class="lefBottomBlockService2 cardElementBlockService">
                        <img class="checkV" src="{{asset('img/checkV.png')}}" alt="">
                        <p class="textcardfirstBlockService">Étude de marché</p>
                        <div class="enSavoirPlusBlock">
                            <a href="">En savoir plus</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="blockConfiance">
        <div class="container">
            <h2 class="textConfiance">Ils nous font confiance</h2>
            <h3 class="bigText">Grandes et petites entreprises, nouvelles Start-up et individuels font partis de nos clients</h3>
            <div class="imgPartener">
                <img src="{{asset('img/schneider-electric.png')}}" alt="">
                <img src="{{asset('img/nexans.png')}}" alt="">
                <img src="{{asset('img/abb.png')}}" alt="">
                <img src="{{asset('img/signify.png')}}" alt="">
                <img src="{{asset('img/schneider-electric.png')}}" alt="">
                <img src="{{asset('img/signify.png')}}" alt="">
            </div>
        </div>
    </div>
    <div class="blockPartenaire">
        <div class="container">
            <!--<div class="dflex">
                <p class="textFaire">Feedback du réseau</p>
                <div class="position-relative">
                    <hr class="hrFaire">
                </div>
            </div>-->
            <div class="groupFaire">
                <p class="nos-dommaines">Feedback du réseau</p>
                <p class="experienceCle">L'expérience est la clé !</p>
            </div>
            <div class="blockSlideFaire Confiance2">
                <div class="carousel-wrap">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="cardTemoignage">
                                <p class="textClient">Je ne peux que recommander la plateforme  TEST&REX-electric  à tous les professionnels de l'électricité car le temps de réaction est court, les experts sont  à l'écoute et la solution est vite trouvée.</p>
                                <div class="client">
                                   <div class="group7">
                                       <p class="nameClient">Babacar Ndiaye</p>
                                       <p class="role">Ingénieur électricien </p>
                                   </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="cardTemoignage">
                                <p class="textClient">Merci à toute l'équipe TEST&REX pour l'accompagnement lors de notre processus d'acquisition d'un laboratoire banc d'étalonnage de compteurs. Grâce à votre implication, nous avons pu trouver le meilleur matériel qui correspond à nos besoins techniques et nos limites budgétaires.
                                 </p>
                                <div class="client">
                                   <div class="group7">
                                       <p class="nameClient">Yannick Cissokho</p>
                                       <p class="role">Responsable métrologie </p>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cardTemoignage">
                                <p class="textClient">
                                    Grâce à la plateforme  TEST&REX-electric, je retrouve plus facilement les utilisateurs de mes produits. Je vends plus et  je communique mieux  avec eux. Les retours d'expérience nous ont permis d'améliorer nos produits pour répondre aux mieux au besoin sur le marché.
                                    Une vraie aubaine cette plateforme pour nous les fabricants                                 
                                </p>
                                <div class="client">
                                   <div class="group7">
                                       <p class="nameClient">Yuli Berlian</p>
                                       <p class="role"> Directrice commerciale</p>
                                   </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="cardTemoignage">
                                <p class="textClient">
                                    Pas de soucis pour ma requête  adressée sur la plateforme  TEST&REX-electric sur comment utiliser une valise d'injection pour tester un câble souterrain HTA. La Communication était  facile, une intervention rapide de membres du réseau, et des conseils adéquats sur la qualité du matériel.
                                </p>
                                <div class="client">
                                   <div class="group7">
                                       <p class="nameClient">Moussa Diarrassouba</p>
                                       <p class="role">Technicien exploitation</p>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="blockRejoindre">
        <div class="container">
            <div class="elementContentRejoindre">
                <div class="block1">
                    <div class="dflex">
                        <p class="textFaire2">Nous Rejoindre</p>
                       <div class="position-relative">
                           <hr class="hrFaire2">
                       </div>
                    </div>
                    <h3 class="rejoindreText">Prêt à rejoindre <span>les experts</span>?</h3>
                    <h4 class="ayezText">Ayez les meilleures expériences d'échange avec des experts électriciens.</h4>
                    <a href="/login" class="btn btnRejoindreWelcome">Nous Rejoindre</a>
                </div>
                <div class="block2">
                    <div class="contentImgPattern">
                        <img class="patternOrange" src="{{asset('img/patternOrange.png')}}" alt="">
                        <img class="patterHomme" src="{{asset('img/portraitHomme.png')}}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>
    @include('layouts.footerLayaout')
</div>
</div>

<!--@section('scripts')
@endsection-->
@endsection
