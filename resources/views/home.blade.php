@extends('plateforme.sample.template')
@php  

    if($user->photo != "Mu-bull-gris.png") 
        $url0 = Storage::disk('s3')->temporaryUrl('users/'.$user->photo, now()->addMinutes(100));
    else 
        $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png';

@endphp
<div>
    @section('content-nav')
    <div class="blockNav">
        <div class="">
            <nav class="navbar navbar-expand-lg navbar-light bg-light navBarPlateforme">
                <a class="navbar-brand navLogo" href="#">
                    <img class="logoImg" src="{{asset('img/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                @if(Auth::user()->type == "sample")
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item activeNav">
                                <a class="nav-link nav-linkModife " href="{{ route('home') }}">Accueil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-linkModife" href="/veille">Veille technologique</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-linkModife" href="/support">Support tech.</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link nav-linkModife" href="/formation">Formation</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-linkModife" href="/notation">Noter un Fabricant</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link nav-linkModife" href="/nousContacter">Nous contacter</a>
                            </li>
                            <li class="nav-item itemConnecte">
                                <div class="input-group">
                                    <div class="form-outline">
                                        <input type="search" id="form1" class="form-control" />
                                    </div>
                                    <button type="button" class="btn btnModifeSearch">
                                        <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                    </button>
                                </div>
                            </li>
                            <li class="nav-item">
                                <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                    <img src="{{$url0}}"  alt="">
                                </button>
                                <div class="modalModife">
                                    <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <ul class="navbar-nav d-block">
                                                        <li class="nav-item">
                                                            <a href="{{ url('/logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                                Se déconnecter
                                                            </a>
                                                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                                @csrf
                                                            </form>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                @else
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item ">
                            <a class="nav-link nav-linkModife " href="/homeFabriquant">Accueil</a>
                        </li>
                        <li class="nav-item activeNav">
                            <a class="nav-link nav-linkModife" href="/communication">Communication</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="/publicationOffre">Publier une offre</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Etude de marché</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Mes notations</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-linkModife" href="#">Contacts</a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <a class="nav-link nav-toute" href="#">
                                <img src=" {{asset('img/touteIcone.png') }}" alt="">
                                Toutes
                            </a>
                        </li>
                        <li class="nav-item itemConnecte">
                            <div class="input-group">
                                <div class="form-outline">
                                    <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btnModifeSearch">
                                    <img src=" {{asset('img/searchIcone.png') }}" alt="">
                                </button>
                            </div>
                        </li>
                        <li class="nav-item">
                            <button style="background: white;" type="button" class="circleImgNav" data-toggle="modal" data-target="#exampleModalLong">
                                <img src="{{$url0}}"  alt="">
                            </button>
                            <div class="modalModife">
                                <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <ul class="navbar-nav d-block">
                                                    <li class="nav-item">
                                                        <a href="{{ url('/logout') }}" class="nav-link" 
                                                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                            Se déconnecter
                                                        </a>
                                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                @endif
            </nav>
        </div>
    </div>
    @endsection
</div>

@section('content-body-plateforme')
@include('flash::message')
@include('adminlte-templates::common.errors')
<div class="contentPageInterneOne">
    <div class="blockOnePublication blockC">
        @if(Auth::user()->type == "sample")
        <div class="flexJustifyElement">
            <h1 class="textTitleOne">Partager votre expérience</h1>
        </div>
        <div class="photoAndInputPub">
            <div class="profilPhoto">
                <img src="{{$url0}}" alt="">
            </div>
            <button id="btnInut1" class="btn btnInputPub">Faites nous votre retour d'expérience  ...</button>
        </div>
    @else 
        <div class="flexJustifyElement">
            <h1 class="textTitleOne">Découvrir ce qui se passe dans la communauté </h1>
        </div>
        <div class="swiper-container mySwipe">
            <div class="swiper-wrapper">
                <a href="{{ route('home') }}" class="btn swiper-slide btnVeille {{ !isset($rex) ? 'active' : '' }}">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeToutes.png')}}" alt="">
                    Toutes
                </a>
                <a href="{{ route('home.rex', 1) }}" class="btn swiper-slide btnVeille @if(isset($rex)) {{ $rex == 1 ? 'active' : '' }} @endif">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeTransformateur.png')}}" alt="">
                    Transformateur 
                </a>

                <a href="{{ route('home.rex', 5) }}" class="btn swiper-slide btnVeille @if(isset($rex)){{ $rex == 5 ? 'active' : '' }} @endif">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconedisjoncteur.png')}}" alt="">
                    Disjoncteur
                </a>

                <a href="{{ route('home.rex', 3) }}" class="btn swiper-slide btnVeille @if(isset($rex)){{ $rex == 3 ? 'active' : '' }} @endif">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeprotecteur.png')}}" alt="">
                    Protection et Automatique
                </a>

                <a href="{{ route('home.rex', 4) }}" class="btn swiper-slide btnVeille @if(isset($rex)){{ $rex == 4 ? 'active' : '' }} @endif">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeprotecteur.png')}}" alt="">
                    Moteur
                </a>

                <a href="{{ route('home.rex', 2) }}" class="btn swiper-slide btnVeille @if(isset($rex)) {{ $rex == 2 ? 'active' : '' }} @endif">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconediagnostique.png')}}" alt="">
                    Diagnostic câbles souterrains
                </a>
                
                <a href="{{ route('home.rex', 6) }}" class="btn swiper-slide btnVeille @if(isset($rex)) {{ $rex == 6 ? 'active' : '' }} @endif">
                    <img class="iconeVeilleBnt" src="{{asset('img/iconeAutres.png')}}" alt="">
                    Autres applications
                </a>
            </div>
        </div>
    @endif
    </div>
    <div class="publierRex">
        <div class="homeBlockOneCategory">
            <div class="head">
               <button id="btnRetourPub1" class="btn btnRetour">
                   <img src="{{asset('img/retourOrange.png')}}" alt="">
               </button>
                <p>Publier un REX pour la communauté</p>
            </div>
            <div class="rowCard">
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex" id="transformateur">
                    <p class="titleCard">REX <br>Transformateur</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/01.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex" id="cable">
                    <p class="titleCard">REX <br>Diagnostic cables souterrains</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/02.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex" id="protection">
                    <p class="titleCard">REX <br>Protection & Automatisme</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/03.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex" id="moteur">
                    <p class="titleCard">REX <br>Moteur & Alternateur</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/05.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex" id="disjoncteur">
                    <p class="titleCard">REX <br>Disjoncteur</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/06.png')}}" alt="">
                    </div>
                </div>
                <div data-toggle="modal" data-target="#ModalPublication" class="cardRex" id="autres">
                    <p class="titleCard">REX <br>Autres applications</p>
                    <div class="blockImg">
                        <img src="{{asset('img/iconsCard/07.png')}}" alt="">
                    </div>
                </div>
            </div>

            <!-- Modal -->
            @php
            if($user->photo != "Mu-bull-gris.png") 
                $url0 = Storage::disk('s3')->temporaryUrl('users/'.$user->photo, now()->addMinutes(100));
            else 
                $url0 = asset('images/uploads') . '/' . 'Mu-bull-gris.png';     
            @endphp
            <div class="modal fade" id="ModalPublication" tabindex="-1" role="dialog" aria-labelledby="ModalPublicationTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="elementImgText">
                                <img src="{{asset('img/retourOrange.png')}}" alt="">
                                <h5 class="modal-title" id="exampleModalLongTitle">Faire un REX ...  </h5>
                            </div>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btnPublic dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Tout le monde
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item dropdown-itemModife" href="#">Dropdown link</a>
                                    <a class="dropdown-item dropdown-itemModife" href="#">Dropdown link</a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="photoAndInputPub">
                                <div class="profilPhoto">
                                    <img height="55" width="40" src="{{$url0}}" alt="">
                                </div>
                                <div>
                                    @if($user)
                                        <p class="nameDePublication">
                                            {{ isset($user->firstName) ? $user->firstName. " " .$user->lastName : $user->enterprise }}
                                        </p>
                                    
                                        <p class="localisationPub">{{ $user->country }}</p>
                                    @endif
                                </div>
                            </div>
                            <form class="formPublication" method="POST" action="{{ route('posts.store') }}" enctype="multipart/form-data">
                                <input type="hidden" name="token" id="token" value="{{ Illuminate\Support\Str::random(155) }}">
                                <input type="hidden" name="_token" id="csrf" value="{{ csrf_token() }}">
                                <input type="hidden" name="rex" id="rex" placeholder="Only for test don't worry" value="">
                                <textarea class="modifeTextArea" placeholder="Faites votre retour d'experience ..." name="text" id="" rows="5" required></textarea>
                                <div style="font-weight:bold !important; font-size:10px !important">
                                    <input id="media" name="media" type="file"  class="filepond" multiple >
                                </div>
                                <div class="blockFooterMap">
                                   <div class="elementJustify">
                                        {{-- <div class="btnGroup7">
                                            <button class="btn btnMap">
                                                <img src="{{asset('img/Icon-map.png') }}" alt="">
                                            </button>
                                            <button class="btn btnUser">
                                                <img src="{{asset('img/IconUser.png') }}" alt="">
                                            </button>
                                        </div> --}}
                                   </div>
                                   <input type="submit" value="Publier" class="btn btnPublier2">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@if($posts)
    <div class="blockDesPublication blockC">
        <div class="pubBlok">
            <p class="textPublication">Publications</p>
        </div>
        @php
            $i=-1;
            $c=-1;
            $r=-1;
            $index = 0;
        @endphp

        @while($index < count($posts))
            @foreach($posts[$index] as $post)
                @php
                    $i++;
                    $index++;
                    if($post->photo != "Mu-bull-gris.png") 
                        $urlP = Storage::disk('s3')->temporaryUrl('users/'.$post->photo, now()->addMinutes(100));
                    else 
                        $urlP = asset('images/uploads') . '/' . 'Mu-bull-gris.png';
                @endphp
                    <div class="cardPublication">
                        <div class="headElement">
                            <div class="blockPhoto">
                                <img src="{{$urlP}}" alt="">
                            </div>
                            <div class="blockNameDate">
                                <p class="nameProfilPub">
                                    {{isset($post->firstName) ? $post->firstName . " " . $post->lastName  : $post->enterprise }}
                                </p>
                                <p class="heurePub">
                                    @if(intval(abs(strtotime("now") - strtotime($post->created))/ 86400) == 0)
                                    @if(intval(abs(strtotime("now") - strtotime($post->created))/ 3600) > 0)
                                    <p class="heurePub">{{intval(abs(strtotime("now") - strtotime($post->created))/3600)}} h</p>
                                    @else(intval(abs(strtotime("now") - strtotime($post->created))/ 3600) == 0)
                                        @if(intval(abs(strtotime("now") - strtotime($post->created))/60) == 0 )
                                            <p class="timeResponse" style="color:#DA732E"> A l'instant</p>
                                        @else
                                            <p class="timeResponse"> Il y'a {{intval(abs(strtotime("now") - strtotime($post->created))/60)}} min</p>
                                        @endif
                                    @endif
                                    @elseif(intval(abs(strtotime("now") - strtotime($post->created))/ 86400) == 1)
                                    <p class="heurePub">Hier à {{strftime("%H:%M", strtotime($post->created))}}</p>
                                    @elseif(intval(abs(strtotime("now") - strtotime($post->created))/ 86400) >= 2 && intval(abs(strtotime("now") - strtotime($post->created))/ 86400) <= 27)
                                    <p class="heurePub"> {{intval(abs(strtotime("now") - strtotime($post->created))/ 86400)}} j </p>
                                    @elseif(intval(abs(strtotime("now") - strtotime($post->created))/ 86400) > 30)
                                    <p class="heurePub">{{intval(abs(strtotime("now") - strtotime($post->created))/ 2592000) }} mois</p>
                                    @else(intval(abs(strtotime("now") - strtotime($post->created))/ 2592000) > 12)
                                    <p class="heurePub">{{intval(abs(strtotime("now") - strtotime($post->created))/ 2592000 * 12) }} années </p>
                                    @endif
                                </p>
                            </div>
                            {{-- <div class="blockTroisPoint">
                                <button class="btn bntTroisPoint">
                                    <img src="{{asset('img/IconTrais.png')}}" alt="">
                                </button>
                            </div> --}}
                        </div>
                        @php
                            $count =  DB::Table('users')
                                    ->join('comments', 'users.id', 'comments.user_id')
                                    ->where('comments.post_id', $post->id)
                                    ->orderBy('comments.created_at', 'desc')
                                    ->count();

                            $comments =  DB::Table('users')->select('comments.id','comments.fullname' ,'users.photo' ,'comments.value' ,'comments.created_at')
                                    ->join('comments', 'users.id', 'comments.user_id')
                                    ->where('comments.post_id', $post->id)
                                    ->orderByDesc('comments.created_at')
                                    ->limit(2)
                                    ->get();

                            $interaction =  DB::Table('reactions')->select('reactions.value')
                                    ->where('reactions.post_id', $post->id)
                                    ->where('reactions.user_id', Auth::id())
                                    ->first();

                            $reactions = DB::Table('reactions')
                                ->where('reactions.post_id', $post->id)
                                ->count();

                            $files = DB::Table('files')->select('files.name', 'files.type')
                                ->where('files.post_id', $post->id)
                                ->get();
                            
                         @endphp
                        <div class="contentCard">
                            <p class="textDescriptionPublication">{!!$post->text!!}</p><br>
                                <div class="swiper-container mySwiper2">
                                    <div class="swiper-wrapper">
                                        @if($files)
                                        @if(count($files) == 1)
                                        @php                                         
                                        $url = Storage::disk('s3')->temporaryUrl('posts/'.$files[0]->name, now()->addMinutes(100)); @endphp
                                        @if($files[0]->type == "image")
                                        <div class="swiper-slide">
                                            <img src="{{$url}}" alt="Post image" >
                                        </div>
                                        @elseif($files[0]->type == "video")
                                            <div class="swiper-slide ">
                                                <div class="elementPub">
                                                    <video controls>
                                                        <source src="{{$url}}" type="video/mp4;charset=UTF-8">
                                                        <source src="{{$url}}" type='video/webm; codecs="vp8, vorbis"' />
                                                        <source src="{{$url}}" type='video/ogg; codecs="theora, vorbis"' />
                                                    </video>
                                                </div>
                                            </div>
                                        @endif

                                        @elseif(count($files) > 1)
                                            @foreach ($files as $file)
                                            @php $url = Storage::disk('s3')->temporaryUrl('posts/'.$file->name, now()->addMinutes(100)); @endphp

                                            @if($file->type == "image")
                                                <div class="swiper-slide">
                                                    <img src="{{$url}}" alt="Post image" >
                                                </div>

                                            @elseif($file->type == "video")
                                                <div id="swiper-slide">
                                                    <div class="elementPub">
                                                        <video controls>
                                                            <source src="{{$url}}" type="video/mp4;charset=UTF-8">
                                                            <source src="{{$url}}" type='video/webm; codecs="vp8, vorbis"' />
                                                            <source src="{{$url}}" type='video/ogg; codecs="theora, vorbis"' />
                                                        </video>
                                                    </div>
                                                </div>
                                            @endif
                                            @endforeach
                                        @endif

                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                            @endif 
                            <div class="blockLike">
                                <div class="elementBtn">
                                    <button value="Pertinent {{$i}}" class="btn btnLike  btnPertinent linkPertinent @if(isset($interaction)) {{ $interaction->value == 'Pertinent' ? 'btnActive' : '' }} @endif">
                                        <img src="{{asset('img/like1.png')}}" alt="">
                                        Pertinent
                                    </button>
                                    <button value="Interessant {{$i}}" class="btn btnLike  btnInteressant linkPertinent @if(isset($interaction)) {{ $interaction->value == 'Interessant' ? 'btnActive' : '' }} @endif">
                                        <img src="{{asset('img/like1.png')}}" alt="">
                                        Intéressant
                                    </button>
                                </div>
                        
                            </div>
                           
                            <div class="blockCompterLike">
                                <div class="elementCompterLike">
                                    <img class="aimeIc" src="{{asset('img/aime.png')}}" alt="">
                                    <p class="aimeText reaction{{$i}}">{{ $reactions }} Appréciation</p>
                                </div>
    
                                <div class="elementCompterLike">
                                    <img class="likeInteresseIc" src="{{asset('img/commentaire.png')}}" alt="">
                                    <p class="aimeText commentary{{$i}}">{{$count}} Commentaire</p>
                                </div>
                            </div>
            
                                <div class="blockInputCommentaire">
                                    <div class="photoCommentaire">
                                        <img src="{{$url0}}" alt="">
                                    </div>
                                    <div class="blockCommente">
                                        <input type="hidden" class="iteration{{$i}}" value="{{$i}}" />
                                        <input type="hidden" class="token{{$i}}"  value="{{ csrf_token() }}" />
                                        <input type="hidden" class="fullname{{$i}}"  value="{{ isset($user->firstName) ? $user->firstName . " " . $user->lastName  : $user->enterprise }}" />        

                                        <textarea type="text" rows="1" class="value value{{$i}}" placeholder="Commenter ..."></textarea>
                                        <input type="hidden" class="user{{$i}}" value="{{ Auth::id() }}">
                                        <input type="hidden" class="post{{$i}}" value="{{ $post->id }}">

                                       {{--  <button class="btn btnEmojie">
                                            <img src="{{asset('img/Smile.png')}}" alt="">
                                        </button> --}}
                                    </div>
                                    {{-- <div class="photoInputFile">
                                        <img src="{{asset('img/Icon-photo.png')}}" alt="">
                                    </div> --}}
                                <!--   <div class="photoInputFile">
                                        <input type="file">
                                    </div>-->
                                    <button class="btn btnEnvoyer" value={{$i}}>Publier</button>
                                </div>
                                <div class="commentaireBlock commentaire_auto{{$i}}">
                                    @foreach ($comments as $comment)
                                        @php
                                            $c++;
                                            $response_comments = DB::Table('users')->select('response_comments.fullname' ,'users.photo' ,'response_comments.value', 'response_comments.created_at as created')
                                                ->join('response_comments', 'users.id', 'response_comments.user_id')
                                                ->join('comments', 'comments.id', 'response_comments.comment_id')
                                                ->where('comments.id', $comment->id)
                                                ->orderBy('response_comments.created_at', 'desc')
                                                ->limit(2)
                                                ->get();
                                                if($comment->photo != "Mu-bull-gris.png") 
                                                    $urlC = Storage::disk('s3')->temporaryUrl('users/'.$comment->photo, now()->addMinutes(100));
                                                else 
                                                    $urlC = asset('images/uploads') . '/' . 'Mu-bull-gris.png';
                                        @endphp
                                        <div class='reponseCommentaireBlock'>
                                            <div class='headResponseCommenatire'>
                                                <div class='imgProfilResponse'>
                                                <img src='{{$urlC}}' alt=''>
                                                </div>
                                                <p class='nameResponse'>{{$comment->fullname}} </p>
                                                @if(intval(abs(strtotime("now") - strtotime($comment->created_at))/ 86400) == 0)
                                                @if(intval(abs(strtotime("now") - strtotime($comment->created_at))/ 3600) > 0)
                                                <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($comment->created_at))/3600)}} heure</p>
                                                @else(intval(abs(strtotime("now") - strtotime($comment->created_at))/ 3600) == 0)
                                                    @if(intval(abs(strtotime("now") - strtotime($comment->created_at))/60) == 0 )
                                                        <p class="timeResponse" style="color:#DA732E"> A l'instant</p>
                                                    @else
                                                        <p class="timeResponse"> Il y'a {{intval(abs(strtotime("now") - strtotime($comment->created_at))/60)}} min</p>
                                                    @endif
                                                @endif
                                                @elseif(intval(abs(strtotime("now") - strtotime($comment->created_at))/ 86400) == 1)
                                                <p class="timeResponse">Hier à {{strftime("%H:%M", strtotime($comment->created_at))}}</p>
                                                @elseif(intval(abs(strtotime("now") - strtotime($comment->created_at))/ 86400) >= 2 && intval(abs(strtotime("now") - strtotime($comment->created_at))/ 86400) <= 27)
                                                <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($comment->created_at))/ 86400)}} jour </p>
                                                @elseif(intval(abs(strtotime("now") - strtotime($comment->created_at))/ 86400) > 30)
                                                <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($comment->created_at))/ 2592000) }} mois</p>
                                                @else(intval(abs(strtotime("now") - strtotime($comment->created_at))/ 2592000) > 12)
                                                <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($comment->created_at))/ 2592000 * 12) }} années </p>
                                                @endif
                                            </div>
                                            <p class='responseCommentaireText'>{!!$comment->value!!}</p>
                                            <div class='likeBlockResponse'>
                                                <button class='btn elementLikeResponse'>
                                                    <img src='{{asset('img/aime.png')}}' alt=''>
                                                    <p>0 J'aime</p>
                                                </button>
                                                <button class='btn elementLikeResponse'>
                                                    <img src='{{asset('img/share.png')}}' alt=''>
                                                    <p>Réponses</p>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="sousReponseCommentaireBlock responseSide{{$c}}">

                                            @foreach($response_comments as $response) 
                                                @php
                                                    $r++;
                                                    if($response->photo != "Mu-bull-gris.png") 
                                                        $urlR = Storage::disk('s3')->temporaryUrl('users/'.$response->photo, now()->addMinutes(100));
                                                    else 
                                                        $urlR = asset('images/uploads') . '/' . 'Mu-bull-gris.png';   
                                                @endphp                                           
                                                    <div class="headResponseCommenatire">
                                                        <div class="imgProfilResponse">
                                                            <img src='{{$urlR}}' alt=''>
                                                        </div>
                                                        <p class="nameResponse">{{$response->fullname}}</p>
                                                        @if(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) == 0)
                                                        @if(intval(abs(strtotime("now") - strtotime($response->created))/ 3600) > 0)
                                                        <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($response->created))/3600)}} heure</p>
                                                        @else(intval(abs(strtotime("now") - strtotime($response->created))/ 3600) == 0)
                                                            @if(intval(abs(strtotime("now") - strtotime($response->created))/60) == 0 )
                                                                <p class="timeResponse" style="color:#DA732E"> A l'instant</p>
                                                            @else
                                                                <p class="timeResponse"> Il y'a {{intval(abs(strtotime("now") - strtotime($response->created))/60)}} min</p>
                                                            @endif
                                                        @endif
                                                        @elseif(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) == 1)
                                                        <p class="timeResponse">Hier à {{strftime("%H:%M", strtotime($response->created))}}</p>
                                                        @elseif(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) >= 2 && intval(abs(strtotime("now") - strtotime($response->created))/ 86400) <= 27)
                                                        <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($response->created))/ 86400)}} jour </p>
                                                        @elseif(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) > 30)
                                                        <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($response->created))/ 2592000) }} mois</p>
                                                        @else(intval(abs(strtotime("now") - strtotime($response->created))/ 2592000) > 12)
                                                        <p class="timeResponse">Il y'a {{intval(abs(strtotime("now") - strtotime($response->created))/ 2592000 * 12) }} années </p>
                                                        @endif
                                                    </div>
                                                    <p class="sousResponseCommentaireText">{!!$response->value!!} </p>
                                                    <div class="likeBlockResponse">
                                                        <button class="btn elementLikeResponse">
                                                            <img src="{{asset('img/aime.png')}}" alt="">
                                                            <p>0 J'aime</p>
                                                        </button>
                                                    </div>
                                            @endforeach

                                        </div>
                                        <div class="sousReponseCommentaireBlock">
                                            <div class="blockInputCommentaire">
                                                <div class="photoCommentaire">
                                                    <img src="{{$url0}}" alt="">
                                                </div>
                                                <div class="blockCommente">
                                                    <input type="hidden" class="iteration_response{{$c}}" value="{{$c}}" />
                                                    <input type="hidden" class="token_response{{$c}}"  value="{{ csrf_token() }}" />
                                                    <input type="hidden" class="fullname_response{{$c}}"  value="{{ isset($user->firstName) ? $user->firstName . " " . $user->lastName  : $user->enterprise }}" />        
                                                    <textarea type="text" rows="1" class="value_response value_response{{$c}}" placeholder="Répondre ..."></textarea>
                                                    <input type="hidden" class="user_response{{$c}}" value="{{ Auth::id() }}">
                                                    <input type="hidden" class="comment{{$c}}" value="{{ $comment->id }}">
                
                                                  {{--   <button class="btn btnEmojie">
                                                        <img src="{{asset('img/Smile.png')}}" alt="">
                                                    </button> --}}
                                                </div>
                                               {{--  <div class="photoInputFile">
                                                    <img src="{{asset('img/Icon-photo.png')}}" alt="">
                                                </div> --}}
                                                <!--   <div class="photoInputFile">
                                                    <input type="file">
                                                </div>-->
                                                <button class="btn btnEnvoyer btnReply" value={{$c}}>Publier</button>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                        </div>
                    </div>
            @endforeach
        @endwhile

    </div>
@else 
    <div></div>
@endif  

    @section('scripts')
        <script src="{{ asset('js/swiper-bundle.min.js') }}" type="text/javascript"></script>

        <script src="{{ asset('js/fileInput/piexif.min.js') }}" type="text/javascript"></script>

        <script src="{{ asset('js/fileInput/sortable.min.js') }}" type="text/javascript"></script>

        <script src="{{ asset('js/fileInput/bootstrap.bundle.min.js') }}" crossorigin="anonymous"></script>

        <script src="{{ asset('js/fileInput/fileinput.min.js') }}"></script>

        <script src="{{ asset('js/fileInput/LANG.js') }}"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

        <script>

            var swiper = new Swiper(".mySwipe", {
                slidesPerView: 'auto',
                spaceBetween: 20,
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },

            });
        </script>
        <script>

            var swiper = new Swiper('.mySwiper2', {
                slidesPerView: '1',
                spaceBetween: 30,
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },

            });
        </script>

        <script>
            $(".alert" ).fadeOut(5000);
        </script>

        <script>
           
            var comment = document.querySelectorAll(".btnEnvoyer");
            comment.forEach(item => {
            item.addEventListener('click', event => {
                var i = item.value;
                    var value = $(".value"+i).val();
                    var fullname = $(".fullname"+i).val();
                    var user =  $(".user"+i).val();
                    var post =  $(".post"+i).val();
                    var token = $(".token"+i).val();
                    if(value){
                        $.ajax({
                            url:"{{route('home.post')}}",
                            method:"post",
                            data:{
                                value:value,
                                fullname:fullname,
                                user_id:user,
                                post_id:post,
                                _token:token
                            },
                            dataType:"json",
                            success: function(data){
                                console.log(data);
                                $(".commentaire_auto"+i).html(data.output);
                                $(".commentary"+i).html(data.count);
                                $(".value").val("");
                            }
                        });
                    }
                })
            });

            var reaction = document.querySelectorAll(".btnLike");
            reaction.forEach(item => {
            item.addEventListener('click', event => {
                var values = item.value;
                values = values.split(" ");
                var value = values[0];
                var i = values[1];
                var type = "";
                var user =  $(".user"+i).val();
                var post =  $(".post"+i).val();
                var token = $(".token"+i).val();
                console.log(value);
                console.log(i);
                if(value){
                    if(value == "Pertinent" || value == "Interessant" ) 
                        type = "post";

                    $.ajax({
                        url:"{{route('reactions.store')}}",
                        method:"post",
                        data:{
                            value:value,
                            user_id:user,
                            type:type,
                            post_id:post,
                            _token:token
                        },
                        dataType:"json",
                        success: function(data){
                            console.log(data);
                            $(".reaction"+i).html(data);
                        }
                    });
                }
            })
        });


        var response_comment = document.querySelectorAll(".btnReply");
        response_comment.forEach(item => {
        item.addEventListener('click', event => {
                event.stopPropagation();

                var i = item.value;
                var valor = $(".value_response"+i).val();
                var fullname = $(".fullname_response"+i).val();
                var user =  $(".user_response"+i).val();
                var comment =  $(".comment"+i).val();
                var token = $(".token_response"+i).val();

                console.log(i);
                if(valor){
                    $.ajax({
                        url:"{{route('home.reply')}}",
                        method:"post",
                        data:{
                            value:valor,
                            fullname:fullname,
                            user_id:user,
                            comment_id:comment,
                            _token:token
                        },
                        dataType:"json",
                        success: function(data){
                            console.log(data);
                            $(".responseSide"+i).html(data);
                            $(".value_response").val("");
                        }
                    });
                }
            })
        });


        </script>
        <!-- Load FilePond library -->
        <script src="https://unpkg.com/filepond/dist/filepond.js"></script>
        <script src="https://unpkg.com/filepond-plugin-image-preview/dist/filepond-plugin-image-preview.js"></script>

        <!-- Turn all file input elements into ponds -->
        <script>
            var token = $("#token").val();

            // get a reference to the fieldset element
               const media  = document.querySelector('input[id="media"]');
            // create a FilePond instance at the fieldset element location
               const pond = FilePond.create(media);

               FilePond.setOptions({
                    server: {
                        url: '{{route('post.file')}}',
                        headers: {
                            'X-XSS-Protection': 0,
                            'X-CSRF-TOKEN' : '{{csrf_token()}}',
                            'X-TOKEN' : token,
                        }
                    }
                });
        </script>

    @endsection
@endsection