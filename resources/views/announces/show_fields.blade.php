<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $announce->id }}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $announce->title }}</p>
</div>

<!-- Second Title Field -->
<div class="form-group">
    {!! Form::label('Second title', 'Second title:') !!}
    <p>{{ $announce->title }}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{{ $announce->link }}</p>
</div>

<!-- Media Field -->
<div class="form-group">
    {!! Form::label('media', 'Media:') !!}
    <p>{{ $announce->media }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $announce->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $announce->updated_at }}</p>
</div>

