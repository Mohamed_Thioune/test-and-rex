@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Announce
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($announce, ['route' => ['announces.update', $announce->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('announces.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection