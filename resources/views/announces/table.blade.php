<div class="table-responsive">
    <table class="table" id="announces-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Link</th>
        <th>Media</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($announces as $announce)
            <tr>
                <td>{{ $announce->title }}</td>
            <td>{{ $announce->link }}</td>
            <td>{{ $announce->media }}</td>
                <td>
                    {!! Form::open(['route' => ['announces.destroy', $announce->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('announces.show', [$announce->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('announces.edit', [$announce->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
