<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class CreateUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // 🚦 Not directly use for register because we already got a register controller and the validation rules who corresponds to it
        $rules = [
            'birthday'            => 'required',
            'country'             => 'required|min:2|max:50',
            'profil'              => 'required',
            'email'               => 'required|email|unique:users,email',
            'password'            => 'required|confirmed',
         ];

         return $rules;
    }
}
