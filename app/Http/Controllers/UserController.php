<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\AppBaseController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Flash;
use Auth;
use DB;
use Response;

use App\Mail\Contact;
use App\Mail\ContactCopy;

use App\Mail\LearningEmail;
use App\Mail\LearningEmailCopy;
class UserController extends AppBaseController
{
    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var User $users */
        $users = User::all();

        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create() 
    {
        return view('users.create');
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();

        /** @var User $user */
        $user = User::create($input);

        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        if (empty($user)) {
            Flash::error('Une erreur a été rencontrée , Veuillez réessayer plus tard');
            return redirect(route('home'));
        }

        $input = $request->all();

        if($request->hasfile('photo'))
        {
            $filePath = 'users';
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); 
            $input['name'] = Str::random(20). '.' . $extension;
            $file->storeAs($filePath, $input['name'], 's3');            
        }

        /** User attributes */  
        $input['photo'] = (isset($input['name'])) ? $input['name'] : $user->photo;
        $input['country'] = ($input['country']) ? $input['country'] : $user->country;
        $input['address'] = ($input['address']) ? $input['address'] : $user->address;

        /** @var Employee $user */
        if(isset($input['firstName']) || isset($input['lastName'])){
            DB::table('employees')
            ->where('user_id', Auth::id())
            ->update(['firstName' => $input['firstName'], 'lastName' => $input['lastName']]);
            
            DB::table('comments')
            ->where('user_id', Auth::id())
            ->update(['fullName' => $input['firstName'] .' '. $input['lastName']]);

            DB::table('response_comments')
            ->where('user_id', Auth::id())
            ->update(['fullName' => $input['firstName'] .' '. $input['lastName']]);
        }elseif(isset($input['enterprise'])){
            DB::table('enterprises')
            ->where('user_id', Auth::id())
            ->update(['enterprise' => $input['enterprise']]);

            DB::table('comments')
            ->where('user_id', Auth::id())
            ->update(['fullName' => $input['enterprise']]);

            DB::table('response_comments')
            ->where('user_id', Auth::id())
            ->update(['fullName' => $input['enterprise']]);
        }
          
        $user->fill($input);
        $user->save();

        Flash::success('User updated successfully.');

        return redirect(route('home'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var User $user */
        $user = User::find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $user->delete();

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }

    public function validation($token){
        /** @var User $user */
        $users = User::all();

        $userid = DB::table('validations')
                ->select('validations.user_id')
                ->where('validations.id',$token)
                ->first();

        if(!$userid){
            Flash::error("⛔️ Token inexistant ou a expire");
            return redirect(route('login'));
        }

        /* Check user state */
        $state_user = DB::table('users')
        ->select('users.*')
        ->where('users.id', $userid->user_id)
        ->first();

        if($state_user->state != 0){
            Flash::success("Compte deja confirme, veuillez vous connecter ");
            return redirect(route('login'));
        }

        /* Change user state */
        $query = DB::table('users')
        ->where('users.id', $userid->user_id)
        ->update(['state' => 1]);
        if($query){
            Flash::success('Compte confirmé avec succès ✔️');
            return redirect(route('login'));
        }else{
            Flash::error("⛔️ Token inexistant ou a expire");
            return redirect(route('login'));
        } 

    }

    public function idea(Request $request){
        if(Auth::user()->type == "sample"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            else if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }

        \Mail::to($user->email)->send(new \App\Mail\IdeaEmailCopy($user, $request));
        \Mail::to("mamadou.keita@testandrex.com")->send(new \App\Mail\IdeaEmail($user, $request));

        Flash::success('Contribution envoyée');
        return redirect(route('watch'));
    }

    public function learning(Request $request){
        if(Auth::user()->type == "sample"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            else if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }

        \Mail::to($user->email)->send(new LearningEmailCopy($user, $request));
        \Mail::to("mamadou.keita@testandrex.com")->send(new LearningEmail($user, $request));

        Flash::success('Demande de formation transmise');
        return redirect(route('welcome.formation'));
    }

    public function contact(Request $request){
        if(Auth::user()->type == "sample"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            else if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }

        \Mail::to($user->email)->send(new ContactCopy($user, $request));
        \Mail::to("mamadou.keita@testandrex.com")->send(new Contact($user, $request));

        Flash::success('Message transmis avec succés');
        return redirect(route('welcome.contact'));
    }
}