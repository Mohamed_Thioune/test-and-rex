<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateadvertisementRequest;
use App\Http\Requests\UpdateadvertisementRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\advertisement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

use Flash;
use Response;

class advertisementController extends AppBaseController
{
    /**
     * Display a listing of the advertisement.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var advertisement $advertisements */
        $advertisements = advertisement::all();

        return view('advertisements.index')
            ->with('advertisements', $advertisements);
    }

    /**
     * Show the form for creating a new advertisement.
     *
     * @return Response
     */
    public function create()
    {
        return view('advertisements.create');
    }

    /**
     * Store a newly created advertisement in storage.
     *
     * @param CreateadvertisementRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $file = $request->file('media');

        if($request->hasfile('media'))
        {
            $filePath = 'advertisements';
            $extension = $file->getClientOriginalExtension(); 
            $mimetype = explode('/',\GuzzleHttp\Psr7\mimetype_from_extension($extension))[0];
            $input['type'] = $mimetype;
            $input['media'] = Str::random(50). '.' . $extension;
            $file->storeAs($filePath, $input['media'], 's3');            
        }

        /** @var advertisement $advertisement */
        $advertisement = advertisement::create($input);

        Flash::success('Advertisement saved successfully.');

        return redirect(route('advertisements.index'));
    }

    /**
     * Display the specified advertisement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var advertisement $advertisement */
        $advertisement = advertisement::find($id);

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        return view('advertisements.show')->with('advertisement', $advertisement);
    }

    /**
     * Show the form for editing the specified advertisement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var advertisement $advertisement */
        $advertisement = advertisement::find($id);

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        return view('advertisements.edit')->with('advertisement', $advertisement);
    }

    /**
     * Update the specified advertisement in storage.
     *
     * @param int $id
     * @param UpdateadvertisementRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        /** @var advertisement $advertisement */
        $advertisement = advertisement::find($id);

        $input = $request->all();

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        $file = $request->file('media');

        if($request->hasfile('media'))
        {
            $filePath = 'advertisements';
            $extension = $file->getClientOriginalExtension(); 
            $mimetype = explode('/',\GuzzleHttp\Psr7\mimetype_from_extension($extension))[0];
            $input['type'] = $mimetype;
            $input['media'] = Str::random(20). '.' . $extension;
            $file->storeAs($filePath, $input['media'], 's3');            
        }

        /** Advertisements attributes */  
        $input['media'] = (isset($input['media'])) ? $input['media'] : $advertisement->media;
        $input['title'] = ($input['title']) ? $input['title'] : $advertisement->title;
        $input['textual'] = ($input['textual']) ? $input['textual'] : $advertisement->textual;
        $input['link'] = ($input['link']) ? $input['link'] : $advertisement->link;

        $advertisement->fill($input);
        $advertisement->save();

        Flash::success('Advertisement updated successfully.');

        return redirect(route('advertisements.index'));
    }

    /**
     * Remove the specified advertisement from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var advertisement $advertisement */
        $advertisement = advertisement::find($id);

        if (empty($advertisement)) {
            Flash::error('Advertisement not found');

            return redirect(route('advertisements.index'));
        }

        $advertisement->delete();

        Flash::success('Advertisement deleted successfully.');

        return redirect(route('advertisements.index'));
    }
}
