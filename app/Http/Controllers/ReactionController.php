<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReactionRequest;
use App\Http\Requests\UpdateReactionRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Reaction;
use Illuminate\Http\Request;
use Flash;
use Response;
Use DB;

class ReactionController extends AppBaseController
{
    /**
     * Display a listing of the Reaction.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Reaction $reactions */
        $reactions = Reaction::all();

        return view('reactions.index')
            ->with('reactions', $reactions);
    }

    /**
     * Show the form for creating a new Reaction.
     *
     * @return Response
     */
    public function create()
    {
        return view('reactions.create');
    }

    /**
     * Store a newly created Reaction in storage.
     *
     * @param CreateReactionRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $state =  DB::Table('reactions')
        ->select('reactions.*')
        ->where('reactions.user_id', $request->user_id)
        ->where('reactions.post_id', $request->post_id)
        ->first();

        if($state){
            DB::table('reactions')->where('id', $state->id)->delete();
            if($state->value != $request->value)
                Reaction::create($input);
               
            $reactions =  DB::Table('reactions')
            ->where('reactions.post_id', $request->post_id)
            ->count();
            $output = $reactions . " J'aime";
            
            return json_encode($output); 
        }

        /** @var Reaction $reaction */
        Reaction::create($input);

        $reactions =  DB::Table('reactions')
            ->where('reactions.post_id', $request->post_id)
            ->count();

        $output = $reactions . " J'aime";

        return json_encode($output);  
    }

    /**
     * Display the specified Reaction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Reaction $reaction */
        $reaction = Reaction::find($id);

        if (empty($reaction)) {
            Flash::error('Reaction not found');

            return redirect(route('reactions.index'));
        }

        return view('reactions.show')->with('reaction', $reaction);
    }

    /**
     * Show the form for editing the specified Reaction.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Reaction $reaction */
        $reaction = Reaction::find($id);

        if (empty($reaction)) {
            Flash::error('Reaction not found');

            return redirect(route('reactions.index'));
        }

        return view('reactions.edit')->with('reaction', $reaction);
    }

    /**
     * Update the specified Reaction in storage.
     *
     * @param int $id
     * @param UpdateReactionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReactionRequest $request)
    {
        /** @var Reaction $reaction */
        $reaction = Reaction::find($id);

        if (empty($reaction)) {
            Flash::error('Reaction not found');

            return redirect(route('reactions.index'));
        }

        $reaction->fill($request->all());
        $reaction->save();

        Flash::success('Reaction updated successfully.');

        return redirect(route('reactions.index'));
    }

    /**
     * Remove the specified Reaction from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Reaction $reaction */
        $reaction = Reaction::find($id);

        if (empty($reaction)) {
            Flash::error('Reaction not found');

            return redirect(route('reactions.index'));
        }

        $reaction->delete();

        Flash::success('Reaction deleted successfully.');

        return redirect(route('reactions.index'));
    }
}
