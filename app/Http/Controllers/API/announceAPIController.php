<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateannounceAPIRequest;
use App\Http\Requests\API\UpdateannounceAPIRequest;
use App\Models\announce;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class announceController
 * @package App\Http\Controllers\API
 */

class announceAPIController extends AppBaseController
{
    /**
     * Display a listing of the announce.
     * GET|HEAD /announces
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = announce::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $announces = $query->get();

        return $this->sendResponse($announces->toArray(), 'Announces retrieved successfully');
    }

    /**
     * Store a newly created announce in storage.
     * POST /announces
     *
     * @param CreateannounceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateannounceAPIRequest $request)
    {
        $input = $request->all();

        /** @var announce $announce */
        $announce = announce::create($input);

        return $this->sendResponse($announce->toArray(), 'Announce saved successfully');
    }

    /**
     * Display the specified announce.
     * GET|HEAD /announces/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var announce $announce */
        $announce = announce::find($id);

        if (empty($announce)) {
            return $this->sendError('Announce not found');
        }

        return $this->sendResponse($announce->toArray(), 'Announce retrieved successfully');
    }

    /**
     * Update the specified announce in storage.
     * PUT/PATCH /announces/{id}
     *
     * @param  int $id
     * @param UpdateannounceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateannounceAPIRequest $request)
    {
        /** @var announce $announce */
        $announce = announce::find($id);

        if (empty($announce)) {
            return $this->sendError('Announce not found');
        }

        $announce->fill($request->all());
        $announce->save();

        return $this->sendResponse($announce->toArray(), 'announce updated successfully');
    }

    /**
     * Remove the specified announce from storage.
     * DELETE /announces/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var announce $announce */
        $announce = announce::find($id);

        if (empty($announce)) {
            return $this->sendError('Announce not found');
        }

        $announce->delete();

        return $this->sendSuccess('Announce deleted successfully');
    }
}
