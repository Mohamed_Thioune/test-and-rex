<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateReactionAPIRequest;
use App\Http\Requests\API\UpdateReactionAPIRequest;
use App\Models\Reaction;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ReactionController
 * @package App\Http\Controllers\API
 */

class ReactionAPIController extends AppBaseController
{
    /**
     * Display a listing of the Reaction.
     * GET|HEAD /reactions
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Reaction::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $reactions = $query->get();

        return $this->sendResponse($reactions->toArray(), 'Reactions retrieved successfully');
    }

    /**
     * Store a newly created Reaction in storage.
     * POST /reactions
     *
     * @param CreateReactionAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateReactionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Reaction $reaction */
        $reaction = Reaction::create($input);

        return $this->sendResponse($reaction->toArray(), 'Reaction saved successfully');
    }

    /**
     * Display the specified Reaction.
     * GET|HEAD /reactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Reaction $reaction */
        $reaction = Reaction::find($id);

        if (empty($reaction)) {
            return $this->sendError('Reaction not found');
        }

        return $this->sendResponse($reaction->toArray(), 'Reaction retrieved successfully');
    }

    /**
     * Update the specified Reaction in storage.
     * PUT/PATCH /reactions/{id}
     *
     * @param  int $id
     * @param UpdateReactionAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateReactionAPIRequest $request)
    {
        /** @var Reaction $reaction */
        $reaction = Reaction::find($id);

        if (empty($reaction)) {
            return $this->sendError('Reaction not found');
        }

        $reaction->fill($request->all());
        $reaction->save();

        return $this->sendResponse($reaction->toArray(), 'Reaction updated successfully');
    }

    /**
     * Remove the specified Reaction from storage.
     * DELETE /reactions/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Reaction $reaction */
        $reaction = Reaction::find($id);

        if (empty($reaction)) {
            return $this->sendError('Reaction not found');
        }

        $reaction->delete();

        return $this->sendSuccess('Reaction deleted successfully');
    }
}
