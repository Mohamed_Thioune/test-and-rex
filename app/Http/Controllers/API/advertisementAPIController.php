<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateadvertisementAPIRequest;
use App\Http\Requests\API\UpdateadvertisementAPIRequest;
use App\Models\advertisement;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class advertisementController
 * @package App\Http\Controllers\API
 */

class advertisementAPIController extends AppBaseController
{
    /**
     * Display a listing of the advertisement.
     * GET|HEAD /advertisements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = advertisement::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $advertisements = $query->get();

        return $this->sendResponse($advertisements->toArray(), 'Advertisements retrieved successfully');
    }

    /**
     * Store a newly created advertisement in storage.
     * POST /advertisements
     *
     * @param CreateadvertisementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateadvertisementAPIRequest $request)
    {
        $input = $request->all();

        /** @var advertisement $advertisement */
        $advertisement = advertisement::create($input);

        return $this->sendResponse($advertisement->toArray(), 'Advertisement saved successfully');
    }

    /**
     * Display the specified advertisement.
     * GET|HEAD /advertisements/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var advertisement $advertisement */
        $advertisement = advertisement::find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        return $this->sendResponse($advertisement->toArray(), 'Advertisement retrieved successfully');
    }

    /**
     * Update the specified advertisement in storage.
     * PUT/PATCH /advertisements/{id}
     *
     * @param  int $id
     * @param UpdateadvertisementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateadvertisementAPIRequest $request)
    {
        /** @var advertisement $advertisement */
        $advertisement = advertisement::find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        $advertisement->fill($request->all());
        $advertisement->save();

        return $this->sendResponse($advertisement->toArray(), 'advertisement updated successfully');
    }

    /**
     * Remove the specified advertisement from storage.
     * DELETE /advertisements/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var advertisement $advertisement */
        $advertisement = advertisement::find($id);

        if (empty($advertisement)) {
            return $this->sendError('Advertisement not found');
        }

        $advertisement->delete();

        return $this->sendSuccess('Advertisement deleted successfully');
    }
}
