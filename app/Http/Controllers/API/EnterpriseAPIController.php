<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEnterpriseAPIRequest;
use App\Http\Requests\API\UpdateEnterpriseAPIRequest;
use App\Models\Enterprise;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EnterpriseController
 * @package App\Http\Controllers\API
 */

class EnterpriseAPIController extends AppBaseController
{
    /**
     * Display a listing of the Enterprise.
     * GET|HEAD /enterprises
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $query = Enterprise::query();

        if ($request->get('skip')) {
            $query->skip($request->get('skip'));
        }
        if ($request->get('limit')) {
            $query->limit($request->get('limit'));
        }

        $enterprises = $query->get();

        return $this->sendResponse($enterprises->toArray(), 'Enterprises retrieved successfully');
    }

    /**
     * Store a newly created Enterprise in storage.
     * POST /enterprises
     *
     * @param CreateEnterpriseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEnterpriseAPIRequest $request)
    {
        $input = $request->all();

        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::create($input);

        return $this->sendResponse($enterprise->toArray(), 'Enterprise saved successfully');
    }

    /**
     * Display the specified Enterprise.
     * GET|HEAD /enterprises/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::find($id);

        if (empty($enterprise)) {
            return $this->sendError('Enterprise not found');
        }

        return $this->sendResponse($enterprise->toArray(), 'Enterprise retrieved successfully');
    }

    /**
     * Update the specified Enterprise in storage.
     * PUT/PATCH /enterprises/{id}
     *
     * @param  int $id
     * @param UpdateEnterpriseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnterpriseAPIRequest $request)
    {
        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::find($id);

        if (empty($enterprise)) {
            return $this->sendError('Enterprise not found');
        }

        $enterprise->fill($request->all());
        $enterprise->save();

        return $this->sendResponse($enterprise->toArray(), 'Enterprise updated successfully');
    }

    /**
     * Remove the specified Enterprise from storage.
     * DELETE /enterprises/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::find($id);

        if (empty($enterprise)) {
            return $this->sendError('Enterprise not found');
        }

        $enterprise->delete();

        return $this->sendSuccess('Enterprise deleted successfully');
    }
}
