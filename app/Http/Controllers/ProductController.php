<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Flash;
use Response;
use Auth;


class ProductController extends AppBaseController
{
    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Product $products */
        $products = Product::all();

        return view('products.index')
            ->with('products', $products);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        $user = Auth::user();
        
        $this->check_valid($user);

        return view('products.create');
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();

        $user = Auth::user();
 
        $this->check_valid($user);

        $file = $request->file('photo');

        if(Auth::user()->type == "sample" && Auth::user()->state != 2  && Auth::user()->profile == 1 ){
            Flash::error('Your are not allowed to publish as far, Look at the permissions you have according to your profile');
            return redirect(route('home'));
        }

        if($request->hasfile('photo'))
        {
            $filePath = 'product';
            $extension = $file->getClientOriginalExtension(); 
            $mimetype = explode('/',\GuzzleHttp\Psr7\mimetype_from_extension($extension))[0];
            $input['name'] = Str::random(50). '.' . $extension;
            $file->storeAs($filePath, $input['name'], 's3');  
        }          

        $input['user_id'] = Auth::id();
        /** @var Product $product */
        $product = Product::create($input);

        Flash::success('Product saved successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.show')->with('product', $product);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit')->with('product', $product);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        if(Auth::user()->type == "sample" && Auth::user()->state != 2){
            Flash::error('Your are not allowed to publish as far, Look at the permissions you have according to your profile');
            return redirect(route('home'));
        }

        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $input = $request->all();
        $file = $request->file('photo');

        if($request->hasfile('photo'))
        {
            $filePath = 'product';
            $file = $request->file('photo');
            $extension = $file->getClientOriginalExtension(); 
            $input['photo'] = Str::random(20). '.' . $extension;
            $file->storeAs($filePath, $input['photo'], 's3');            
        }

        /** Products attributes */  
        $input['photo'] = (isset($input['photo'])) ? $input['photo'] : $product->name;
        $input['name'] = ($input['name']) ? $input['name'] : $product->name;
        $input['description'] = ($input['description']) ? $input['description'] : $product->description;
        $input['type'] = ($input['type']) ? $input['type'] : $product->type;
        $input['price'] = ($input['price']) ? $input['price'] : $product->price;

        $input['user_id'] = Auth::id();

        $product->fill($input);
        $product->save();

        Flash::success('Product updated successfully.');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Product $product */
        $product = Product::find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $product->delete();

        Flash::success('Product deleted successfully.');

        return redirect(route('products.index'));
    }

    public function check_valid($user){

        if($user->state != 2 )
        {
            Flash::error('Your are not allowed to publish as far, Look at the permissions you have according to your profile');
            return redirect(route('home'));
        }
        
    }
}
