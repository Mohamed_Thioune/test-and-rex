<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateCommentRequest;
use App\Models\Comment;
use App\Models\Product;
use App\Models\Response_comment;

use Auth;
use DB;
use Flash;

use Illuminate\Support\Facades\Storage;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()->isAdmin)
            return(redirect('users'));

        if(Auth::user()->type == "sample"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }else if(Auth::user()->type == "fabricant"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }

        $posts = array();

        $postit = DB::Table('posts')
        ->select('posts.id', 'users.type', 'users.profile')
        ->join('users', 'users.id', 'posts.user_id')
        ->orderBy('posts.created_at', 'desc')
        ->get();

        foreach($postit as $post){
            if($post->type == "sample" && $post->profile == 1){
                $posted = DB::Table('posts')
                ->select('posts.id', 'posts.text', 'posts.created_at as created', 'employees.firstName', 'employees.lastName', 'users.photo', 'users.id as user_id')
                ->join('users', 'users.id', 'posts.user_id')
                ->join('employees', 'employees.user_id', 'posts.user_id')
                ->where('posts.id', $post->id)
                ->get();

                array_push($posts,$posted);
            }
            else{
                $posted = DB::Table('posts')
                    ->select('posts.id','posts.text', 'posts.created_at as created', 'enterprises.enterprise', 'users.photo', 'users.id as user_id')
                    ->join('users', 'users.id', 'posts.user_id')
                    ->join('enterprises', 'enterprises.user_id', 'posts.user_id')
                    ->where('posts.id', $post->id)
                    ->get();

                array_push($posts,$posted);
            }
        }
    
        return view('home', compact('posts', 'user'));
    }


    public function rex($rex)
    {
        if(Auth::user()->type == "sample"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }else if(Auth::user()->type == "fabricant"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }

        $posts = array();

        $postit = DB::Table('posts')
        ->select('posts.id', 'users.type', 'users.profile')
        ->join('users', 'users.id', 'posts.user_id')
        ->where('posts.rex', $rex)
        ->orderBy('posts.created_at', 'desc')
        ->get();

        foreach($postit as $post){
            if($post->type == "sample" && $post->profile == 1){
                $posted = DB::Table('posts')
                ->select('posts.id', 'posts.text', 'posts.created_at as created', 'employees.firstName', 'employees.lastName', 'users.photo', 'users.id as user_id')
                ->join('users', 'users.id', 'posts.user_id')
                ->join('employees', 'employees.user_id', 'posts.user_id')
                ->where('posts.id', $post->id)
                ->get();

                array_push($posts,$posted);
            }
            else{
                $posted = DB::Table('posts')
                    ->select('posts.id','posts.text','posts.created_at as created', 'enterprises.enterprise', 'users.photo', 'users.id as user_id')
                    ->join('users', 'users.id', 'posts.user_id')
                    ->join('enterprises', 'enterprises.user_id', 'posts.user_id')
                    ->where('posts.id', $post->id)
                    ->get();

                array_push($posts,$posted);
            }
        }
    
        return view('home', compact('posts', 'user', 'rex'));
    }


    public function store_comment(Request $request)
    {
        $input = $request->all();

        $input['value'] = nl2br($request->value);

        /** @var Comment $comment */
        $comment = Comment::create($input);

        $count =  DB::Table('users')->select('comments.id' ,'comments.fullname' ,'users.photo' ,'comments.value', 'comments.created_at as created')
        ->join('comments', 'users.id', 'comments.user_id')
        ->where('comments.post_id', $request->post_id)
        ->orderBy('comments.created_at', 'desc')
        ->count();

        $comments =  DB::Table('users')->select('comments.id' ,'comments.fullname' ,'users.photo' ,'comments.value', 'comments.created_at as created')
        ->join('comments', 'users.id', 'comments.user_id')
        ->where('comments.post_id', $request->post_id)
        ->orderBy('comments.created_at', 'desc')
        ->limit(2)
        ->get();


        $output = "";

        foreach($comments as $comment){
            $time = "";

            if($comment->photo != "Mu-bull-gris.png") 
                $urlC = Storage::disk('s3')->temporaryUrl('users/'.$comment->photo, now()->addMinutes(100));
            else 
                $urlC = asset('images/uploads') . '/' . 'Mu-bull-gris.png';   

            if(intval(abs(strtotime("now") - strtotime($comment->created))/ 86400) == 0){
                if(intval(abs(strtotime("now") - strtotime($comment->created))/ 3600) > 0)
                    $time = "Il y'a " . intval(abs(strtotime("now") - strtotime($comment->created))/3600) . " heure";
                else if(intval(abs(strtotime("now") - strtotime($comment->created))/ 3600) == 0){
                    if(intval(abs(strtotime("now") - strtotime($comment->created))/60) == 0 )
                        $time =  "A l'instant";
                    else
                        $time = "Il y'a " . intval(abs(strtotime("now") - strtotime($comment->created))/60) . " min";
                }
            }else if(intval(abs(strtotime("now") - strtotime($comment->created))/ 86400) == 1)
                $time = "Hier à" . strftime("%H:%M", strtotime($comment->created));
            else if(intval(abs(strtotime("now") - strtotime($comment->created))/ 86400) >= 2 && intval(abs(strtotime("now") - strtotime($comment->created))/ 86400) <= 27)
                $time =  "Il y'a " . intval(abs(strtotime("now") - strtotime($comment->created))/ 86400) . " jour";
            else if(intval(abs(strtotime("now") - strtotime($comment->created))/ 86400) > 30)
                $time = "Il y'a " . intval(abs(strtotime("now") - strtotime($comment->created))/ 2592000) . " mois";
            else if (intval(abs(strtotime("now") - strtotime($comment->created))/ 2592000) > 12)
                $time = "Il y'a " . intval(abs(strtotime("now") - strtotime($comment->created))/ 2592000 * 12) . " an";
            
                $style = $time == "A l'instant" ? "style=color:#DA732E" : "";

                $output = $output . 
                    "<div class='reponseCommentaireBlock'>
                        <div class='headResponseCommenatire'>
                            <div class='imgProfilResponse'>
                                <img src='" . $urlC . "' alt=''>
                            </div>
                            <p class='nameResponse'>". $comment->fullname ."</p>
                            <p class='timeResponse' " . $style. " >" . $time ."</p>
                        </div>
                        <p class='responseCommentaireText'>". $comment->value  ."</p>
                        <div class='likeBlockResponse'>
                            <button class='btn elementLikeResponse'>
                                <img src='" . asset('img/aime.png') ."' alt=''>
                                <p>0 J'aime</p>
                            </button>
                            <button class='btn elementLikeResponse'>
                                <img src='". asset('img/share.png') ."' alt=''>
                                <p>Répondre</p>
                            </button>
                        </div>
                    </div>                    
                </div>
                    ";
        }

        $data = array('output' => $output, 'count' => $count . " Commentaire");
        return json_encode($data);   
    }

    public function reply_comment(Request $request)
    {
        $input = $request->all();

        $input['value'] = nl2br($request->value);

        /** @var Response_Comment $comment */
        $response = Response_comment::create($input);

        
        $response_comments = DB::Table('users')->select('response_comments.fullname' ,'users.photo' ,'response_comments.value', 'response_comments.created_at as created')
        ->join('response_comments', 'users.id', 'response_comments.user_id')
        ->join('comments', 'comments.id', 'response_comments.comment_id')
        ->where('comments.id', $response->comment_id)
        ->orderBy('response_comments.created_at', 'desc')
        ->limit(2)
        ->get();

        $output = "";

        foreach ($response_comments as $response){

            $time_response = "";

            $photo = $response->photo; 
            $time = now()->addMinutes(100);
            $stock = Storage::disk('s3')->temporaryUrl('users/'.$response->photo, now()->addMinutes(100));
            if($response->photo != "Mu-bull-gris.png") 
                $urlR = $stock;
            else 
                $urlR = asset('images/uploads') . '/' . 'Mu-bull-gris.png';   

            if(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) == 0){
                if(intval(abs(strtotime("now") - strtotime($response->created))/ 3600) > 0)
                    $time_response = "Il y'a " . intval(abs(strtotime("now") - strtotime($response->created))/3600) . " heure";
                else if(intval(abs(strtotime("now") - strtotime($response->created))/ 3600) == 0){
                    if(intval(abs(strtotime("now") - strtotime($response->created))/60) == 0 )
                        $time_response =  "A l'instant";
                    else
                        $time_response = "Il y'a " . intval(abs(strtotime("now") - strtotime($response->created))/60) . " min";
                }
            }else if(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) == 1)
                $time_response = "Hier à" . strftime("%H:%M", strtotime($response->created));
            else if(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) >= 2 && intval(abs(strtotime("now") - strtotime($response->created))/ 86400) <= 27)
                $time_response =  "Il y'a " . intval(abs(strtotime("now") - strtotime($response->created))/ 86400) . " jour";
            else if(intval(abs(strtotime("now") - strtotime($response->created))/ 86400) > 30)
                $time_response = "Il y'a " . intval(abs(strtotime("now") - strtotime($response->created))/ 2592000) . " mois";
            else if (intval(abs(strtotime("now") - strtotime($response->created))/ 2592000) > 12)
                $time_response = "Il y'a " . intval(abs(strtotime("now") - strtotime($response->created))/ 2592000 * 12) . " an";

            $style = ($time_response == "A l'instant") ? "style=color:#DA732E" : "";

            $output = $output . 
                "
                    <div class='headResponseCommenatire'>
                        <div class='imgProfilResponse'>
                        <img src=" . $urlR . " alt=''>
                        </div>
                        <p class='nameResponse'>". $response->fullname ."</p>
                        <p class='timeResponse' " . $style. ">" . $time_response . "</p>
                    </div>
                    <p class='sousResponseCommentaireText'>" . $response->value . " </p>
                    <div class='likeBlockResponse'>
                        <button class='btn elementLikeResponse'>
                            <img src='" . asset('img/aime.png') . "' alt=''>
                            <p>0 J'aime</p>
                        </button>
                    </div>
                ";
        }

        return json_encode($output);   
    }




    public function watch(){
    
        if(Auth::user()->type == "sample"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }else if(Auth::user()->type == "fabricant"){
            if(Auth::user()->profile == 1)
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
            if(Auth::user()->profile == 2)
                $user = DB::Table('users')->select('users.*', 'enterprises.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
        }
        
        $products = DB::Table('products')
                ->select('products.*')
                ->orderBy('products.created_at','desc')
                ->get();

        return view('plateforme/veilleTechno', compact('products', 'user'));
    }


    /**
     * My apologies for the person who gonna read this code 😅 
     * i just created a function to retrieve a exception post_max 
    */
    public function exception_large()
    {
        Flash::error('Le fichier uploadé est volumineux, Taille autorisé < 60 Mo');
        return redirect('home');
    }

}
