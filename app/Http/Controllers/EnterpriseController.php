<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEnterpriseRequest;
use App\Http\Requests\UpdateEnterpriseRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Enterprise;
use Illuminate\Http\Request;
use Flash;
use Response;

class EnterpriseController extends AppBaseController
{
    /**
     * Display a listing of the Enterprise.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Enterprise $enterprises */
        $enterprises = Enterprise::all();

        return view('enterprises.index')
            ->with('enterprises', $enterprises);
    }

    /**
     * Show the form for creating a new Enterprise.
     *
     * @return Response
     */
    public function create()
    {
        return view('enterprises.create');
    }

    /**
     * Store a newly created Enterprise in storage.
     *
     * @param CreateEnterpriseRequest $request
     *
     * @return Response
     */
    public function store(CreateEnterpriseRequest $request)
    {
        $input = $request->all();

        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::create($input);

        Flash::success('Enterprise saved successfully.');

        return redirect(route('enterprises.index'));
    }

    /**
     * Display the specified Enterprise.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::find($id);

        if (empty($enterprise)) {
            Flash::error('Enterprise not found');

            return redirect(route('enterprises.index'));
        }

        return view('enterprises.show')->with('enterprise', $enterprise);
    }

    /**
     * Show the form for editing the specified Enterprise.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::find($id);

        if (empty($enterprise)) {
            Flash::error('Enterprise not found');

            return redirect(route('enterprises.index'));
        }

        return view('enterprises.edit')->with('enterprise', $enterprise);
    }

    /**
     * Update the specified Enterprise in storage.
     *
     * @param int $id
     * @param UpdateEnterpriseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEnterpriseRequest $request)
    {
        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::find($id);

        if (empty($enterprise)) {
            Flash::error('Enterprise not found');

            return redirect(route('enterprises.index'));
        }

        $enterprise->fill($request->all());
        $enterprise->save();

        Flash::success('Enterprise updated successfully.');

        return redirect(route('enterprises.index'));
    }

    /**
     * Remove the specified Enterprise from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Enterprise $enterprise */
        $enterprise = Enterprise::find($id);

        if (empty($enterprise)) {
            Flash::error('Enterprise not found');

            return redirect(route('enterprises.index'));
        }

        $enterprise->delete();

        Flash::success('Enterprise deleted successfully.');

        return redirect(route('enterprises.index'));
    }
}
