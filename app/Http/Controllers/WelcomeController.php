<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

use App\Mail\Contribution;
use App\Mail\ContributionCopy;

use Flash;

class WelcomeController extends Controller
{
    public function index(){
        $user = null;

        $user = $this->get_user();

        return view('welcome', compact('user'));

    }

    public function formation(){
        $user = $this->get_user();
        $events = DB::Table('events')
        ->select('events.*')
        ->where('events.day', '>=', now())
        ->orderBy('events.day')
        ->get();

        $product = DB::Table('products')
        ->select('products.*', 'enterprises.enterprise')
        ->join('enterprises', 'products.user_id', 'enterprises.user_id')
        ->first();
        return view('plateforme/formation', compact('user', 'product', 'events'));
    }

    public function support(){
        $user = $this->get_user();

        $events = DB::Table('events')
        ->select('events.*')
        ->where('events.day', '>=', now())
        ->orderBy('events.day')
        ->get();

    $product = DB::Table('products')
        ->select('products.*', 'enterprises.enterprise')
        ->join('enterprises', 'products.user_id', 'enterprises.user_id')
        ->first();
        return view('plateforme/support', compact('user', 'product', 'events'));
    }

    public function note(){
        $user = $this->get_user();

        $events = DB::Table('events')
        ->select('events.*')
        ->where('events.day', '>=', now())
        ->orderBy('events.day')
        ->get();

        $product = DB::Table('products')
            ->select('products.*', 'enterprises.enterprise')
            ->join('enterprises', 'products.user_id', 'enterprises.user_id')
            ->first();

        return view('plateforme/notation', compact('user', 'product', 'events'));
    }

    public function contact(){
        $user = $this->get_user();

        $events = DB::Table('events')
        ->select('events.*')
        ->where('events.day', '>=', now())
        ->orderBy('events.day')
        ->get();

        $product = DB::Table('products')
            ->select('products.*', 'enterprises.enterprise')
            ->join('enterprises', 'products.user_id', 'enterprises.user_id')
            ->first();

        return view('plateforme/nousContacter', compact('user', 'product', 'events'));
    }

    public function notation(){
        $user = $this->get_user();

        $events = DB::Table('events')
        ->select('events.*')
        ->where('events.day', '>=', now())
        ->orderBy('events.day')
        ->get();

        $product = DB::Table('products')
            ->select('products.*', 'enterprises.enterprise')
            ->join('enterprises', 'products.user_id', 'enterprises.user_id')
            ->first();

        return view('plateforme/notation', compact('user', 'product', 'events'));
    }

    public function get_user(){
        $user = null;

        if(Auth::guest())
            return view('welcome');

        if(Auth::user()->type = "sample")
            if(Auth::user()->profile == 1){
                $user = DB::Table('users')->select('users.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->where('users.id', Auth::id())
                ->first();
            }if(Auth::user()->profile == 2){
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
            }
        else if(Auth::user()->type = "fabricant")
            if(Auth::user()->profile == 2){
                $user = DB::Table('users')->select('users.*', 'enterprises.*')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
            }if(Auth::user()->profile == 3){
                $user = DB::Table('users')->select('users.*', 'enterprises.*', 'employees.*')
                ->join('employees', 'users.id', 'employees.user_id')
                ->join('enterprises', 'users.id', 'enterprises.user_id')
                ->where('users.id', Auth::id())
                ->first();
            }

        return $user;
    }

    public function warning(Request $request){

        \Mail::to($request->email)->send(new Contribution($request));
        \Mail::to("mamadou.keita@testandrex.com")->send(new ContributionCopy($request));

        Flash::success('Message transmis avec succés');
        return redirect(route('contact-me'));
    }

}
