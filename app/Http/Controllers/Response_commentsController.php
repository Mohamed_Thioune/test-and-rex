<?php

namespace App\Http\Controllers;

use App\Http\Requests\Createresponse_commentsRequest;
use App\Http\Requests\Updateresponse_commentsRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Response_comment;
use Illuminate\Http\Request;
use Flash;
use Response;

class Response_commentsController extends AppBaseController
{
    /**
     * Display a listing of the response_comments.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var response_comments $responseComments */
        $responseComments = Response_comment::all();

        return view('response_comments.index')
            ->with('responseComments', $responseComments);
    }

    /**
     * Show the form for creating a new response_comments.
     *
     * @return Response
     */
    public function create()
    {
        return view('response_comments.create');
    }

    /**
     * Store a newly created response_comments in storage.
     *
     * @param Createresponse_commentsRequest $request
     *
     * @return Response
     */
    public function store(Createresponse_commentsRequest $request)
    {
        $input = $request->all();

        /** @var response_comments $responseComments */
        $responseComment = Response_comment::create($input);

        $responseComments =  DB::Table('users')->select('employees.firstName', 'employees.lastName', 'users.photo' ,'comments.value')
        ->join('employees', 'users.id', 'employees.user_id')
        ->join('comments', 'users.id', 'comments.user_id')
        ->where('comments.post_id', $request->post_id)
        ->get();
        Flash::success('Response Comments saved successfully.');

        return redirect(route('responseComments.index'));
    }

    /**
     * Display the specified response_comments.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var response_comments $responseComments */
        $responseComments = Response_comment::find($id);

        if (empty($responseComments)) {
            Flash::error('Response Comments not found');

            return redirect(route('responseComments.index'));
        }

        return view('response_comments.show')->with('responseComments', $responseComments);
    }

    /**
     * Show the form for editing the specified response_comments.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var response_comments $responseComments */
        $responseComments = Response_comment::find($id);

        if (empty($responseComments)) {
            Flash::error('Response Comments not found');

            return redirect(route('responseComments.index'));
        }

        return view('response_comments.edit')->with('responseComments', $responseComments);
    }

    /**
     * Update the specified response_comments in storage.
     *
     * @param int $id
     * @param Updateresponse_commentsRequest $request
     *
     * @return Response
     */
    public function update($id, Updateresponse_commentsRequest $request)
    {
        /** @var response_comments $responseComments */
        $responseComments = Response_comment::find($id);

        if (empty($responseComments)) {
            Flash::error('Response Comments not found');

            return redirect(route('responseComments.index'));
        }

        $responseComments->fill($request->all());
        $responseComments->save();

        Flash::success('Response Comments updated successfully.');

        return redirect(route('responseComments.index'));
    }

    /**
     * Remove the specified response_comments from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var response_comments $responseComments */
        $responseComments = Response_comment::find($id);

        if (empty($responseComments)) {
            Flash::error('Response Comments not found');

            return redirect(route('responseComments.index'));
        }

        $responseComments->delete();

        Flash::success('Response Comments deleted successfully.');

        return redirect(route('responseComments.index'));
    }
}
