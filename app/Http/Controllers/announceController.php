<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateannounceRequest;
use App\Http\Requests\UpdateannounceRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\announce;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;

use Flash;
use Response;

class announceController extends AppBaseController
{
    /**
     * Display a listing of the announce.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var announce $announces */
        $announces = announce::all();

        return view('announces.index')
            ->with('announces', $announces);
    }

    /**
     * Show the form for creating a new announce.
     *
     * @return Response
     */
    public function create()
    {
        return view('announces.create');
    }

    /**
     * Store a newly created announce in storage.
     *
     * @param CreateannounceRequest $request
     *
     * @return Response
     */
    public function store(CreateannounceRequest $request)
    {
        $input = $request->all();

        $file = $request->file('media');

        if($request->hasfile('media'))
        {
            $filePath = 'advertisements';
            $extension = $file->getClientOriginalExtension(); 
            $mimetype = explode('/',\GuzzleHttp\Psr7\mimetype_from_extension($extension))[0];
            $input['media'] = Str::random(50). '.' . $extension;
            $file->storeAs($filePath, $input['media'], 's3');  
        }          

        /** @var announce $announce */
        $announce = announce::create($input);

        Flash::success('Announce saved successfully.');

        return redirect(route('announces.index'));
    }

    /**
     * Display the specified announce.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var announce $announce */
        $announce = announce::find($id);

        if (empty($announce)) {
            Flash::error('Announce not found');

            return redirect(route('announces.index'));
        }

        return view('announces.show')->with('announce', $announce);
    }

    /**
     * Show the form for editing the specified announce.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var announce $announce */
        $announce = announce::find($id);

        if (empty($announce)) {
            Flash::error('Announce not found');

            return redirect(route('announces.index'));
        }

        return view('announces.edit')->with('announce', $announce);
    }

    /**
     * Update the specified announce in storage.
     *
     * @param int $id
     * @param UpdateannounceRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        /** @var announce $announce */
        $announce = announce::find($id);

        $input = $request->all();

        if (empty($announce)) {
            Flash::error('Announce not found');

            return redirect(route('announces.index'));
        }

        if($request->hasfile('media'))
        {
            $filePath = 'advertisements';
            $file = $request->file('media');
            $extension = $file->getClientOriginalExtension(); 
            $input['media'] = Str::random(20). '.' . $extension;
            $file->storeAs($filePath, $input['media'], 's3');            
        }

        /** Announces attributes */  
        $input['media'] = (isset($input['media'])) ? $input['media'] : $announce->media;
        $input['title'] = ($input['title']) ? $input['title'] : $announce->title;
        $input['link'] = ($input['link']) ? $input['link'] : $announce->link;

        $announce->fill($input);
        $announce->save();

        Flash::success('Announce updated successfully.');

        return redirect(route('announces.index'));
    }

    /**
     * Remove the specified announce from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var announce $announce */
        $announce = announce::find($id);

        if (empty($announce)) {
            Flash::error('Announce not found');

            return redirect(route('announces.index'));
        }

        $announce->delete();

        Flash::success('Announce deleted successfully.');

        return redirect(route('announces.index'));
    }
}
