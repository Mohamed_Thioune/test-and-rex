<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Post;
use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;
use Auth;

use DB;

use Flash;
use Response;

class PostController extends AppBaseController
{
    /**
     * Display a listing of the Post.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Post $posts */
        $posts = Post::all();

        return view('posts.index')
            ->with('posts', $posts);
    }

    /**
     * Show the form for creating a new Post.
     *
     * @return Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created Post in storage.
     *
     * @param CreatePostRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $model  = new Post;

        $model->rex = $request->rex;

        $model->text = nl2br($request->text);

        $model->user_id = Auth::id();
        
        /** @var Post $post */
        $model->save();
        DB::table('files')
        ->where('token', $request->token)
        ->update(['post_id' => $model->id]);

        Flash::success('Your Post created successfully.');

        return redirect(route('home'));
    }

    public function file(Request $request){
        // Count of uploaded files in array
        $file = $request->file('media');
        $context = new file();

        if($request->hasfile('media'))
        {
            $input['token'] = $request->header('X-TOKEN');
            $filePath = 'posts';
            $extension = $file->getClientOriginalExtension(); 
            $mimetype = explode('/',\GuzzleHttp\Psr7\mimetype_from_extension($extension))[0];
            $input['type'] = $mimetype;
            $input['name'] = Str::random(50). '.' . $extension;
            $file->storeAs($filePath, $input['name'], 's3');
            $files = File::create($input); 
            
        }
    }

    /**
     * Display the specified Post.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Post $post */
        $post = Post::find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified Post.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Post $post */
        $post = Post::find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified Post in storage.
     *
     * @param int $id
     * @param UpdatePostRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePostRequest $request)
    {
        /** @var Post $post */
        $post = Post::find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        $post->fill($request->all());
        $post->save();

        Flash::success('Post updated successfully.');

        return redirect(route('posts.index'));
    }

    /**
     * Remove the specified Post from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Post $post */
        $post = Post::find($id);

        if (empty($post)) {
            Flash::error('Post not found');

            return redirect(route('posts.index'));
        }

        $post->delete();

        Flash::success('Post deleted successfully.');

        return redirect(route('posts.index'));
    }
}
