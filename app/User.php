<?php

namespace App;

use Eloquent as Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;

    public $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo', 'isAdmin', 'phone', 'country', 'address', 'type', 'profile', 'state', 'lastLogin', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules_updated = [
        'country' => 'required',
        'photo' => 'mimes:jpeg,jpg,png,tif,webp|max:12400'

    ];
}
