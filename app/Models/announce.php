<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class announce
 * @package App\Models
 * @version September 28, 2021, 10:53 pm UTC
 *
 * @property string title
 * @property string link
 * @property string media
 */
class announce extends Model
{
    use SoftDeletes;

    public $table = 'announces';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'title',
        'link',
        'media'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'link' => 'string',
        'media' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required|max:30',
        'media' => 'required'
    ];

    
}
