<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Enterprise
 * @package App\Models
 * @version June 24, 2021, 4:38 pm UTC
 *
 * @property string enterprise
 * @property string activityDomain
 * @property string targetArea
 */
class Enterprise extends Model
{
    use SoftDeletes;

    public $table = 'enterprises';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'enterprise',
        'expertiseDomain',
        'targetArea',
        'model',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'enterprise' => 'string',
        'expertiseDomain' => 'string',
        'targetArea' => 'string',
        'model' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'enterprise' => 'required',
    ];

    
}
