<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Employee
 * @package App\Models
 * @version June 24, 2021, 4:35 pm UTC
 *
 * @property string firstName
 * @property string lastName
 * @property string enterprise
 * @property string expertiseDomain
 * @property string function
 * @property string username
 */
class Employee extends Model
{
    use SoftDeletes;

    public $table = 'employees';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'username',
        'firstName',
        'lastName',
        'enterprise',
        'expertiseDomain',
        'function',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'username' => 'string',
        'firstName' => 'string',
        'lastName' => 'string',
        'enterprise' => 'string',
        'expertiseDomain' => 'string',
        'function' => 'string',
        'user_id' => 'integer'
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'firstName' => 'required',
        'lastName' => 'required',
        'expertiseDomain' => 'required'
    ];

    
}
