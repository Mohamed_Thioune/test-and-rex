<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class advertisement
 * @package App\Models
 * @version September 28, 2021, 10:46 pm UTC
 *
 * @property string frame
 * @property string textual
 * @property string link
 * @property string media
 */
class advertisement extends Model
{
    use SoftDeletes;

    public $table = 'advertisements';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'frame',
        'type',
        'title',
        'textual',
        'link',
        'media'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'frame' => 'string',
        'title' => 'string',
        'textual' => 'string',
        'link' => 'string',
        'media' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'media' => 'required',
        'title' => 'max:15',
        'textual' => 'max:35'
    ];

    
}
