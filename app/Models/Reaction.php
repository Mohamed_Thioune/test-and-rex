<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Reaction
 * @package App\Models
 * @version August 17, 2021, 3:59 pm UTC
 *
 * @property string value
 * @property string type
 * @property integer user_id
 */
class Reaction extends Model
{
    use SoftDeletes;

    public $table = 'reactions';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'value',
        'type',
        'post_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'value' => 'string',
        'type' => 'string',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'value' => 'required',
        'type' => 'required',
        'user_id' => 'required'
    ];

    
}
