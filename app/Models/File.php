<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class File
 * @package App\Models
 * @version September 13, 2021, 8:11 pm UTC
 *
 * @property string token
 * @property string name
 */
class File extends Model
{
    use SoftDeletes;

    public $table = 'files';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'token',
        'name',
        'type',
        'post_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'token' => 'string',
        'type' => 'string',
        'name' => 'string',
        'post_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'token' => 'required',
        'name' => 'required',
        'type' => 'required',
        
    ];

    
}
