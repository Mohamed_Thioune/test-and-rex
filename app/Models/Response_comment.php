<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class response_comments
 * @package App\Models
 * @version August 23, 2021, 5:00 pm UTC
 *
 * @property string value
 * @property string fullname
 * @property integer user_id
 * @property integer comment_id
 */
class Response_comment extends Model
{
    use SoftDeletes;

    public $table = 'response_comments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'value',
        'fullname',
        'user_id',
        'comment_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'value' => 'string',
        'fullname' => 'string',
        'user_id' => 'integer',
        'comment_id' => 'integer',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
