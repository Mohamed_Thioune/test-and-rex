<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @package App\Models
 * @version July 24, 2021, 4:33 am UTC
 *
 * @property string text
 * @property string type
 * @property string media
 * @property time Duration
 * @property integer online
 * @property integer request_monetize
 * @property integer is_monetize
 */
class Post extends Model
{
    use SoftDeletes;

    public $table = 'posts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'text',
        'rex',
        'type',
        'media',
        'duration',
        'online',
        'request_monetize',
        'is_monetize',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'text' => 'string',
        'type' => 'string',
        'media' => 'string',
        'online' => 'integer',
        'request_monetize' => 'integer',
        'is_monetize' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'text' => 'required',
        'media' => 'max:60200|file|mimes:jpeg,jpg,png,webp,mp4,vlc,avi,webm,flv,wmv,mov,ts,3gp,qt,ogg'
    ];

    
}
