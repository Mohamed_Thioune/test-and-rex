<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version July 21, 2021, 1:54 am UTC
 *
 * @property string name
 * @property string photo
 * @property string description
 * @property integer price
 * @property string type
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'photo',
        'description',
        'price',
        'type',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'photo' => 'string',
        'description' => 'string',
        'price' => 'integer',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'photo' => 'required',
        'type' => 'required'
    ];

    public static $rules_updated = [
        'name' => 'required',
        'photo' => 'mimes:jpeg,jpg,png,tif,webp|max:12400'

    ];

    
}
