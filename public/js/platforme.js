$(".btnTrais").click(function() {
    $(".modalModifeProfil").show();
});
$(".close").click(function() {
    $(".modalModifeProfil").hide();
});
$(".BtnAnnuler").click(function() {
    $(".modalModifeProfil").hide();
});
//pour page internes
$("#btnInut1").click(function() {
    $(".publierRex").show();
    $(".blockC").hide();
});

$("#btnRetourPub1").click(function() {
    $(".publierRex").hide();
    $(".blockC").show();
});

$("#btnDemandeFormation").click(function(e) {
    $(".blockDemander").show();
    $(".blockOnePublication").hide();
    e.preventDefault();
});

$( "#transformateur" ).click(function() {
    $("#rex").val("1");
});

$( "#cable" ).click(function() {
    $("#rex").val("2");
});

$( "#protection" ).click(function() {
    $("#rex").val("3");
});

$( "#moteur" ).click(function() {
    $("#rex").val("4");
});

$( "#disjoncteur" ).click(function() {
    $("#rex").val("5");
});

$( "#autres" ).click(function() {
    $("#rex").val("6");

});


$(".btnRetourDemande").click(function(e) {
    $(".blockDemander").hide();
    $(".blockOnePublication").show();
    e.preventDefault();
});

$("#btnNouvautes").click(function(e) {
    $(".comparatifsProduitsBlock").hide();
    $(".ideeBlock").hide();
    $(".nauvautesBlock").show();
    $("#btnComparatifsVeille").removeClass('active')
    $("#btnIdee").removeClass('active')
    $("#btnNouvautes").addClass('active')
    e.preventDefault();
});
$("#btnComparatifsVeille").click(function(e) {
    $(".comparatifsProduitsBlock").show();
    $(".nauvautesBlock").hide();
    $(".ideeBlock").hide();
    $("#btnComparatifsVeille").addClass('active')
    $("#btnNouvautes").removeClass('active')
    $("#btnIdee").removeClass('active')
    e.preventDefault();
});
$("#btnIdee").click(function(e) {
    $(".comparatifsProduitsBlock").hide();
    $(".nauvautesBlock").hide();
    $(".ideeBlock").show();
    $("#btnNouvautes").removeClass('active')
    $("#btnComparatifsVeille").removeClass('active')
    $("#btnIdee").addClass('active')
    e.preventDefault();
});
$("#btnIdee").click(function(e) {
    $(".comparatifsProduitsBlock").hide();
    $(".nauvautesBlock").hide();
    $(".ideeBlock").show();
    $("#btnNouvautes").removeClass('active')
    $("#btnComparatifsVeille").removeClass('active')
    $("#btnIdee").addClass('active')
    e.preventDefault();
});

$( "#btnNouvautesFabriquant" ).click(function() {
    $(".elementComparatifDeMarque").hide();
    $(".elementClassementProduit").hide();
    $(".elementClassementFabriquant").show();
    $("#btnIdeeProduits").removeClass('active');
    $("#btnNouvautesFabriquant").addClass('active');
    $("#btnComparatifsMarques").removeClass('active');
    e.preventDefault();

});

$( "#btnIdeeProduits" ).click(function() {
    $(".elementComparatifDeMarque").hide();
    $(".elementClassementProduit").show();
    $(".elementClassementFabriquant").hide();
    $("#btnIdeeProduits").addClass('active');
    $("#btnNouvautesFabriquant").removeClass('active');
    $("#btnComparatifsMarques").removeClass('active');
    e.preventDefault();

});

$( "#btnComparatifsMarques" ).click(function() {
    $(".elementComparatifDeMarque").show();
    $(".elementClassementProduit").hide();
    $(".elementClassementFabriquant").hide();
    $("#btnIdeeProduits").removeClass('active');
    $("#btnNouvautesFabriquant").removeClass('active');
    $("#btnComparatifsMarques").addClass('active');
    e.preventDefault();
});

$(function() {
    $("#image-attachment").fileinput({
        minFileCount: 0,
        maxFileCount: 1,
        showUpload: false,
        showCaption: false,
        /*
                initialPreview: [
                    //depend on previous data
                    'http://upload.wikimedia.org/wikipedia/commons/thumb/e/e1/FullMoon2010.jpg/631px-FullMoon2010.jpg'
                ],*/
        initialPreviewAsData: true,
        initialPreviewConfig: [
            //depend on previous data
            {
                caption: "Moon.jpg",
                size: 930321,
                width: "120px",
                key: 1,
                showDelete: true
            }
        ],
        deleteUrl: "/site/file-delete",
        overwriteInitial: true,
        initialCaption: "The Moon and the Earth"
    });

});

$("#btnDemandeFormation").click(function(e) {
    $(".blocksContentPublication").hide();
    $(".blocksContentPublierOffre").show();

    e.preventDefault();
});
$(".btnRetourPublication").click(function(e) {
    $(".blocksContentPublication").show();
    $(".blocksContentPublierOffre").hide();

    e.preventDefault();
});

$("#btnIdeeAmelioration").click(function(e) {
    $(".blockideeAmelioration").show();
    $(".blockNoAbonne").hide();

    e.preventDefault();
});


$(".btnPertinent").click(function() {
    $(".btnPertinent").addClass('btnActive');
    $(".btnInteressant ").removeClass('btnActive')
});

$(".btnInteressant").click(function() {
    $(".btnPertinent").removeClass('btnActive');
    $(".btnInteressant ").addClass('btnActive')
});


// pour modification photo profil


$('#imageInput').on('change', function() {
    $input = $(this);
    if($input.val().length > 0) {
        fileReader = new FileReader();
        fileReader.onload = function (data) {
            $('.image-preview').attr('src', data.target.result);
        }
        fileReader.readAsDataURL($input.prop('files')[0]);
        $('.image-button').css('display', 'none');
        $('.image-preview').css('display', 'block');
        $('.change-image').css('display', 'block');
        $('.photoBlockProfilInput').css('display', 'block');
    }
});

$('.change-image').on('click', function() {
    $control = $(this);
    $('#imageInput').val('');
    $preview = $('.image-preview');
    $preview.attr('src', '');
    $preview.css('display', 'none');
    $control.css('display', 'none');
    $('.image-button').css('display', 'block');
    $('.photoBlockProfilInput').css('display', 'none');
});
