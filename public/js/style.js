// code our la page login changement de formulaire
$(document).ready(function() {
    $(".blokOneBis").hide();
    $(".blokOneFabriquant").hide();
    $(".formEquipementOne").hide();

    $(".btnUtilisateur").click(function() {
        $(".btnFabricant").removeClass('activeBtn');
        $(".btnUtilisateur").addClass('activeBtn');
        $(".formEquipementOne").hide();
        $(".formUser").show();
    });
    $(".btnFabricant").click(function() {
        $(".formEquipementOne").show();
        $(".formUser").hide();
        $(".btnUtilisateur").removeClass('activeBtn');
        $(".btnFabricant").addClass('activeBtn');
    });


    $('#continuerEntreprise').click(function(e) {
        if ($('.inputUtilisateur').val().length === 0) {
            alert('Veuillez remplir les champs svp!');
        } else {
            $(".hide1").hide();
            $(".blokOneBis").show();
            e.preventDefault();
        }
    });
    $("#btnRetour1").click(function(e) {
        $(".hide1").show();
        $(".blokOneBis").hide();
        e.preventDefault();
    });

    $('#continuerFabriquant').click(function(e) {
        if ($('.inputUtilisateur2').val().length === 0) {
            alert('Veuillez remplir les champs svp!');
        } else {
            $(".hide2").hide();
            $(".blokOneFabriquant").show();
            e.preventDefault();
        }
    });
    $("#btnRetour2").click(function(e) {
        $(".hide2").show();
        $(".blokOneFabriquant").hide();
        e.preventDefault();
    });

});

//Pour les tabs
$("#btnTabsInscription").click(function(e) {
    $(".contentInscription").show();
    $(".contentLogin").hide();
    $("#btnTabsInscription").addClass("activeBtn");
    $("#btnTabsConnexion").removeClass("activeBtn");
    e.preventDefault();
});
$("#btnTabsConnexion").click(function(e) {
    $(".contentLogin").show();
    $(".contentInscription").hide();
    $("#btnTabsInscription").removeClass("activeBtn");
    $("#btnTabsConnexion").addClass("activeBtn");
    e.preventDefault();
});

//Pour les formulaires

$("#contunierEquipement").click(function(e) {
    $(".formeAccount").show();
    $(".headTabAccount").hide();
    $(".blockfabriquants").hide();
    $(".votreProfil").hide();
    $(".contentAccount").removeClass("back1");
    $(".contentChooseProfil").hide();
    e.preventDefault();
});
$("#btnRetour1").click(function(e) {
    $(".formeAccount").hide();
    $(".headTabAccount").show();
    $(".votreProfil").show();
    $(".contentAccount").addClass("back1");
    $(".contentChooseProfil").show();
    $(".blockfabriquants").hide();
    $(".blockUtilisationEquipement").hide();
    e.preventDefault();
});
$("#btnContinuerFabriquant").click(function(e) {
    $(".formeAccount").show();
    $(".headTabAccount").hide();
    $(".blockfabriquants").show();
    $(".votreProfil").hide();
    $(".contentAccount").removeClass("back1");
    $(".contentChooseProfil").hide();
    $(".blockUtilisationEquipement").hide();
    e.preventDefault();
});

$("#btnRetour2").click(function(e) {
    $(".formeAccount").hide();
    $(".headTabAccount").show();
    $(".votreProfil").show();
    $(".contentAccount").addClass("back1");
    $(".contentChooseProfil").show();
    $(".blockfabriquants").hide();
    $(".blockUtilisationEquipement").hide();
    e.preventDefault();
});

$("#btnDown1").click(function(e) {
    $(".elementTextHide").toggle();
    e.preventDefault();
});


$("#btnDown2").click(function(e) {
    $(".elementTextHide2").toggle();
    e.preventDefault();
});
$("#btnSuite").click(function(e) {
    $(".textSuite").show();
    $("#btnSuite").hide();
    $("#reduirebtn").show();
    e.preventDefault();
});
$("#reduirebtn").click(function(e) {
    $(".textSuite").hide();
    $("#btnSuite").show();
    $("#reduirebtn").hide();
    e.preventDefault();
});

//Pour le select formulaire inscriptions
//Pour utilisateur d'équipement
$('#select1').on('change', function() {
        if (this.value == "1") {
            $(".inputSelectHide").show();
        } else if (this.value == "1") {
            $(".inputSelectHide").show();
        } else {
            $(".inputSelectHide").hide();
        }
    })
    //Pour fabricant
$('#select2').on('change', function() {
    if (this.value == "2") {
        $(".inputSelectHide2").show();
    } else if (this.value == "2") {
        $(".inputSelectHide2").show();
    } else {
        $(".inputSelectHide2").hide();
    }
})


$('.owl-carousel').owlCarousel({
    margin: 10,
    nav: true,
    navText: ["<div class='nav-btn prev-slide'><img src=\"img/chevronLeft.svg\"></div>", "<div class='nav-btn next-slide'><img src=\"img/chevronRight.svg\"></div>"],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 3
        },
        1000: {
            items: 3
        }
    }
});
