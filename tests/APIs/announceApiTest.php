<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\announce;

class announceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_announce()
    {
        $announce = factory(announce::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/announces', $announce
        );

        $this->assertApiResponse($announce);
    }

    /**
     * @test
     */
    public function test_read_announce()
    {
        $announce = factory(announce::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/announces/'.$announce->id
        );

        $this->assertApiResponse($announce->toArray());
    }

    /**
     * @test
     */
    public function test_update_announce()
    {
        $announce = factory(announce::class)->create();
        $editedannounce = factory(announce::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/announces/'.$announce->id,
            $editedannounce
        );

        $this->assertApiResponse($editedannounce);
    }

    /**
     * @test
     */
    public function test_delete_announce()
    {
        $announce = factory(announce::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/announces/'.$announce->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/announces/'.$announce->id
        );

        $this->response->assertStatus(404);
    }
}
